module gitlab.com/Tristero/webdev-go

go 1.14

require (
	github.com/go-sql-driver/mysql v1.5.0 // direct
	github.com/google/uuid v1.1.1 // direct
	github.com/julienschmidt/httprouter v1.3.0 // direct
	go.mongodb.org/mongo-driver v1.3.2 // direct
	golang.org/x/crypto v0.0.0-20200323165209-0ec3e9974c59 // direct
)
