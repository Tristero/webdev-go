package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"strings"

	"gitlab.com/Tristero/webdev-go/serveurs/code8/rot13"
)

func main() {
	fmt.Println("Serveur Go Chiffre de César v1")

	/*msg := []byte("hello")
	rslt := rot13.Rot13(msg)
	rslt = rot13.Rot13(rslt)
	fmt.Println(string(rslt))*/

	lis, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatalf("Erreur sur le listener : %v", err)
	}

	for {
		cnx, err := lis.Accept()
		if err != nil {
			log.Fatalf("Erreur à la connexion : %v", err)
		}

		go gereCnx(cnx)
	}
}

func gereCnx(cnx net.Conn) {

	// lire l'entrée
	scanner := bufio.NewScanner(cnx)

	for scanner.Scan() {
		ln := strings.ToLower(scanner.Text())
		rslt := rot13.Rot13([]byte(ln))
		strRslt := string(rslt)
		// envoi du résultat
		fmt.Printf("Message reçu : %v\nMessage chiffré : %v\n", ln, strRslt)
		fmt.Fprintf(cnx, "Message chiffré : %v\n", strRslt)
	}

	defer cnx.Close()

	fmt.Println("Client déconnecté")
}
