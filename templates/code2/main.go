package main

import (
	"log"
	"os"
	"text/template"
)

func main() {
	tpl, err := template.ParseGlob("*.txt")
	if err != nil {
		log.Fatalf("Erreur au chargement des fichiers : %v", err)
	}

	if tpl.ExecuteTemplate(os.Stdout, "fichier1.txt", nil) != nil {
		log.Fatalf("Erreur sur le traitement du fichier1 : %v", err)
	}

	if tpl.ExecuteTemplate(os.Stdout, "fichier2.txt", nil) != nil {
		log.Fatalf("Erreur sur le traitement du fichier2 : %v", err)
	}

	if tpl.ExecuteTemplate(os.Stdout, "fichier3.txt", nil) != nil {
		log.Fatalf("Erreur sur le traitement du fichier3 : %v", err)
	}
}
