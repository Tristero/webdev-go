package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net"
)

func main() {
	fmt.Println("Client Go v0.1")
	cnx, err := net.Dial("tcp", "localhost:8080")
	if err != nil {
		log.Fatalf("Erreur sur l'appel : %v", err)
	}
	defer cnx.Close()

	tab, err := ioutil.ReadAll(cnx)
	if err != nil {
		log.Fatalf("Erreur sur la lecture des données : %v", err)
	}

	fmt.Println(string(tab))
}
