package main

import (
	"fmt"
	"log"
	"net/http"
)

type chien struct{}
type chat struct{}

func (c chien) ServeHTTP(rep http.ResponseWriter, req *http.Request) {
	fmt.Fprintf(rep, "Appel de la page copnsacré aux chiens")
}

func (c chat) ServeHTTP(rep http.ResponseWriter, req *http.Request) {
	fmt.Fprintf(rep, "Appel de la page copnsacré aux chats")

}

func main() {

	var chien chien
	var chat chat
	mux := http.NewServeMux()
	fmt.Println("Lancement serveur multiplex v2")
	mux.Handle("/chien/", chien)
	mux.Handle("/chat", chat)

	if err := http.ListenAndServe(":8080", mux); err != nil {
		log.Fatalf("Erreur ListenAndServe : %v", err)
	}
}
