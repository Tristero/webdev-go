package main

import (
	"log"
	"os"
	"text/template"
)

func main() {
	t, err := template.ParseFiles("texte1.gohtml")
	if err != nil {
		log.Fatalf("Erreur au chargement du fichier : %v", err)
	}

	f, err := os.Create("resultat.html")
	if err != nil {
		log.Fatalf("Erreur à la création du fichier de sortie : %v", err)
	}

	defer f.Close()

	if t.Execute(f, nil) != nil {
		log.Fatal("Erreur à l'exécution")
	}
}
