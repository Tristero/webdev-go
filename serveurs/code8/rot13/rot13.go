package rot13

/*
Rot13 converti un slice de byte. Seuls les caractères ASCII sont admis de a à z seulement. Il retourne un tableau de slice
```
	msg := "anything"
	gsm := rot13([]byte(msg))
	fmt.Println(string(gsm))

	msg = "nalguvat"
	gsm = rot13([]byte(msg))
	fmt.Println(string(gsm))
```
*/
func Rot13(msg []byte) []byte {
	var rslt = make([]byte, len(msg))
	for idx, elem := range msg {
		rslt[idx] = change(elem)
	}
	return rslt
}

func change(i byte) byte {
	rslt := i + 13
	if rslt > 122 {
		rslt = (rslt % 122) + 96
	}
	return rslt
}
