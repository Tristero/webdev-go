package main

import (
	"log"
	"os"
	"text/template"
)

var tpl *template.Template

func init() {
	tpl = template.Must(
		template.ParseFiles("fichier.html"),
	)
}

func main() {
	if tpl.ExecuteTemplate(os.Stdout, "fichier.html", "42") != nil {
		log.Fatalf("Erreur dans l'exécution du parsing")
	}
}
