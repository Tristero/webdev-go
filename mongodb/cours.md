# MongoDB

Pré-requis : il faut une instance de MongoDB sur son ordinateur. Soit on utilisera une image Docker, soit on installera la version Community Edition. Connaître comment fonctionne les bases de données MongoDB, notamment savoir ce qu'est une collection, un document et comment y accéder, faire des manipulations de bases (CRUD). Besoin d'un tutoriel ? [Tutorials.com](https://www.tutorialspoint.com/mongodb/index.htm). Sinon MongoDB dispose de cours très complets, gratuits avec un cursus : [Mongo University](https://university.mongodb.com/)

Pour installer MongoDB selon son OS : [doc officielle](https://docs.mongodb.com/manual/administration/install-community/) (il n'y a rien de bien complexe : on suivra seulement avec attention les directives).

## 1-Driver

Pour faire dialoguer Mongo et Go, nous devons utiliser un driver et l'installer avec la commande : ```go get go.mongodb.org/mongo-driver/mongo``` (qu'il faudra appeler pour chaque nouveau projet avec ```go mod```).

Contrairement aux bases comme MySQL/MariaDB qui disposent d'un driver reposant sur l'API ```database/sql```, MongoDB a développé son propre driver et donc sa propre syntaxe... parfois lourde, peu ergonomique.

Exemple de création d'un projet avec les modules:

```sh
mkdir goproj
cd goproj
go mod init goproj
go get go.mongodb.org/mongo-driver/mongo
```

## 2-Connexion

Dans un premier temps, nous ne verrons pas l'application des contextes (nous utiliserons ```TODO()``` en lieu et place de ```Background()```). Une fois les bases acquises, nous verrons comment les utiliser.

### 1-Options

Dans un premier temps nous devons déterminer les options de connexions au serveur. Ces options sont à importer : ```import "go.mongodb.org/mongo-driver/mongo/options"```

Comme notre base est localisée sur notre disque (```localhost```) et que nous n'avons pas modifié le port (```27017```), nous allons pouvoir créer un nouveau client. Mais avant cela nous devons préparer des éléments utiles à la connexion grâce au sous-package ```options``` et l'appel de la fonction ```Client()``` et de sa méthode ```ApplyURI()```.

Dans cette méthode on passera une URI indiquant le protocole, l'adresse et le port.

```go
opts = options.Client().ApplyURI("mongodb://localhost:27017")
```

On obtient un pointeur de type ```options.ClientOptions```.

A partir de ces options on va pouvoir créer un client puis on vérifiera s'il est possible de se connecter. Ici nous devons recourir aux Contexts car nous devons en passer un en paramètre.

### 2-Connexion du client

Nous devons créer un client à partir de la fonction ```Connect()``` du package ```mongo``` ! Celui-ci nous retournera deux valeurs :

- un pointeur ```mongo.Client``` qui est notre client à proprement parlé
- une erreur

Cette fonction ```Connect()``` prend deux paramètres :

- un contexte
- les options que nous avons créées

Pour le contexte on utilisera la fonction ```TODO()``` (pour plus de renseignement : voir la [doc officielle](https://pkg.go.dev/context?tab=doc#TODO))

```go
client, err := mongo.Connect(context.TODO(), opts)
```

### 3-Vérification de la connexion

Pour vérifier la connexion on dispose de la méthode ```Ping()```. Qui dit méthode, dit objet associé : ici c'est notre client.

```Ping()``` demande deux arguments :

- un contexte (à nouveau on utilisera ```TODO()```)
- des préférences de lecture (quel serveur sera sélectionné pour l'opération : ici il n'y en a qu'un seul donc on choisira ```nil```)

```go
log.Fatal(client.Ping(context.TODO(), nil))
```

Le fait d'appeler ```Ping()``` crée **une connexion entre le client et le serveur**.

### 4-Déconnexion

Maintenant nous devons déconnecter le client du serveur.

Le type ```Client``` dispose de la méthode ```Disconnect()``` qui prend en argument un contexte et retourne une erreur :

```go
err = client.Disconnect(context.TODO())
```

Code [Go](code1/main.go)

### 5-Contextes

Nous avons manipulés les connexions avec TODO() qui ne fait rien. Maintenant implémentons le même code avec un timeout !

Nous souhaitons faire deux timeouts indépendants :

- un pour la connexion
- un pour le ping

Les deux reposent sur la même syntaxe simple :

```go
ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
defer cancel()
```

On appliquera ```ctx``` comme argument à ```Connect``` et ```Ping``` en lieu et place de ```context.TODO()```.

Test : on arrête le serveur et on lance le code. On vérifiera les temps affichés par les logs.

Code [Go](code2/main.go)

## 4-Package bson

Rien de plus simple avec MongoDB. En ligne de commande c'est en utilisant ```use``` suivi du nom d'une base. Si celle-ci existe alors on bascule sur elle, sinon elle est créée pour nous. Le principe est exactement le même avec Go.

Lorsqu'on manipule des bases, collections et documents, Mongo a recours à une déclinaison de JSON nommé BSON : version améliorée de JSON [Doc](http://bsonspec.org/). Donc le driver Go pour Mongo dispose de types adaptés pour le BSON.

Le package bson fourni va nous permettre de manipuler des documents BSON. Il existe deux types de *primitives* pour les manipuler car on ne les utilise jamais directement mais via ```bson.A```,```bson.D```, ```bson.E``` et ```bson.M```. A pour *array*, D pour *document*, E pour *élément* de document et M pour *map*.

Voici quelques exemples :

```go
// Un document D
bson.D{{"User", "John Doe"}, {"msg", "hello, world"}, {"pi", 3.14159}}
// Son pendant M
bson.M{"User": "John Doe", "msg": "hello world", "pi": 3.14159}

// Tableaux requiert bson.A qui peut contenir lui-même des documents :
bson.A{"element1", 123, "John Doe", bson.D{{"userId", 1}}}
```

Quant au type E, sa définition est simple c'est une structure avec une clé et une valeur :

```go
type E struct {
    Key   string
    Value interface{}
}
```

Avec ces quelques connaissances, nous allons pouvoir faire notre toute première requête : lister toutes les bases présentes sur notre serveur !

## 5-Une première requête : lister toutes les bases

Le type ```Client``` dispose d'une méthode ```ListDatabaseNames()``` qui prend au moins deux arguments :

- un contexte
- un document (de type ```bson.D``` ou ```bson.M```)

Et retourne deux types :

- un résultat sous forme de slice de string
- une erreur

Nous souhaitons obtenir **toutes** les bases de données existantes sur notre serveur.

```go
dbs, err := client.ListDatabaseNames(ctx, bson.D{})
```

Code [Go](code3/main.go)

## 6-Seconde requête : lister toutes les collections

Parmi toutes les bases créées il y en a trois installées pour nous *de facto*. Nous allons utiliser "local" et regarder les collections présentes.

Avec le shell mongo, c'est plutôt simple :

```txt
> use local
switched to db local
> db.getCollectionNames()
[ "startup_log" ]
```

Essayons d'obtenir le dernier résultat. Pour cela nous devons nous connecter à la base puis requêter. Encore une fois, on s'appuie sur notre type ```Client```. Il nous faut appeler la méthode ```Database()``` en lui passant le nom de la base que l'on souhaite utiliser (ici "local") puis on appelle ```ListCollectionNames()```. Cette méthode nécessite trois paramètres :

- un contexte
- un document (D ou M)
- options (nil : nous ne y intéresserons pas pour l'instant)

Et retourne deux valeurs :

- une chaîne de strings
- une erreur

Pour obtenir **toutes** les collections :

```go
r, err := client.Database("local").ListCollectionNames(ctx, bson.D{}, nil)
```

Code [Go](code4/main.go)

## 7-CRUD

Pour nos essais nous créerons une base "biblio" et une collection "livres".

### 1-Créer UN document

Avant toute chose définissons une structure représentant un livre : il nous faut le titre, l'auteur, l'éditeur et le nombre de page. Pour l'auteur nous créerons une autre structure avec le nom et le prénom

```go
type Livre struct {
    Titre   string
    Auteur  Auteur
    Editeur string
    Pages   int
}

type Auteur struct {
    Nom    string
    Prenom string
}
```

Avec MongoDB on dispose non pas de *create* mais de plusieurs méthodes :```insert()```, ```insertMany()``` et ```insertOne()```. Nous allons créer un seul document pour le moment donc le pendant de ```insertOne()``` est ... ```InsertOne()```. On passera un paramètre un contexte et notre variable de type ```Livre```. Cette méthode nous retournera deux valeurs :

- un type ```InsertOneResult```
- une erreur

On récupèrera un ID via ```InsertOneResult``` et on l'affichera :

```go
// Ici on crée notre "document" qui est une structure Go traditionnelle
l := Livre{
    Titre: "Shakespeare, la biographie",
    Auteur: Auteur{
        Prenom: "Peter",
        Nom:    "Ackroyd",
    },
    Editeur: "Points",
    Pages:   764,
}

// on "récupère" (en fait on crée et la base et la collection) notre collection "livres"
coll := client.Database("biblio").Collection("livres")
// Puis on insère
rslt, err := coll.InsertOne(ctx, l)
if err !=nil {
    log.Printf("Erreur dans l'insertion du document : %v",err)
} else {
    // on afficle l'ID automatiquement retourné à l'insertion
    fmt.Printf("Document inséré : %v\n", rslt.InsertedID)

}
```

Code [Go](code5/main.go)

### 2-Créer DES documents

Comme nous l'avons indiqué ci-dessus on peut bien évidemment passer plusieurs documents au serveur MongoDB. Pour cela l'API pour Go dispose de ```insertMany()```. A la différence du code précédent on passera un slice de type ```interface{}``` et on nous retournera une liste d'IDs enregistrés dans ```InsertManyResult.InsertedIDs```

Pour créer notre liste de documents, on va créer un slice de type **interface{}** et non ```Livre``` :

```go
lst := []interface{}{
    Livre{
        Titre: "L'art du zazen",
        Auteur: Auteur{
            Prenom: "Pierre",
            Nom:    "Crépon",
        },
        Editeur: "Albin Michel",
        Pages:   166,
    },
    Livre{
        Titre: "Les Argonautiques",
        Auteur: Auteur{
            Prenom: "Apollonios",
            Nom:    "de Rhodes",
        },
        Editeur: "Les Belles Lettres",
        Pages:   359,
    },
}
```

Le reste du code ne diffère pas de précédemment exceptés que MongoDB retourne un slice d'ID et non un seul ID. :

```go
rslt, err := coll.InsertMany(ctx, lst)
if err != nil {
    log.Printf("Erreur dans l'insertion des documents : %v", err)
} else {
    // on afficle l'ID automatiquement retourné à l'insertion
    fmt.Printf("Documents insérés : %v\n", rslt.InsertedIDs)
}
```

Code [Go](code6/main.go)

### 3-Lire UN document SANS options de filtrage

L'API JS traditionnelle permet de récupérer un document via sa fonction ```findOne()``` donc son équivalent Go est ```FindOne()```.

Cette méthode associée au type ```Collection``` nécessite au moins 2 arguments :

- un contexte
- un filtre : qui est un document à passer nous permettant de filtrer la recherche selon des critères
- des **options** comme skip, limit, sort...

Et cette méthode retourne un seul pointeur de type ```SingleResult```.

Puis on traitera le résultat en le décodant avec notre structure Livre : on appellera ```Decode(&pointeur)``` qui retournera une erreur.

Nous allons rechercher dans un premier temps le premier document.

Code :

```go
var livre Livre
err := coll.FindOne(ctx, bson.D{}).Decode(&livre)
if err != nil {
    log.Printf("Erreur dans le décodage : %v\n", err)
}
fmt.Printf("Résultat : %v\n", livre)
```

Ici on récupèrera le premier résultat enregistré dans la base.

Code [Go](code7/main.go)

### 4-Lire UN document AVEC filtrage

On passera en second argument un document de type ```bson.D``` :

```go
err = coll.FindOne(ctx, bson.D{{"titre", "Les Argonautiques"}}).Decode(&livre)
```

Avec VSCode, on aura un message d'alerte : *"primitive.E composite literal uses unkeyed fields"*.

Corrigeons en appelant le type structure ```primitive.E``` et en renseignant les deux champs disponibles :

```go
err = coll.FindOne(ctx, bson.D{
primitive.E{
        Key: "titre",
        Value: "Les Argonautiques"},
},
).Decode(&livre)
```

Si on souhaite chercher un auteur par son nom, on utilisera la syntaxe suivante pour le document :

```go
bson.D{
    primitive.E{
        Key:   "auteur.nom",
        Value: "Ackroyd"},
    },
}
```

**Attention aux majuscules !**

Code [Go](code8/main.go)

### 5-Lecture de plusieurs documents avec des options

Pour récupérer plusieurs documents l'API JS recours à ```find()``` donc son équivalent Go est ```Find()```.

Pour une présentation un peu plus élaboré, nous allons recourir aux options ! Notre base n'ayant pas un grand nombre de documents nous allons rechercher les deux premiers documents. En SQL nous disposons simplement de "LIMIT". C'est quasiment la même chose avec Go mais en plus complexe.

La première étape consiste à définir nos options : le driver Go dispose du package ```options```. A ce package on appelle la fonction ```Find()``` car c'est avec ```Find``` que l'on veut traiter. Cet appel crée une nouvelle instance de type ```FindOptions``` : une structure permettant d'affiner nos recherches.

Puis on va limiter avec l'appel de la méthode associée  : ```SetLimit()```. Celle-ci demande un argument de type int64 et retourne un pointeur de type ```FindOptions```.

Maintenant nous allons appliquer cette option à notre requête ```Find()```.

Code :

```go
opt := options.Find().SetLimit(2)
rslt, err := coll.Find(ctx, bson.D{{}}, opt)
```

Quel est le type de rslt ? C'est un curseur (```mongo.Cursor```) sur lequel nous allons itérer avec ```Next()```. Cette dernière méthode nécessite de passer un contexte en paramètre.

Dans cette boucle on va décoder le contenu avec la méthode ```Decode()``` qui nécessite le type ```Livre``` afin de pouvoir associer données et structure puis stocker les données reçues.

Code :

```go
for rslt.Next(ctx) {
    var livre Livre
    if err := rslt.Decode(&livre); err != nil {
        log.Printf("Erreur dans le décodage : %v\n", err)
            continue
    }
    fmt.Println(livre)
}
```

Une fois la boucle achevée on gère l'erreur possible de la méthode ```Find``` puis on ferme le curseur avec ```Close()``` et on lui passe à nouveau un contexte :

```go
if err = rslt.Close(ctx); err != nil {
        log.Printf("Erreur à la fermeture du curseur : %v\n", err)
    }
```

Code [Go](code9/main.go)

### 6-Update

Comme pour la recherche on peut mettre à jour un document ou plusieurs et comme toujours les méthodes employées sont explicites : ```UpdateOne()``` et ```UpdateMany()```

Pour illustrer leur utilisation respective, nous allons ajouter un nouveau champ avec ```UpdateMany``` puis modifier un champ avec ```UpdateOne```. Ce champ sera le numéro de tome.

#### UpdateMany

Avec ```UpdateMany```, nous allons rajouter un champ au document (rappel: [doc API officielle pour JS](https://docs.mongodb.com/manual/reference/method/db.collection.updateMany/))

Pour cela nous devons utiliser une option mais pas de filtrage. Cette option qui représente le troisième argument de ```UpdateMany()``` (après, dans l'ordre, le contexte et le filtrage) :

```go
update := bson.D{
        {"$set", bson.D{
            {"NumTome", 1},
        }},
}
```

Il peut y avoir un avertissement du compilateur : éviter les littéraux et utiliser le type ```primitive.E``` (ce qui est le cas pour le code final).

Le retour d'```UpdateMany``` est un pointeur de type ```mongo.UpdateResult``` et une erreur traditionnelle. On peut récupérer 4 valeurs :

- ```MatchedCount``` : nombre de documents qui correspondent à la recherche
- ```ModifiedCount``` : nombre de documents modifiés
- ```UpsertedCount``` : nombre de documents dits "upserted" (mis à jour et insertion de données)
- ```UpsertedID``` : une interface retournant nil ou un ID pour l'*upsert*

```go
rslt, err := coll.UpdateMany(ctx, bson.D{}, update)

// Gestion de l'erreur

fmt.Printf("Nombre de documents mis à jour : %v ; nombre trouvés : %v\n", rslt.ModifiedCount, rslt.MatchedCount)
```

Code [Go](code10/main.go)

#### UpdateOne

Le fonctionnement est identique à UpdateOne sauf que s'il existe plusieurs documents pouvant être modifiables seul le premier trouvé sera modifié !

Donc ```UpdateOne()``` prend trois paramètres :

- contexte
- filtre
- options

Et elle retourne :

- un pointeur de type ```UpdateResult```
- une erreur

Et comme précédemment on aura des infos sur le nombre de documents trouvés, modifiés et les upserts.

**NB** il existe le cas de ```ReplaceOne()``` le fonctionnement est quasi identique au précédent mais il dispose d'un argument supplémentaire : un document de remplacement. Cf. [Doc officielle](https://pkg.go.dev/go.mongodb.org/mongo-driver/mongo?tab=doc#Collection.ReplaceOne)

## XXXX-Références

Dépôt officiel est sur [GitHub.com](https://github.com/mongodb/mongo-go-driver/)

Doc officielle du driver sur [pkg.go.dev](https://pkg.go.dev/go.mongodb.org/mongo-driver/mongo?tab=doc)
