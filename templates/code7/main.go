package main

import (
	"log"
	"os"
	"text/template"
)

var pays = map[string]string{
	"France":    "Paris",
	"Allemagne": "Berlin",
	"Belgique":  "Bruxelles",
	"Danemark":  "Copenhague",
	"Irlande":   "Dublin",
}

func main() {

	tpl, err := template.ParseFiles("fichier.txt")

	if err != nil {
		log.Fatalf("Erreur sur l'analyse du fichier : %v", err)
	}

	if tpl.Execute(os.Stdout, pays) != nil {
		log.Fatal("Erreur sur le traitement des données.")
	}
}
