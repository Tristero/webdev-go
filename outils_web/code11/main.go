package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
)

func main() {
	fmt.Println("Lancement serveur Go-AJAX")

	http.HandleFunc("/", racine)
	http.HandleFunc("/ajax", ajax)

	http.Handle("favicon.ico", http.NotFoundHandler())
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func racine(rep http.ResponseWriter, req *http.Request) {

	f, err := ioutil.ReadFile("index.html")
	if err != nil {
		log.Printf("Erreur dans l'ouverture du fichier : %v", err)
	}

	rep.Header().Set("Content-Type", "text/html; charset=utf-8")
	rep.Write(f)
}

func ajax(rep http.ResponseWriter, req *http.Request) {
	log.Println("Appel de la requête /ajax")
	fmt.Fprint(rep, "Hello from Ajax !")
}
