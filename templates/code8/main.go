package main

import (
	"log"
	"os"
	"text/template"
)

type personne struct {
	Nom    string
	Prenom string
}

func main() {
	jd := personne{
		Prenom: "John",
		Nom:    "Doe",
	}

	tpl, err := template.ParseFiles("fichier.txt")
	if err != nil {
		log.Fatalf("Erreur sur le parsing :%v", err)
	}

	if tpl.Execute(os.Stdout, jd) != nil {
		log.Fatalf("Erreur sur le traitement des données")
	}

}
