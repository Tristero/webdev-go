# Amazon Web Services

Dans l'univers du cloud computing, on dispose de nombreuses plateformes : AWS, MS Azure, Google Cloud, Alibaba Cloud, OVH Cloud...

Et nous ferons le choix du plus important : Amazon et ses services web.

## 1-Création d'une instance AWS EC2

Il faut créer un compte gratuit et donner son numéro de carte bancaire pour utiliser les services lorsque ceux-ci ont été utilisés soit parce que l'on a utilisé un service ne faisant pas partie de l'essai gratuit, soit parce que l'on a dépassé le niveau de base.

Une fois l'inscription passée, on se connecte au service EC2. Si on dispose d'un compte on se connecte soit en mode racine (si on est tout seul) soit dans le second mode si on appartient à un groupe d'utilisateur.

On va créer une instance : "lancer une instance". Puis il faudra choisir une image système : bien sélectionner les images gratuites pour l'essai. En fonction de sa connaissance de l'univers Linux, on prendra Ubuntu si on est débutant sinon on se laissera guider par ses propres choix. Ensuite il faudra sélectionner une instance aussi éligible pour l'essai gratuit : "t2 micro". Et on laissera les paramètres par défaut.

Avec l'essai gratuit d'un an, on bénéficie d'un espace de stockage de 30Go : on laissera à 8Go parce qu'on utilisera pas autant d'espace pour nos essais.

Il sera demandé une paire de clé-valeur représentant le serveur : on pourra donner un nom "web-serveur-23-04-2020", par exemple.

Il faudra configurer le groupe de sécurité : on clique sur "modifier" puis on crée un nouveau groupe de sécurité si c'est la première fois que l'on crée une instance EC2. On peut si l'on souhaite changer le nom du groupe de sécurité. Normalement on dispose d'un accès SSH prédéfini : on laisse ses valeurs par défaut. Et on rajoute un autre Type : HTTP. On ne touche pas au protocole TCP ni au Port 80.

Il ne reste plus qu'à lancer l'instance : là sera demandé de choisir ou de créer une paire de clé-valeur. Nous allons créer une nouvelle paire : on donne un nom de son choix au fichier que l'on va récupérer puis on télécharge la paire générée (un fichier PEM). Il faudra bien la conserver parce qu'il ne sera plus possible de la télécharger à nouveau ! Puis on lancera l'instance.

Avec le fichier PEM reçu (clé privée), on lui change ses droits : ```chmod 400 fichier.pem``` afin d'éviter toute écriture sur ce dernier. On déplacera le fichier PEM dans le dossier caché local "~/.ssh" pour plus de sécurité.

## 2-Déploiement

Pour notre premier projet à déployer, nous allons faire très simple pour bien assimiler le fonctionnement du déploiement sur AWS.

On va créer un petit serveur web qui affichera un simple message :

```go
func main() {
    http.HandleFunc("/", racine)
    http.ListenAndServe(":80", nil)
}

func racine(rep http.ResponseWriter, req *http.Request) {
    fmt.Fprint(rep, "<p>Mon premier serveur sur AWS</p>")
}
```

On n'affichera pas de log. On n'utilisera pas de template pour l'instant.

Code [Go](code1/main.go)

### 1-Compilation

Go permet la cross-compilation : cela signifie que l'on peut compiler pour Linux, Windows, Mac, ... version x64, ARM sans avoir à utiliser un autre compilateur, un autre système. Pour Windows, on lira cet article du [Wiki GO](https://github.com/golang/go/wiki/WindowsCrossCompiling). Toujours pour Windows, on aura peut-être intérêt d'installer WSL et d'une distribution Linux de son choix : on disposera alors de tous les outils Linux.

 Or il va falloir compiler pour Linux 64bits : donc on va modifier temporairement l'OS visé par le compilateur et l'architecture matérielle. Donc dans le shell qui va nous servir à la compilation et positionné dans le répertoire du code source :

```sh
GOOS=linux GOARCH=amd64 go build -o serveur main.go
```

L'option ```-o``` est optionnel est doit être présent en premier suivi du nom du binaire à générer.

### 2-Upload du binaire

Pour cela il faut disposer de l'application ```scp```. Pour l'utiliser il nous faut disposer du DNS de l'instance disponible depuis la console EC2 : si l'on dispose d'une seule instance on regarde dans la partie inférieure de la console et l'on aura l'adresse complète du DNS public.

Nous allons envoyer une copie de notre binaire en utilisant notre fichier PEM pour s'identifier sur l'instance et pour cela nous allons utiliser le DNS public de l'instance

```txt
scp -i ~/.ssh/[nom du fichier].pem ./[nom du binaire avec le chemin complet si besoin] [nom de l'utilisateur]@[ec2-...compute.amazonaws.com]:
```

Si on a suivi nos instructions précédentes la commande peut ressembler à :

```sh
scp -i ~/.ssh/XXXX.pem serveur ubuntu@ec2-XXXXXXXXXX.compute.amazonaws.com:
```

**Remarques** :

- on remplacera les X par les valeurs qui ont été choisies lors de la création de l'instance, et de la récupération des clés.
- **ATTENTION** à ne pas oublier le ":" final !!!
- le nom de l'utilisateur est le nom de l'OS : ici Ubuntu

Si c'est la première connexion : on tapera "yes" indiquant que l'on veut se connecter au serveur distant.

### 3-SSH

Il nous faut maintenant nous connecter à notre instance avec SSH et pour cela il nous faut utiliser notre fichier PEM. La commande ressemble à la précédente :

```sh
ssh -i ~/.ssh/XXXXXXX.pem ubuntu@ec2-XXXXXXXXX.compute.amazonaws.com
```

**ATTENTION** à ne PAS terminer la commande avec les deux points !

Et l'on se retrouve avec le shell de notre instance : on peut vérifier que notre binaire est présent : ```ll```.

Normalement le binaire est exécutable si ce n'est pas le cas : un simple ```chmod +x serveur``` devrait le rendre exécutable, ou pour plus de sécurité : ```chmod 100 binaire``` (juste exécutable).

### 4-Test

Nous n'avons pas fermé notre connexion SSH : nous allons lancer notre binaire avec ```sudo ./serveur```.

Sur la console web EC2, nous devons copier l'adresse IP publique IPv4 ; puis dans un navigateur web on collera l'adresse IP publique. Et on observera notre message s'affichera si tout est OK.

On sort de notre serveur avec Ctrl + C puis on tape ```exit``` pour quitter notre session SSH.

Le problème c'est l'absence de persistence du serveur : on doit conserver notre connexion SSH !

## 3-Persistance

Le problème est de pouvoir lancer une application sans avoir à se connecter en SSH et de conserver sa session. Ceci se résout avec un système de gestion de processus tel que systemd ou SysV init. Mais un nouveau problème apparait : il faut savoir quel est le gestionnaire utilisé par la distribution Linux. Avec Debian/Ubuntu, c'est systemd.

Pour notre projet, nous allons donc recourir à systemd. Ce gestionnaire est plus simple à utiliser que son concurrent. Nous devons créer un fichier de configuration pour pouvoir lancer notre application. Ces fichiers portent l'extension ```.service``` et sont stockés dans ```/etc/systemd/system``` (on remarquera un bon nombre sont des liens symboliques).

### 1-Création du service

On se crée un fichier ```serveur_go.service``` par exemple dans le répertoire ```/etc/systemd/system``` contenant :

```txt
[Unit]
Description=Serveur Go

[Service]
ExecStart=/home/ubuntu/serveur
User=root
Group=root
Restart=always

[Install]
WantedBy=multi-user.target
```

Quelques commentaires :

- pour pouvoir utiliser le port 80 il faut que l'utilisateur et le groupe soit ```root```.
- en cas de crash du système, au redémarrage de ce dernier, notre service sera aussi redémarrer
- la section ```WantedBy``` indique le *runlevel* du lancement de l'application (notre serveur fonctionnera en mode multi-utilisateur sans affichage graphique)

### 2-Commandes systemd

- Ajouter le service : ```sudo systemctl enable serveur_go.service```
- Vérifier le status du service : ```sudo systemctl status serveur_go```
- Activer le service : ```sudo systemctl start serveur_go```
- Arrêter le service : ```sudo systemctl stop serveur_go```

Pour supprimer totalement le service : [lien "superuser"](https://superuser.com/questions/513159/how-to-remove-systemd-services)

En mode super-utilisateur (sudo):

```sh
systemctl stop [nom du service]
systemctl disable [nom du service]
rm /etc/systemd/system/[nom du service]
systemctl daemon-reload
systemctl reset-failed
```

Code [service](code2/serveur_go.service)

Exercice : reprendre le code [8 du répertoire des sessions](../sessions/code8). Puis compiler le code en modifiant le port 8080 en 80. Puis transférer le binaire générer ET les fichiers HTML vers l'instance. Donc il faudra aussi gérer le service (l'arrêter), gérer le binaire déjà installé sur l'instance EC2 (est-ce qu'on veut l'écraser, le supprimer ou le renommer ?). On peut aussi générer un binaire avec un autre nom mais cela signifie qu'il faudra aussi modifier le fichier de configuration du service !

Pour la commande SCP on pourra transférer tous les fichiers templates html avec ```*.html```

L'objectif est que notre serveur fonctionne. Donc on testera d'abord sans la persistance via une session SSH. On lancera notre serveur avec sudo. Si tout fonctionne à merveille, on effectuera la persistance. Pour cela il faut modifier le fichier de service en rajoutant dans la section ```[Service]``` la ligne suivante : ```WorkingDirectory=/home/ubuntu/<mettre ici le nom du répertoire où est notre serveur et les fichiers utiles>```. Par exemple, si avec notre configuration de base : ```WorkingDir=/home/ubuntu```

Toute modification du fichier nécessitera la relance :

```sh
sudo systemctl daemon-reload
```

Puis on lancera le service et on vérifiera que le service fonctionne (avec ```status```), puis avec le navigateur.

Nous avons laisser les messages de log : on peut y accéder grâce à ```systemctl status serveur_go```.

Exercice : placer les templates dans un dossier. Attention à la modification du code source : faut-il modifier seulement le pointeur générer avec ```ParseGlob()``` ? ou bien les autres références ? donc on devra bien tester localement le comportement de l'application (attention au port !!!)

Pour SCP, on créera un répertoire sur l'instance "templates" et on copiera les fichiers locaux vers le répertoire distant.

**Dans tous les cas** : on fera attention à la compilation du binaire si son OS local n'est pas un Linux !

## 4-Arrêt de l'instance

Il faut faire très attention aux coûts même si avec l'essai gratuit on dispose de 750 heures de calcul. Pour cela il faut se connecter sur la console web des instances EC2. On sélectionne l'instance à arrêter. Puis on clique sur le bouton "Actions" puis "Etat de l'instance" puis arrêter.

Pour savoir si l'on a eu des frais : on clique sur son nom d'utilisateur (en haut à droite) puis "Tableau de bord de ma facturation".

Attention : si on redémarre l'instance les DNS et IP publics seront nouveaux !

Pour détruire son instance : il suffit de sélectionner "résilier" dans "Actions" > "Etat de l'instance".
