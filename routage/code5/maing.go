package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

func index(rep http.ResponseWriter, req *http.Request, _ httprouter.Params) {
	fmt.Fprintln(rep, "Hello world")
}

func main() {
	fmt.Println("Lancement du serveur multiplex v5")
	mux := httprouter.New()

	mux.GET("/", index)
	log.Fatal(http.ListenAndServe(":8080", mux))
}
