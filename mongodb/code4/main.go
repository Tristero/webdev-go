package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func main() {

	// Création du client
	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://localhost:27017"))
	if err != nil {
		log.Fatalf("Erreur à la création du client : %v", err)
	}
	log.Println("Création du client réussie -> Connexion ?")

	// Connexion
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	if err := client.Connect(ctx); err != nil {
		log.Fatalf("Erreur à la connexion : %v", err)
	}
	log.Println("Connexion réussie -> Ping ?")

	if err := client.Ping(ctx, nil); err != nil {
		log.Fatalf("Erreur ping : %v", err)
	}
	log.Println("Ping réussi.")

	// Requête : lister toutes les collections
	r, err := client.Database("local").ListCollectionNames(ctx, bson.D{}, nil)
	if err != nil {
		log.Printf("Erreur sur le listage des collections : %v", err)
	}
	for _, col := range r {
		fmt.Printf("\t%s\n", col)
	}

	// déconnexion
	if err := client.Disconnect(ctx); err != nil {
		log.Fatalf("Erreur à la déconnexion : %v", err)
	}

	fmt.Println("Déconnexion")
}
