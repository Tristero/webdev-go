# Go et les bases de données

Nous allons nous consacrer ici uniquement aux bases relationnels SQL. Les bases NoSQL seront étudiées ultérieurement.

Pré-requis : connaître ce que sont les bases de données relationnelles et les bases du langage SQL. Avoir installé MySQL/MariaDB en local (ou sur AWS : l'offre gratuite propose 750h de calcul).

Pour la base de données MySQL/MariaDB en local voici les pré-requis: [base.md](base.md). Donc on aura une base dénommée : ```bogase``` ; un utilisateur ```gopher``` ayant pour mot de passe ```gopherpswd``` et un accès unique en ```localhost``` à cette base.

## 1-Package database/sql et driver MySQL

Ce package permet de fournir une "interface" commune, indépendante des serveurs de base de donnée sur lequel des drivers seront élaborés (le driver que nous utiliserons est une implémentation de ```database/sql/driver```). Il ne s'agit pas d'une obligation mais d'une recommandation à suivre. L'avantage est que le code ne changera pas beaucoup entre les différents serveurs (seulement l'appel du driver idoine et du code SQL).

On dispose sur le Wiki Go d'une liste de drivers suivant les spécifications avec plus ou moins de respect [lien](https://github.com/golang/go/wiki/SQLDrivers). Pour MySQL/MariaDB on dispose de trois drivers ! Nous utiliserons : [https://github.com/go-sql-driver/mysql/](https://github.com/go-sql-driver/mysql/)

En utilisant ce driver et le package ```database/sql```, nous allons pouvoir communiquer avec le serveur en ne recourant qu'à la documentation de ```database/sql``` et à son [pendant wiki](https://github.com/golang/go/wiki/SQLInterface).

Soit on installe une base locale, soit on utilise AWS RDS (offre gratuite comprenant 750 heures de calcul). On créera un utilisateur si besoin : "user" avec un mot de passe de son choix associé à une base ```golang``` par exemple.

## 2-Gestionnaire de base

Quelques fonctions utiles :

- ```sql.Open(nomDuDriver string, srcDeDonnée string)``` : retourne un pointeur de type ```DB``` et une erreur. Ce pointeur n'ouvre pas de connexion à la base. Quant à la source de données elle doit se constituer d'élément propre à la connexion vers un serveur (nom, adresse, port) ainsi que le nom de la base.
- ```db.Ping()``` vérifie si la connexion est possible : ne retourne qu'une erreur

**NB** : la syntaxe de la source de données est selon la documentation du driver : ```username:password@protocol(address)/dbname?param=value```. Donc si l'on a suivi nos pré-requis, on aura la chaîne suivante : ```gopher:gopherpswd@tcp(localhost)/gobase``` que l'on pourra raccourcir (si et seulement on est connecté en TCP sur le localhost au port 3306) : ```gopher:gopherpswd@/gobase```

Concernant la connexion : celle-ci s'effectuera en différé lors d'un requêtage.

Notre premier code va créer un pointeur de type ```DB``` (gestionnaire de la base) puis on testera la connexion et on cloturera via un ```defer```.

Code :

```go
func main() {
    // création d'un pointeur de gestion de base
    db, err := sql.Open("mysql", "gopher:gopherpswd@/gobase")
    check(err, "à la création du pointeur")

    // diffère la fermeture
    defer db.Close()

    // Vérification de la connexion
    err = db.Ping()
    check(err, "au Ping")

    // Enfin si tout est OK :
    fmt.Println("Connexion existante !")
}
```

Remarque concernant la fonction ```check``` : c'est une fonction helper simple qui évite un code redondant sur les erreurs.

L'appel du driver est préfixé avec un ```_``` : cela signifie que nous n'utiliserons pas directement le driver mais que le package "database/sql" doit le précharger pour pouvoir, lui, l'utiliser.

Code complet [Go](code1/main.go)

## 3-Interagir avec les données

Objectif : nous allons créer un serveur web avec différentes routes. Chacune de ces routes engendrera une action sur le serveur de base de données de type ```CRUD``` (create-read-update-delete) ainsi que l'utilisation de ```DROP``` et de ```CREATE``` pour les tables.

### 1-Code de base

On va créer une variable globale pour le pointeur de gestion de la base, une fonction-helper pour gérer les erreurs et les afficher simplement, puis on va placer les premiers éléments de code :

- gestion de la connexion à la base
- lancement du serveur web avec l'implémentation de la racine du site

Code :

```go
var db *sql.DB

func main() {
    // Gestion de la base
    var err error
    db, err = sql.Open("mysql", "gopher:gopherpswd@/gobase")
    check(err, "")
    defer db.Close()

    // Vérification de la connexion
    err = db.Ping()
    check(err, "à la tentative de connexion")

    // Gestion des routes
    http.HandleFunc("/", racine)
    // Gestion du serveur
    fmt.Println("Lancement du serveur 1")
    http.Handle("/favicon.ico", http.NotFoundHandler())
    log.Fatal(http.ListenAndServe(":8080", nil))
}

func racine(rep http.ResponseWriter, req *http.Request) {
    fmt.Fprint(rep, "Racine du site")
}

func check(e error, msg string) {
    if e != nil {
        log.Printf("Erreur %s : %v", msg, e)
    }
}
```

### 2-Création d'une table

Objet : Comment créer une table ?

On va utiliser ```Exec()``` que l'on appellera depuis notre variable pointeur globale ```db```. Cette fonction sera utile lorsque l'on utilisera une requête SQL qui ne retourne pas de tuples (de ```rows```).

Cette fonction ```Exec()``` retourne deux valeurs :

- un type ```Result```
- une erreur

Le type ```Result``` retourné nous permet d'accéder à deux éléments :

- ```LastInsertId()```
- ```RowsAffected()```

Dans les deux cas, elles retournent un entier ```int64``` et une erreur.

Pour l'instant nous ne nous occuperons que de ```RowsAffected```.

Code :

```go
func create(rep http.ResponseWriter, req *http.Request) {
    var e error
    r, e := db.Exec("CREATE TABLE clients (nom VARCHAR(80), prenom VARCHAR(80));")
    check(e, "à la création de la table")

    i, e := r.RowsAffected()
    check(e, "sur RowsAffected")

    if e == nil {
        fmt.Fprintf(rep, "Nombre de rows : %d", i)
    } else {
        http.Error(rep, e.Error(), http.StatusInternalServerError)
    }
}
```

Pour tester, on n'oubliera pas de rajouter la route "/create" à ```main.go```, de vérifier que le serveur MySQL/MariaDB fonctionne.

Si tout c'est bien déroulé (le message affiché sera ```Nombre de rows : 0```), on vérifiera en se connectant à MySQL si la table a été bien créée :

```sh
mysql -u gopher -p
```

Puis dans le client :

```sql
USE gobase;
SHOW tables;
DESCRIBE clients;
```

Code [Go](code2/create.go)

### 3-Ajouter des données

Objet : insérer de nouvelles données dans la base avec la commande SQL ```INSERT```.

Dans le cas précédent on a utilisé ```Exec()```. Ici nous allons deux méthodes :

- ```Prepare()``` qui prend en argument une string représentant une requeête SQL et retourne un pointeur de type ```Stmt``` et une erreur.
- ```Exec()``` qui est appelé depuis le pointeur ```Stmt``` créé et à qui on passe les valeurs à enregistrer dans la base. Elle nous retournera un type ```Result``` et une erreur.

Notre requête SQL ressemblera à : ```insert into clients(nom, prenom) values (?, ?)``` que nous allons passer en argument de ```Prepare()```. Cette méthode est disponible depuis le pointeur de gestion de base (la variable globale ```db```).

Cet appel va nous permettre de faire plusieurs requêtes et cela, même concurrentiellement ! Mais pour pouvoir insérer les données, nous devons récupérer le pointeur de type ```Stmt```.

Le type ```Stmt``` est une déclaration ou *statement* préparée. Ce type permet d'utiliser la concurrence en toute sécurité.

C'est sur ce type que l'on va pouvoir associé données et requête et d'exécuter la requête avec ```Exec()```.

Cette méthode ```Exec()``` prend en arguments toutes les valeurs nécessaires pour la requête et retourne les mêmes types qu'```Exec()``` pour la création de la table.

**ATTENTION** on a deux méthodes bien différentes : pour la création de la table ```Exec()``` est une méthode du type ```DB``` et pour l'insertion pour le type ```Stmt```. Même si les éléments retournés sont identiques : ```Result``` et ```error``` !

Nous allons pour notre projet et exemple créer une structure :

```go
data := []struct {
    prenom string
    nom    string
}{
    {"John", "Doe"},
    {"Haskell", "Curry"},
    {"Pike", "Rob"},
    {"Grace", "Hopper"},
}
```

On va ensuite préparer notre requête qui sera appelée quatre fois :

```go
stmt, err := db.Prepare("insert into clients(nom, prenom) values (?, ?)")
```

Et après la gestion de l'erreur on va appelé ```defer``` afin de clôre le pointeur de type ```Stmt```.

Puis on itère sur notre slice de structs et on utilise ```Exec()``` en passant le nom et le prénom de l'élément en cours d'itération :

```go
for _, elem := range data {
    r, err := stmt.Exec(elem.nom, elem.prenom)
    check(err, "sur l'insertion de données")

    i, err := r.RowsAffected()
    check(err, "sur le RowsAffected")
}
```

Puis on affichera un message pour le navigateur.

**ATTENTION** il faut bien penser à appeler ```Close()``` sur le pointeur ```Stmt``` !

Code complet [Go](code2/insert.go)

### 4-Lecture des données

Objet : afficher le contenu de notre base de données sur le navigateur

La documentation officielle nous indique que pour des requêtes retournant des enregistrements, nous devons employer ```Query()```. C'est méthode accessible depuis le pointeur ```DB```.

En argument, cette méthode prend une requête et une serie d'arguments. Ces arguments multiples sont utiles si l'on doit passer des valeurs particulières à intégrer dans la requête : par exemple, on dispose d'une requête dont les résultats dépendent d'un âge. Cet âge sera passé en argument et remplacé par ```Query()``` ([cf. exemple de la documentation](https://golang.org/pkg/database/sql/#example_DB_Query_multipleResultSets))

Ici nous allons chercher toutes les valeurs simplement.

```go
rows, err := db.Query("select prenom, nom from clients")
```

```Query()``` retournera un pointeur ```Rows``` et une erreur. Le type ```Rows```va nous permettre de parcourir les données : c'est un curseur. On parcourera les enregistrements avec ```Next()``` qui retournera un booléen.

**ATTENTION** il faudra à la fin de l'utilisation du pointeur ```Rows``` appelé ```Close()``` : on utilisera ```defer```.

Pour récupérer les données, on doit créer des variables dans lesquelles seront enregistrées les données :

```go
for rows.Next() {
    var (
        prenom string
        nom    string
    )

    err := rows.Scan(&prenom, &nom)
    check(err, "du scan des de l'enregistrement")
}
```

Code complet [Go](code2/read.go)

*Remarque* : il faut faire attention à bien mettre en place la route dans ```main.go```

En exercice : on pourra modifier [create.go](code2/create.go) pour afficher le résultat de ```SHOW TABLES```.

### 5-Mise à jour de données

Objet : modifier un client avec la commande SQL ```UPDATE```.

Comme pour la création d'un élément, nous allons recourir à ```Exec``` car nous ne récupérerons que le nombre de données écrite avec le type ```Result``` retournée par ```Exec()```.

```go
func update(rep http.ResponseWriter, req *http.Request) {
    r, err := db.Exec("UPDATE clients SET prenom=? WHERE nom=?", "Jane", "Doe")
    check(err, "sur la requête update")

    nb, err := r.RowsAffected()
    check(err, "sur la récupération de données affectées")

    fmt.Fprintf(rep, "Nombre de valeurs mises à jour : %v", nb)
}
```

*Remarque* : nous aurions pu rédiger notre requête directement sans l'intervention d'une substitution des ```?``` par des valeurs.

On se redirigera vers [http://localhost/read](http://localhost/read) pour observer les modifications.

Code [Go](code2/update.go)

### 6-Suppression d'une donnée

Objet : supprimer un client avec la commande ```DELETE```

Le principe est le même que le précédent : nous n'avons pas d'enregistrements (*tuples* de résultats).

Donc nous utiliserons à nouveau ```Exec()```. Et nous afficherons le nombre de données effacées.

```go
func delete(rep http.ResponseWriter, req *http.Request) {
    r, err := db.Exec("DELETE FROM clients WHERE nom=?", "Doe")
    check(err, "sur la requête de suppression")

    nb, err := r.RowsAffected()
    check(err, "sur la récupération du nombre de suppression")

    fmt.Fprintf(rep, "Nombre de suppression : %d", nb)
}
```

Après la suppression on se redirigera vers [/read](http://localhost/read).

Code [Go](code2/delete.go)

### 7-Dropper une table

Objet : suppression de la table "clients".

La commande SQL est très simple : ```DROP TABLE clients```

Nous pouvons encore une fois utiliser ```Exec()``` mais nous allons utiliser en alternative la méthode ```Prepare()``` suivi de ```Exec()``` associée au pointeur de type ```Stmt```. Donc ici il faudra penser à "fermer" le pointeur ```Stmt``` créé par ```Prepare()``` ! Que passer à ```Exec()``` ? on y place rien. Si on plaçait un ```?``` dans ```Prepare()``` et que l'on passait la valeur ```clients``` dans ```Exec``` nous aurons une erreur fatale : adresse invalide ou un déréférencement d'un pointeur nil !

Le processus est plus long et source de bug : c'est pourquoi il vaut mieux réserver ce processus pour des requêtes multiples.

Si on utilise ```RowsAffected()``` pour récupérer un résultat, on obtient 0. Donc on va s'abstraire de tout résultat et informer du bon traitement de l'opération ! 

Code  [Go](code2/drop.go)
