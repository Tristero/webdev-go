# Servir des fichiers

Comment proposer des fichiers : avec ```io.Copy()```, ```ServeContent``` et ```ServeFile``` (du package ```http```), puis avec ```http.FileServer```. Puis nous verrons ```http.NotFoundHandler```

## 1-Servir un fichier avec io.Copy

Si l'on souhaite afficher un contenu provenant d'un autre site (telle une image) : on peut très bien utiliser un code HTML ```<img src="http://serveur.com/image.jpg>```  puis on envoie notre réponse. C'est la solution la plus simple :

```go
func envoiImg(rep http.ResponseWriter, req *http.Request) {
    rep.Header().Set("Content-Type", "text/html")

    fmt.Fprintf(w, `<img src="/chemin/vers/mon-image.jpg">`)
}

func main() {
    http.HandleFunc("/", envoiImg)
    http.ListenAndServe(":8080", nil)
}
```

Cette solution  fonctionne aussi lorsque l'image est locale.

Mais on peut utiliser ```io.Copy()``` qui prend deux arguments :

- un type implémentant ```io.Writer```
- un type implémentant ```io.Reader```

Et en retour on a deux valeur :

- un entier 64 bits
- une erreur

L'entier ne nous intéresse pas (il retourne le nombre d'octets).

On procédera d'abord par ouvrir le fichier image avec ```os.Open``` puis on écrira avec ```io.Copy``` directement dans notre variable ```http.ResponseWriter```.

```go
func imgLocale(rep http.ResponseWriter, req *http.Request) {

    f, err := os.Open("../images/img.png")
    if err != nil {
        http.Error(rep, "Fichier introuvable", 404)
        log.Fatalf("Erreur fichier image : %v", err)
    }
    defer f.Close()

    _, err = io.Copy(rep, f)
    if err != nil {
        log.Fatalf("Erreur de copie : %v", err)
    }

}
```

Code [Go](code1/main.go)

## 2-ServeContent et ServeFile

### ServeContent

dépend du package ```http``` et sa signature est un peu plus complexe car elle demande 5 paramètres :

- une réponse de type ```http.ResponseWriter```
- un pointeur de requête ```http.Request```
- un nom (nom du fichier) sous forme d'une chaîne
- un type ```time.Time``` indiquant la dernière date de modification du fichier
- un contenu de type ```io.ReadSeeker``` : ce dernier va écrire pour nous le ```Content-Type``` en analysant le contenu du fichier.

Concrètement, comment procéder ?  les 3 derniers éléments de la fonction seront comme suit :

- ```Name()``` utilisé via le pointeur ```os.File```
- ```ModTime()``` utilisé via l'appel de ```Stat()``` sur le pointeur ```os.File```
- et le pointeur ```os.File```

Code :

```go
func img(rep http.ResponseWriter, req *http.Request) {
    fichier, err := os.Open("../images/img.png")
    if err != nil {
        http.Error(rep, "Erreur sur le fichier", 404)
        log.Fatalf("Fichier inconnu : %v", err)
    }

    defer fichier.Close()

    fs, err := fichier.Stat()
    if err != nil {
        http.Error(rep, "Erreur sur la récupération des infos du fichier", 404)
        log.Fatalf("Erreur sur la récup d'info du fichier %v", err)
    }

    http.ServeContent(
        rep,
        req,
        fichier.Name(),
        fs.ModTime(),
        fichier,
    )
}
```

NB : pour récupérer le nom du fichier on dispose de deux méthodes ```Name``` dont les résultats diffèrent

- ```fichier.Name()``` : retourne le nom du fichier (avec son extension) et du chemin d'accès vers le fichier
- ```fs.Name()``` : retourne seulement le nom du fichier (avec son extension).

Dans les deux cas, le serveur trouvera le fichier.

Code [Go](code2/main.go)

### ServeFile

c'est le pendant de la fonction précédente : elle va s'occuper des opérations de récupération des données pour nous. Sa signature est nettement plus concise :

- un type ```http.ResponseWriter```
- un type ```http.Request```
- le nom du fichier à récupèrer sous forme de chaîne

Donc notre code précédent se résumera simplement à :

```go
func img(rep http.ResponseWriter, req *http.Request) {
    http.ServeFile(rep, req, "../images/img.png")
}
```

Code [Go](code3/main.go)

## 3-FileServer

Contrairement aux deux fonctions précédentes, ```FileServer``` va nous permettre de servir plusieurs fichiers. Et son utilisation est très simple. Voici l'exemple donné par la documentation [doc](https://pkg.go.dev/net/http?tab=doc#FileServer)

```go
http.Handle("/", http.FileServer(http.Dir("/tmp")))
```

```file.FileServer``` prend en argument tout objet implémentant une interface ```http.FileSystem```. Nous avons passé une fonction ```Dir``` qui implémente ```http.FileSystem``` et qui va rechercher dans le système de fichier local un répertoire (passé en argument à la fonction sous forme de chaîne). ```FileServer``` retournera un objet ```Handler```

Que se passera-ti-il ? Le code nous génèrera une liste de lien vers les fichiers image.

Code [Go](code4/main.go) : on trouvera une version condensé (pas d'appel de ```http.Handle```)

## 4-StripPrefix()

Permet de rediriger une URL vers un autre répertoire que celui indiqué par l'URL. Il s'agit d'une redirection des toutes les URL contenant le motif de préfixe (même dans le HTML) : toutes les URLs seront automatiquement redirigées.

Cette fonction retourne un ```Handler```. Et prend deux arguments :

1 - préfixe (chaîne de caractère)
2 - un type ```Handler```

Donc si on tape une URL avec "/ressources/" on va pouvoir la rediriger vers une autre ressource.

Exemple :

```go
func main() {
    fmt.Println("Lancement serveur de fichiers v5.")
    http.Handle(
        "/ressources/",
        http.StripPrefix("/ressources", http.FileServer(http.Dir("../images/"))),
    )

    http.ListenAndServe(":8080", nil)
}

```

Test : on ouvre le lien suivant [http://localhost:8080/ressources/](http://localhost:8080/ressources/) et on observe que l'on est redirigé vers le répertoire image : mais il n'y a pas de modification de l'URL !

Un autre exemple montrant une redirection des toutes les URLs : pour cela nous allons créer une nouvelle fonction pour le document racine qui dirige vers un dossier (inexistant : "./ressources") et nous redirigerons les URLs vers le bon répertoire ("../images")

```go
func main() {
    fmt.Println("Serveur de fichier v6")

    http.HandleFunc("/", root)
    http.Handle("/ressources/",
        http.StripPrefix("/ressources/",
            http.FileServer(http.Dir("../images/"))),
    )

    http.ListenAndServe(":8080", nil)
}

func root(rep http.ResponseWriter, req *http.Request) {
    rep.Header().Set("Content-type", "text/html; charset=utf-8")
    fmt.Fprintln(
        rep,
        `<img src="/ressources/img2.png">`,
    )
}
```

On pourra tester les deux URLs :

- [http://localhost:8080/](http://localhost:8080/)
- [http://localhost:8080/ressources/](http://localhost:8080/)

Tout est bien redirigé.

Code [Go](code6/main.go)

## 5-Création d'un serveur de fichiers statiques

Nous allons créer un fichier HTML quelconque que nous nommerons ```index.html``` et nous allons proposer un serveur de fichiers statiques :

```html
<body>
    <h1>Hello Gophers</h1>
    <img src="images/img3.png" alt="Gophers">
</body>
```

Lorsque l'on propose à servir un fichier ```index.html```, le contenu du répertoire n'est plus affiché : c'est ```index.html``` qui est affiché par défaut !

**ATTENTION** comme nous avons placé le répertoire images à un niveau supérieur par rapport au répertoire des codes Go et HTML, on va devoir faire une redirection très simple (sinon les images ne s'afficheront pas) :

```go
func main() {

    fmt.Println("Serveur de fichiers statiques v7")

    http.Handle(
        "/images/",
        http.StripPrefix(
            "/images/",
            http.FileServer(http.Dir("../images/")),
        ),
    )

    http.Handle("/", http.FileServer(http.Dir(".")))

    http.ListenAndServe(
        ":8080",
        nil,
    )
}
```

Code [Go](code7/main.go) / [HTML](code7/index.html)

En revanche, si les répertoires sont conçus selon une hiérarchie suivante :

```txt
site/
  |____index.html
  |____main.go
  |____./rsc/
           |_____img2.png
```

Le code Go se simplifera à l'extrême :

```go
func main() {
    http.ListenAndServe(
        ":8080",
        http.FileServer(
            http.Dir("."),
        ),
    )
}
```

**ATTENTION** : l'accès à "http:/localhost:8080/rsc/" est possible ; autrement dit, l'accès aux données est complet.

Et si nous souhaitons ajouter du code CSS : on crée un fichier CSS dans notre répteroire "rsc".

```css
body {
    background-color: rgb(218, 209, 209);
}

h1{
    font-size: 2.00em;
    text-align: center;
    color: white;
}

img {
    width: 25%;
}
```

Il ne faudra pas oublier de vider le cache pour voir les résultats, si besoin est.

Code [Go](code8/main.go) / [HTML](code8/index.html) / [CSS](code8/rsc/style.css)

## 6-log.Fatal() et http.Error()

### log.Fatal()

Il est courant de voir un code suivant :

```go
func main() {
    log.Fatal(http.ListenAndServe(":8080",nil)
}
```

Ce qui est parfaitement valide : si ```ListenAndServe``` échoue, le programme s'arrête et retourne le message d'erreur avec le code de sortie à 1.

C'est la façon courante de gérer les erreurs avec les fonctions ne retournant qu'une erreur.

### http.Error()

Lorsque l'on traite de fichiers comme par exemple avec ```os.Open()```, il est bon de retourner un message d'erreur au client (navigateur), pour cela nous disposons de ```http.Error()``` qui prend 3 arguments :

- 1 : le type réponse ```http.ResponseWriter```
- 2 : un message (chaîne de caractères)
- 3 : le code HTTP (un entier : 404) ou  un code ```http.StatusNotFound``` (qui est une constante fournie par le package ```http```)

Exemple : reprise du [code numéro 2](code2/main.go)

```go
func img(rep http.ResponseWriter, req *http.Request) {
    fichier, err := os.Open("../images/img.png")
    if err != nil {
        http.Error(rep, "Erreur sur le fichier", 404)
        log.Fatalf("Fichier inconnu : %v", err)
    }

    defer fichier.Close()

    fs, err := fichier.Stat()
    if err != nil {
        http.Error(rep, "Erreur sur la récupération des infos du fichier", 404)
        log.Fatalf("Erreur sur la récup d'info du fichier %v", err)
    }

    ...
}
```

## 7-http.NotFoundHandler()

Cette méthode attchée au type ```Handler``` retourne une simple requête ```Handler``` qui renvoit les messages d'erreur "404 page not found" au client. 

Pour notre exemple, les tests seront plus efficaces avec Chrome : nous essayons d'accéder à une ressource via une URL : ".../ressources/hello" et là on a un message de retour. En revanche si l'URL se compose seulement de ".../ressources" alors on aura une erreur 404.

Code :

```go
func hello(rep http.ResponseWriter, req *http.Request) {
    fmt.Fprintln(rep, "Hello gophers !")
}

func main() {
    fmt.Println("Lancement serveur 10")
    http.Handle("/ressources", http.NotFoundHandler())
    http.HandleFunc("/ressources/hello/", hello)
    log.Fatal(http.ListenAndServe(":8080", nil))
}
```

Par défaut si l'on essaie d'accéder à une ressource inexistante avec un serveur par défaut : ```http.ListenAndServe(..., nil)```, on aura toujours le message "404 page not found". Le code du package est particulièrement clair sur son utilisation : il crée tout le code nécessaire pour les headers et le message d'erreur.

D'ailleurs, on pourra tester des URLs inexistantes, le message "404 page not found" sera toujours affiché.

Code [Go](code9/main.go)
