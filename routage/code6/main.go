package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
)

func user(rep http.ResponseWriter, req *http.Request, params httprouter.Params) {
	prenom := params.ByName("prenom")
	nom := params.ByName("nom")
	fmt.Fprintf(rep, "Hello, %s %s", prenom, nom)
}

func main() {
	fmt.Println("Serveur httprouter v2")

	mux := httprouter.New()
	mux.GET("/:prenom/:nom", user)

	log.Fatalln(http.ListenAndServe(":8080", mux))
}
