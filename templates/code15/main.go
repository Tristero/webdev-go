package main

import (
	"html/template"
	"log"
	"os"
)

func main() {
	tpl, err := template.ParseGlob("*.txt")
	if err != nil {
		log.Fatalf("Erreur sur le parsing global : %v", err)
	}

	err = tpl.ExecuteTemplate(os.Stdout, "fichier.txt", 42)
	if err != nil {
		log.Fatalf("Erreur sur le traitement des templates : %v", err)
	}
}
