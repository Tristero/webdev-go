package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
)

var ctx context.Context

func main() {
	fmt.Println("Activation du serveur 2")

	http.HandleFunc("/", root)
	http.HandleFunc("/suite", suite)

	http.Handle("favicon.ico", http.NotFoundHandler())
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func root(rep http.ResponseWriter, req *http.Request) {
	ctx = req.Context()
	ctx = context.WithValue(ctx, "UserID", 1)
	ctx = context.WithValue(ctx, "nom", "John Doe")

	fmt.Fprintf(rep, "<p>Bonjour %v, <a href='/suite'>La suite</a></p>", ctx.Value("nom"))

	log.Printf("Données enregistrées :\nUserID= %v\nNom=%v\n",
		ctx.Value("UserID"),
		ctx.Value("nom"))
}

func suite(rep http.ResponseWriter, req *http.Request) {
	id := ctx.Value("UserID").(int)
	nom := ctx.Value("nom").(string)

	fmt.Fprintf(rep, "Valeurs retournées sont : %v (UserID) et %v (nom)", id, nom)

	log.Printf("Contexte = %v\nUserID = %v\nNom = %v", ctx, id, nom)
}
