package main

import (
	"log"
	"os"
	"text/template"
)

func main() {
	tpl, err := template.ParseFiles("fichier.txt")
	if err != nil {
		log.Fatalf("Erreur sur le fichier : %v", err)
	}

	if tpl.Execute(os.Stdout, "world") != nil {
		log.Fatal("Erreur sur l'exécution")
	}
}

/* Alternative
var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseFiles("fichier.txt"))
}

func main() {
	if tpl.ExecuteTemplate(os.Stdout, "fichier.txt", "world") != nil {
		log.Fatalln("Erreur sur le passage de données")
	}
}
*/
