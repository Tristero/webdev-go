package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Livre struct {
	Titre   string
	Auteur  Auteur
	Editeur string
	Pages   int
}

type Auteur struct {
	Nom    string
	Prenom string
}

func main() {

	// Création du client
	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://localhost:27017"))
	if err != nil {
		log.Fatalf("Erreur à la création du client : %v", err)
	}
	log.Println("Création du client réussie -> Connexion ?")

	// Connexion
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	if err := client.Connect(ctx); err != nil {
		log.Fatalf("Erreur à la connexion : %v", err)
	}
	log.Println("Connexion réussie -> Ping ?")

	if err := client.Ping(ctx, nil); err != nil {
		log.Fatalf("Erreur ping : %v", err)
	}
	log.Println("Ping réussi.")

	// Requête : insérer plusieurs documents
	// Pour plus de clarté nous avons extrait le code dans une fonction helper

	requete(ctx, client.Database("biblio").Collection("livres"))

	// déconnexion
	if err := client.Disconnect(ctx); err != nil {
		log.Fatalf("Erreur à la déconnexion : %v", err)
	}

	fmt.Println("Déconnexion")
}

func requete(ctx context.Context, coll *mongo.Collection) {

	fmt.Println("Filtrer avec bson.D{}")
	var livre Livre

	err := coll.FindOne(ctx, bson.D{
		primitive.E{
			Key:   "auteur.nom",
			Value: "Ackroyd"},
	},
	).Decode(&livre)

	if err != nil {
		log.Printf("Erreur sur la récupération du document : %v", err)
		return
	}
	fmt.Printf("Résultat du filtrage: %v\n", livre)

}
