package main

import (
	"context"
	"fmt"
	"time"
)

func main() {
	ctx, cancel := context.WithCancel(context.Background())
	defer cancel()

	for i := range gen(ctx) {
		fmt.Println(i)
		if i == 5 {
			cancel()
			break
		}
	}

	time.Sleep(3 * time.Second)

}

func gen(ctx context.Context) <-chan int {
	ch := make(chan int)

	go func() {
		var nb int
		for {
			select {
			case <-ctx.Done():
				fmt.Println("Arrêt de la boucle infinie")
				return
			case ch <- nb:
				nb++
			}
		}
	}()

	return ch
}
