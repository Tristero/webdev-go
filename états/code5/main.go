package main

import (
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"path/filepath"
)

func main() {
	fmt.Println("Lancement serveur 6")
	http.HandleFunc("/", racine)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func racine(rep http.ResponseWriter, req *http.Request) {

	var rslt string

	// Traitement du fichier téléchargé
	if req.Method == http.MethodPost {
		// ouvrir le fichier reçu
		f, h, err := req.FormFile("fichier")
		if err != nil {
			e := "Erreur sur le fichier téléversé"
			http.Error(rep, e, 500)
			log.Fatalln(e, " : ", err)
		}
		defer f.Close()

		slice, err := ioutil.ReadAll(f)
		if err != nil {
			e := "Erreur sur la lecture du contenu du fichier"
			http.Error(rep, e, 500)
			log.Fatalln(e, " : ", err)
		}

		rslt = string(slice)
		// Affichage du nom du fichier récupéré
		fmt.Printf("Nom du fichier récupéré : %v\n", h.Filename)

		// Ecriture du contenu du fichier en local dans le dossier ./user

		dst, err := os.Create(filepath.Join("./user/", h.Filename))
		if err != nil {
			e := "Erreur à la création du pointeur de fichier"
			http.Error(rep, e, 500)
			log.Fatalln(e, " : ", err)
		}
		defer dst.Close()

		// écriture
		_, err = dst.Write(slice)
		if err != nil {
			e := "Erreur à l'écriture"
			http.Error(rep, e, 500)
			log.Fatalln(e, " : ", err)
		}

		// Vérification
		if nom, err := os.Open(filepath.Join("./user/", h.Filename)); err != nil {
			fmt.Printf("%s n'existe pas !", nom)
		} else {
			fmt.Printf("Le fichier %s a été bien enregistré.\n", nom.Name())
			defer nom.Close()
		}
	}

	// traitement du template
	tpl, err := template.ParseFiles("template.html")
	if err != nil {
		e := "Erreur sur l'ouverture du template"
		http.Error(rep, e, 500)
		log.Fatalln(e, " : ", err)
	}

	if tpl.ExecuteTemplate(rep, "template.html", rslt) != nil {
		e := "Erreur sur l'exécution du template : %v"
		http.Error(rep, e, 500)
		log.Fatalln(e, " : ", err)
	}
}
