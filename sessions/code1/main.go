package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/google/uuid"
)

func main() {
	fmt.Println("Lancement serveur 1")

	http.HandleFunc("/", racine)

	log.Fatal(http.ListenAndServe(":8080", nil))
}

func racine(rep http.ResponseWriter, req *http.Request) {
	c, err := req.Cookie("ID")
	if err != nil {
		// Création du cookie
		v := creationCookie(rep)
		// On affiche le contenu
		fmt.Fprintf(rep, "<p>Un nouveau cookie a été créé avec comme valeur : %s</p>", v)
		return
	}
	// En revanche si le cookie existe :
	fmt.Fprintf(rep, `<p>Le cookie enregistré avec pour valeur valeur = %s</p>`, c.Value)
	fmt.Printf("Cookie existant : %s\n", c.Value)
}

func creationCookie(rep http.ResponseWriter) string {
	rslt := uuid.New()
	fmt.Printf("ID : %v\n", rslt.String())

	c := http.Cookie{
		Name:     "ID",
		Value:    rslt.String(),
		HttpOnly: true,
	}
	http.SetCookie(rep, &c)
	return rslt.String()
}
