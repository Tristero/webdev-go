package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func main() {
	opt := options.Client().ApplyURI("mongodb://localhost:27017")
	client, err := mongo.NewClient(opt)
	if err != nil {
		log.Fatalf("Erreur à la création du client : %v", err)
	}
	log.Println("Création du client réussie -> Connexion ?")

	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	if err := client.Connect(ctx); err != nil {
		log.Fatalf("Erreur à la connexion : %v", err)
	}
	log.Println("Connexion réussie -> Ping ?")

	if err := client.Ping(ctx, nil); err != nil {
		log.Fatalf("Erreur ping : %v", err)
	}
	log.Println("Ping réussi.")

	// Requête simple : combien y a-t-il de documents dans la collection ?
	//collection := client.Database("biblio").Collection("livres")
	dbs, err := client.ListDatabaseNames(ctx, bson.D{})
	if err != nil {
		log.Printf("Erreur sur la requête de la collection : %v", err)
	}

	for _, coll := range dbs {
		fmt.Printf("\t%s\n", coll)
	}

	// déconnexion
	if err := client.Disconnect(ctx); err != nil {
		log.Fatalf("Erreur à la déconnexion : %v", err)
	}

	fmt.Println("Déconnexion")
}
