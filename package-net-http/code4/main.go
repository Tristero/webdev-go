package main

import (
	"fmt"
	"log"
	"net/http"
)

type server struct{}

func (s server) ServeHTTP(rep http.ResponseWriter, req *http.Request) {
	rep.Header().Set("Message", "Ceci est une création inutile")
	rep.Header().Set("Content-Type", "text/html; charset=utf-8")
	fmt.Fprintln(rep, "<h1>Code HTML</h1>")
}

func main() {
	var s server
	fmt.Println("Serveur Go 4")
	if err := http.ListenAndServe(":8080", s); err != nil {
		log.Fatalf("Erreur ListenAndServe : %v", err)
	}

}
