package main

import (
	"encoding/base64"
	"fmt"
	"log"
)

func main() {
	msg := "Ceci est une chaîne avec des caractères non pris en charge par les cookies."
	encode := base64.StdEncoding.EncodeToString([]byte(msg))
	fmt.Println(encode)
	decode, err := base64.StdEncoding.DecodeString(encode)
	if err != nil {
		log.Fatalf("Erreur dans le décodage : %v", err)
	}
	fmt.Println(string(decode))
}
