package main

import (
	"fmt"
	"time"
)

func main() {

	for v := range gen() {
		fmt.Println(v)
		// on casse la boucle
		if v == 5 {
			break
		}
	}

	// Pendant ce temps, la goroutine fonctionne
	time.Sleep(10 * time.Second)
}

func gen() <-chan int {
	ch := make(chan int)

	// On crée une boucle infinie : elle envoit un entier incrémenté
	go func() {
		var n int
		for {
			ch <- n
			n++
		}
		// ce message ne sera jamais affiché
		fmt.Println("Arrêt de la boucle infine ? ...")
	}()

	return ch
}
