package main

/*
on changera l'import text/template vs html/template
*/

import (
	"log"
	"os"
	"text/template"
)

func main() {
	code := `<script>alert("code malicieux")</script>`

	tpl, err := template.ParseFiles("fichier.html")
	if err != nil {
		log.Fatalf("Erreur sur le parsing : %v", err)
	}

	if err = tpl.Execute(os.Stdout, code); err != nil {
		log.Fatalf("Erreur sur le traitement : %v", err)
	}

}
