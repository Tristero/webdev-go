# Pré-requis pour la base de données

Après l'installation locale de MySQL (ou MariaDB : dans les deux cas, les commandes sont **exactement** identiques), on va se connecter en mode root :

```sh
sudo mysql
```

Puis nous allons créer une base et utilisateur (avec son mot de passe) pour cette base.

```sql
/*on crée une base gobase*/
CREATE DATABASE gobase;
SHOW DATABASE;

/*on va créer un utilisateur*/
USE mysql;
CREATE USER 'gopher'@'localhost' IDENTIFIED BY 'gopherpswd';
/* on peut changer localhost par une adresse IP ou par % pour un accès depuis n'importe où.
Puis on alloue des privilèges à cet utilisateur uniquement sur notre base
*/
GRANT ALL PRIVILEGES ON gobase.* TO 'gopher'@'localhost';
FLUSH PRIVILEGES;
/* pour vérifier */
SHOW GRANTS FOR 'gopher'@'localhost';
```

Après l'exercice on pourra :

- supprimer la base : ```DROP DATABASE gobase;```
- révoquer les privilèges de notre utilisateur : ```REVOKE ALL PRIVILEGES FROM 'gopher';```
- puis supprimer l'utilisateur : ```DROP USER 'gopher'@'localhost'```

[Retour](cours.md)