package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
)

func main() {
	fmt.Println("Lancement du serveur 8")

	http.HandleFunc("/", setCookie)
	http.HandleFunc("/lire", readCookie)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func setCookie(rep http.ResponseWriter, req *http.Request) {
	// Création d'un type Cookie
	c := http.Cookie{
		Name:  "ID",
		Value: "1234567890AB",
	}

	// Création du cookie
	http.SetCookie(rep, &c)

	//templating
	tpl, err := template.ParseFiles("template.html")
	if err != nil {
		http.Error(rep, err.Error(), http.StatusInternalServerError)
		log.Fatalf("Erreur sur la récupération du template : %v", err)
	}

	if err = tpl.ExecuteTemplate(rep, "template.html", c); err != nil {
		http.Error(rep, err.Error(), http.StatusInternalServerError)
		log.Fatalf("Erreur sur l'exécution du templating : %v", err)
	}
}

func readCookie(rep http.ResponseWriter, req *http.Request) {
	// Récupération du cookie
	c, err := req.Cookie("ID")
	if err != nil {
		http.Error(rep, "Aucun cookie trouvé : "+err.Error(), http.StatusNoContent)
		log.Fatalf("Erreur sur la récupération du cookie : %v", err)
	}

	fmt.Fprintf(rep, "Contenu du cookie : %v", c)
}
