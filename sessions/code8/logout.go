package main

import (
	"fmt"
	"net/http"
)

func logout(rep http.ResponseWriter, req *http.Request) {

	if !dejaConnecte(req) {
		http.Redirect(rep, req, "/", http.StatusSeeOther)
		return
	}

	// Pas besoin de vérifier l'erreur puisqu'elle a été gérée par la fonction dejaConnecte
	c, _ := req.Cookie("sid")

	fmt.Printf("Avant suppression, la base Id contient :%v\n", len(dbIDs))
	// suppression du username dans dbIDs
	delete(dbIDs, c.Value)
	fmt.Printf("Après suppression, la base Id contient :%v\n", len(dbIDs))

	// destruction du cookie et de sa valeur
	c.MaxAge = -1
	c.Value = ""
	http.SetCookie(rep, c)

	// redirection
	http.Redirect(rep, req, "/", http.StatusSeeOther)

}
