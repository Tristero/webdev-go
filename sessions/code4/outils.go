package main

import "net/http"

// Outils

func recupUtilisateur(req *http.Request) utilisateur {
	c, err := req.Cookie("sid")
	if err != nil {
		return utilisateur{}
	}
	uid := dbIDs[c.Value]
	return dbUsers[uid]
}

func dejaConnecte(req *http.Request) bool {
	c, err := req.Cookie("sid")
	if err != nil {
		return false
	}
	// on va vérifier aussi que le sid est relié à un uuid !
	_, ok := dbIDs[c.Value]
	return ok
}
