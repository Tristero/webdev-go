package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
)

type utilisateur struct {
	Username   string
	MotDePasse []byte
	Nom        string
	Prenom     string
	ID         string
	New        bool
}

// bases de données live : maps
var dbUsers = map[string]utilisateur{} // ID utilisateur -> utilisateur
var dbIDs = map[string]string{}        // ID session -> ID utilisateur

func main() {
	fmt.Println("Lancement du serveur 5")

	http.HandleFunc("/", racine)
	http.HandleFunc("/inscription", inscription)
	http.HandleFunc("/compte", compte)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func racine(rep http.ResponseWriter, req *http.Request) {

	tpl, err := template.ParseFiles("racine.html")
	if err != nil {
		http.Error(rep, err.Error(), 500)
		log.Fatalf("Erreur sur la récupération du template : %v", err)
	}
	if tpl.Execute(rep, nil) != nil {
		http.Error(rep, err.Error(), 500)
		log.Fatalf("Erreur sur le traitement du template : %v", err)
	}
}
