package main

import "net/http"

// Outils

func recupUtilisateur(req *http.Request) utilisateur {
	c, err := req.Cookie("sid")
	if err != nil {
		return utilisateur{}
	}
	username := dbIDs[c.Value]
	return dbUsers[username]
}

func dejaConnecte(req *http.Request) bool {
	c, err := req.Cookie("sid")
	if err != nil {
		return false
	}
	// on va vérifier aussi que le sid est relié à un uuid !
	_, ok := dbIDs[c.Value]
	return ok
}

func redirigeLogin(rep http.ResponseWriter, req *http.Request) {
	bErr = true
	http.Redirect(rep, req, "/login", 302)
	// Attention ici pas de return : il ne doit s'appliquer que sur le bloc d'instruction concerné 
}
