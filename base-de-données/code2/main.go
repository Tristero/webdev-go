package main

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
)

var db *sql.DB

func main() {
	// Gestion de la base
	var err error
	db, err = sql.Open("mysql", "gopher:gopherpswd@/gobase")
	check(err, "")
	defer db.Close()

	// Vérification de la connexion
	err = db.Ping()
	check(err, "à la tentative de connexion")

	// Gestion des routes
	http.HandleFunc("/", racine)
	http.HandleFunc("/create", create)
	http.HandleFunc("/insert", insert)
	http.HandleFunc("/read", read)
	http.HandleFunc("/update", update)
	http.HandleFunc("/delete", delete)
	http.HandleFunc("/drop", drop)
	// Gestion du serveur
	fmt.Println("Lancement du serveur 1")
	http.Handle("/favicon.ico", http.NotFoundHandler())
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func racine(rep http.ResponseWriter, req *http.Request) {
	fmt.Fprint(rep, "Racine du site")
}

func check(e error, msg string) {
	if e != nil {
		log.Printf("Erreur %s : %v", msg, e)
	}
}
