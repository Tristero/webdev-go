package main

import (
	"fmt"
	"net/http"
)

func main() {

	fmt.Println("Serveur de fichiers statiques v7")

	http.Handle(
		"/images/",
		http.StripPrefix(
			"/images/",
			http.FileServer(http.Dir("../images/")),
		),
	)

	http.Handle("/", http.FileServer(http.Dir(".")))

	http.ListenAndServe(
		":8080",
		nil,
	)
}
