package main

import (
	"log"
	"math"
	"os"
	"text/template"
)

var fns = template.FuncMap{
	"carre": carre,
	"rc":    rc,
	"db":    db,
}

func carre(i int) int {
	return i * i
}

func rc(i int) float64 {
	return math.Sqrt(float64(i))
}

func db(i int) int {
	return i * 2
}

func main() {
	tpl := template.New("").Funcs(fns)

	tpl, err := tpl.ParseFiles("fichier.txt")

	if err != nil {
		log.Fatalf("Erreur sur le parsing : %v", err)
	}

	tpl.ExecuteTemplate(os.Stdout, "fichier.txt", []int{42, 3, 6, 9})
}
