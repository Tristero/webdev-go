package main

import (
	"fmt"
	"net/http"
)

func delete(rep http.ResponseWriter, req *http.Request) {
	r, err := db.Exec("DELETE FROM clients WHERE nom=?", "Doe")
	check(err, "sur la requête de suppression")

	nb, err := r.RowsAffected()
	check(err, "sur la récupération du nombre de suppression")

	fmt.Fprintf(rep, "Nombre de suppression : %d", nb)
}
