package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"strings"
)

func main() {
	fmt.Println("Server Go v0.5")
	lis, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatalf("Erreur listener :%v", err)
	}

	for {
		cnx, err := lis.Accept()
		if err != nil {
			log.Fatalf("Erreur connexion : %v", err)
		}

		go gereConn(cnx)
	}
}

func gereConn(cnx net.Conn) {
	defer cnx.Close()

	// lis le contenu envoyé par le client
	requete(cnx)

	// retourne le résultat
	reponse(cnx)
}

// fonction de gestion de la requête
func requete(cnx net.Conn) {
	i := 0
	scn := bufio.NewScanner(cnx)
	for scn.Scan() {
		ln := scn.Text()
		fmt.Println(ln)
		// Extraction du nom de la méthode requise par le client
		if i == 0 {
			fields := strings.Fields(ln)
			fmt.Printf("\t**METHODE : %v**\n", fields[0])
			fmt.Printf("URL demandée : %v\n", fields[1])
		}
		// si ligne vide : on casse la boucle
		if ln == "" {
			break
		}
		// incrémente le compteur
		i++
	}
}

func reponse(cnx net.Conn) {
	// Contenu HTML
	body := `<!DOCTYPE html>
<html lang="fr">
<head>
	<meta charset="UTF-8">
	<title>Document</title>
</head>
<body>
	<p>Hello, world</p>
</body>
</html>`

	// Entêtes HTTP
	fmt.Fprintf(cnx, "HTTP/1.1 200 OK\r\n")
	fmt.Fprintf(cnx, "Content-Length: %d\r\n", len(body))
	fmt.Fprintf(cnx, "Content-Type: text/html\r\n")
	fmt.Fprintf(cnx, "\r\n")
	fmt.Fprintf(cnx, body)
}
