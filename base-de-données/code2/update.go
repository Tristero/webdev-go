package main

import (
	"fmt"
	"net/http"
)

func update(rep http.ResponseWriter, req *http.Request) {
	r, err := db.Exec("UPDATE clients SET prenom=? WHERE nom=?", "Jane", "Doe")
	check(err, "sur la requête update")

	nb, err := r.RowsAffected()
	check(err, "sur la récupération de données affectées")

	fmt.Fprintf(rep, "Nombre de valeurs mises à jour : %v", nb)
}
