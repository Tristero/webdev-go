package main

import (
	"log"
	"os"
	"text/template"
)

func main() {

	str := []string{"un", "deux", "trois", "quatre", "cinq"}

	tpl, err := template.ParseFiles("fichier.txt")
	if err != nil {
		log.Fatalf("Erreur dans le parsing :%v", err)
	}

	err = tpl.Execute(os.Stdout, str)
	if err != nil {
		log.Fatalf("Erreur sur le traitement du fichier : %v", err)
	}
}
