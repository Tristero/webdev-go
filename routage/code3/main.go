package main

import (
	"fmt"
	"log"
	"net/http"
)

type Chien struct{}
type Chat struct{}

func (c Chien) ServeHTTP(rep http.ResponseWriter, req *http.Request) {
	fmt.Fprintln(rep, "Page spéciale pour les toutous")
}

func (c Chat) ServeHTTP(rep http.ResponseWriter, req *http.Request) {
	fmt.Fprintln(rep, "Page spéciale pour les matous")
}

func main() {

	var chien Chien
	var chat Chat
	fmt.Println("Lancement serveur multiplex v3")

	http.Handle("/chien", chien)
	http.Handle("/chat", chat)

	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatalf("Erreur ListenAndServe : %v", err)
	}
}
