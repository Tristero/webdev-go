package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	fmt.Println("Lancement du serveur 9")

	http.HandleFunc("/", racine)
	http.HandleFunc("/creer", creation)
	http.HandleFunc("/lire", lire)
	http.HandleFunc("/supprime", supprime)

	log.Fatal(http.ListenAndServe(":8080", nil))
}

func racine(rep http.ResponseWriter, req *http.Request) {
	fmt.Fprintf(rep, `<h1>Racine du site</h1><br/><a href="./creer">Création du cookie</a>`)
}

func creation(rep http.ResponseWriter, req *http.Request) {

	// Création du cookie
	c := http.Cookie{
		Name:  "ID",
		Value: "AZ1234",
	}
	http.SetCookie(rep, &c)

	fmt.Fprintln(rep, `<a href="./lire">Lire le cookie</a>`)
	fmt.Printf("Création d'un cookie chez le client %v\n", c)
}

func lire(rep http.ResponseWriter, req *http.Request) {

	c, err := req.Cookie("ID")
	if err != nil {
		http.Error(rep, err.Error(), 500)
		log.Fatalf("Erreur sur la récupération du cookie : %v", err)
	}
	fmt.Fprintf(rep, `<a href="./supprime">Suppression du cookie</a><br><p>%v<p>`, c)
}

func supprime(rep http.ResponseWriter, req *http.Request) {
	c, err := req.Cookie("ID")
	if err != nil {
		http.Redirect(rep, req, "./", http.StatusSeeOther)
		log.Println("Erreur impossible de récupérer le cookie : ", err)
		return
	}

	c.MaxAge = -1
	http.SetCookie(rep, c)
	fmt.Printf("Après MaxAge=-1 : %v\n", c)
	http.Redirect(rep, req, "/", http.StatusSeeOther)
}
