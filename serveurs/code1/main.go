package main

import (
	"fmt"
	"io"
	"log"
	"net"
	"time"
)

func main() {
	li, err := net.Listen("tcp", ":9090")
	if err != nil {
		log.Fatalf("Erreur sur la création du listener : %v", err)
	}
	fmt.Println("Lancement du serveur")
	defer li.Close()

	for {
		cnx, err := li.Accept()
		if err != nil {
			log.Fatalf("Erreur sur la connexion : %v", err)
		}

		// Traitement des requêtes et des réponses
		io.WriteString(cnx, "Serveur Go 0.1\n")
		fmt.Fprintf(cnx, "lancement le %v\n", time.Now())

		cnx.Close()
	}
}
