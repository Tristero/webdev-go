# MOOC : développement web avec Go

Sommaire

- [1-Templates](templates/cours.md)
- [2-Serveurs Web](serveurs/cours.md)
- [3-package net/http](package-net-http/cours.md)
- [4-Routage](routage/cours.md)
- [5-Servir des fichiers](fichiers/cours.md)
- [6-Etats](états/cours.md) : formulaires, redirection, cookies et sessions
- [7-Sessions](sessions/cours.md)
- [8-Amazon Web Services](aws/cours.md)
- [9-Bases de données](base-de-données/cours.md) : MySQL
- [10-Outils web supplémentaires](outils_web/cours.md) : JSON, AJAX et l'API fetch, HTTPS, HMAC, Context, encodage en base 64, WebStorage
- [11-MongoDB : les bases](mongodb/cours.md)
