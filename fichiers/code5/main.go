package main

import (
	"fmt"
	"net/http"
)

func main() {
	fmt.Println("Lancement serveur de fichiers v5.")
	http.Handle(
		"/ressources",
		http.StripPrefix("/ressources", http.FileServer(http.Dir("../images/"))),
	)

	http.ListenAndServe(":8080", nil)
}
