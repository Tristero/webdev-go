package main

import (
	"log"
	"os"
	"text/template"
)

func main() {
	admins := []struct {
		Nom   string
		MDP   string
		Admin bool
	}{
		{
			Nom:   "John",
			MDP:   "Azerty",
			Admin: false,
		},
		{
			Nom:   "Alice",
			MDP:   "123Poi",
			Admin: true,
		},
		{
			Nom:   "Bob",
			MDP:   "VF53jj7mP",
			Admin: true,
		},
		{
			Nom:   "",
			MDP:   "AbCd",
			Admin: true,
		},
	}

	tpl, err := template.ParseFiles("fichier.txt")
	if err != nil {
		log.Fatalf("Erreur sur le parsing : %v", err)
	}

	err = tpl.Execute(os.Stdout, admins)
	if err != nil {
		log.Fatalf("Erreur sur le traitement du template : %v", err)
	}
}
