package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
)

func inscription(rep http.ResponseWriter, req *http.Request) {

	// si l'utilisateur est déjà connecté et référencé
	if dejaConnecte(req) {
		fmt.Println("Utilisateur déjà enregistré : redirection vers le compte")
		http.Redirect(rep, req, "/compte", http.StatusSeeOther)
		return
	}

	// traitement du formulaire
	if req.Method == http.MethodPost {

		u := utilisateur{}

		u.ID = uuid.New().String()
		u.New = true
		u.Nom = req.FormValue("nom")
		u.Prenom = req.FormValue("prenom")
		u.Username = req.FormValue("username")

		// génération du mot de passe
		b, err := bcrypt.GenerateFromPassword([]byte(req.FormValue("motdepasse")), bcrypt.DefaultCost)
		if err != nil {
			http.Error(rep, "Erreur sur le mot de passe", http.StatusInternalServerError)
			log.Printf("Erreur sur la génération du hash : %v\n", err)
			return
		}
		u.MotDePasse = b

		// création du cookie de session
		c := &http.Cookie{
			Name:  "sid",
			Value: u.Username,
		}

		// enregistrement des données
		dbIDs[c.Value] = u.Username
		fmt.Printf("Données IDs : %v\n", dbIDs)
		dbUsers[u.Username] = u
		fmt.Printf("Données users : %v\n", dbUsers)

		http.SetCookie(rep, c)

		// on redirige :
		http.Redirect(rep, req, "/compte", http.StatusSeeOther)
		return
	}

	if err := tpl.ExecuteTemplate(rep, "inscription.html", nil); err != nil {
		http.Error(rep, "Erreur traitement du template", http.StatusInternalServerError)
		log.Printf("Erreur sur le traitement du template %v\n", err)
	}
}
