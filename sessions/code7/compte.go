package main

import (
	"log"
	"net/http"
)

func compte(rep http.ResponseWriter, req *http.Request) {

	// si utilisateur essie de se connecter sans en avoir le droit
	if !dejaConnecte(req) {
		http.Error(rep, "Accès interdit !", http.StatusForbidden)
		log.Println("Accès au compte interdit")
		return
	}

	// récupération des données
	u := recupUtilisateur(req)

	// on va modifier le nouvel utilisateur : dorénavant il ne le sera plus:
	// pour cela on doit faire une copie
	if u.New {
		old := u
		old.New = false
		dbUsers[old.Username] = old
	}

	if err := tpl.ExecuteTemplate(rep, "compte.html", u); err != nil {
		log.Printf("Erreur sur le traitement du template :%v\n", err)
		http.Error(rep, "Erreur sur le template", http.StatusInternalServerError)
	}
}
