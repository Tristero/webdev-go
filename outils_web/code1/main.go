package main

import (
	"crypto/hmac"
	"crypto/sha256"
	"fmt"
	"io"
)

func main() {
	h1 := genCode("exemple@societe.org", "macle")
	h2 := genCode("exempl@societe.org", "macle")
	var msg string
	if hmac.Equal(h1, h2) {
		msg = "Les deux hashes sont équivalents"
	} else {
		msg = "Les deux hashes sont dissemblables"
	}
	fmt.Println(msg)
}

func genCode(donnee string, cle string) []byte {
	// génère un type hash.Hash
	h := hmac.New(sha256.New, []byte(cle))
	fmt.Println(h)
	// génère le code de hashage
	io.WriteString(h, cle)
	// affiche le hash au format hexadécimal
	fmt.Printf("Hash généré : %x\n", h.Sum(nil))
	// retourne un slice
	return h.Sum(nil)
}
