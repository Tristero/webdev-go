package main

import (
	"context"
	"fmt"
	"log"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func main() {
	fmt.Println("Création d'un client avec essai de connexion/déconnexion")
	opts := options.Client().ApplyURI("mongodb://localhost:27017")
	client, err := mongo.Connect(context.TODO(), opts)
	if err != nil {
		log.Fatalf("Erreur à la connexion : %v", err)
	}

	if err = client.Ping(context.TODO(), nil); err != nil {
		log.Fatalf("Erreur au Ping : %v", err)
	}

	fmt.Println("Connexion réussie")

	if err = client.Disconnect(context.TODO()); err != nil {
		log.Fatalf("Erreur à la déconnexion : %v", err)
	}

	fmt.Println("Déconnexion du serveur")
}
