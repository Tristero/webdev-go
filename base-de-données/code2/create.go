package main

import (
	"fmt"
	"net/http"
)

func create(rep http.ResponseWriter, req *http.Request) {
	r, e := db.Exec("CREATE TABLE clients (nom VARCHAR(80), prenom VARCHAR(80));")
	check(e, "à la création de la table")

	i, e := r.RowsAffected()
	check(e, "sur RowsAffected")

	if e == nil {
		fmt.Fprintf(rep, "Nombre de rows : %d", i)
	} else {
		http.Error(rep, e.Error(), http.StatusInternalServerError)
	}
}
