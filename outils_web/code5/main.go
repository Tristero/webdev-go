package main

import (
	"context"
	"fmt"
	"log"
	"net/http"
	"time"
)

func main() {
	fmt.Println("Lancement du serveur 3 : les timeouts")

	http.HandleFunc("/", racine)
	http.Handle("favicon.ico", http.NotFoundHandler())
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func racine(rep http.ResponseWriter, req *http.Request) {
	ctx := req.Context()
	ctx = context.WithValue(ctx, "uid", 42)

	// simulation de l'accès à un serveur distant
	i, err := accesBDD(ctx)
	if err != nil {
		http.Error(rep, err.Error(), 500)
	}
	fmt.Fprintf(rep, "UID = %v", i)

}

func accesBDD(ctx context.Context) (int, error) {
	ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
	defer cancel()

	ch := make(chan int)

	// on va récupérer la valeur de l'uid avec simulation d'une latence réseau
	go func() {
		// Récupère la donnée après un certain temps : timeout !
		uid := ctx.Value("uid").(int)
		time.Sleep(3 * time.Second)

		// Vérification s'il n'y a pas d'erreur
		if ctx.Err() != nil {
			return
		}
		ch <- uid
	}()

	// Que retourner ?
	select {
	case <-ctx.Done(): // si le premier canal à renvoyer une donnée vient du contexte
		return 0, ctx.Err()
	case val := <-ch: // si le premier canal à renvoyer une donnée vient de notre canal
		return val, nil
	}

}
