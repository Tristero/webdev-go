package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
)

type Personne struct {
	Nom    string
	Prenom string
}

func main() {
	fmt.Println("Lancement serveur 3")
	http.HandleFunc("/", racine)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func racine(rep http.ResponseWriter, req *http.Request) {
	p := Personne{}
	p.Prenom = req.FormValue("prenom")
	p.Nom = req.FormValue("nom")

	tpl, err := template.ParseFiles("template.html")
	if err != nil {
		http.Error(rep, "Template : problème", http.StatusNotFound)
		log.Fatal("Erreur sur le template : %v", err)
	}

	err = tpl.ExecuteTemplate(rep, "template.html", p)
	if err != nil {
		http.Error(rep, "Erreur exécution templating", http.StatusNotFound)
		log.Fatal("Erreur exécution du templating : %v", err)
	}
}
