package main

import (
	"log"
	"os"
	"text/template"
)

type pays struct {
	Capitale string
	Pays     string
}

var slice = []pays{
	pays{
		Capitale: "Berlin",
		Pays:     "Allemagne",
	},
	pays{
		Pays:     "France",
		Capitale: "Paris",
	},
	pays{
		Pays:     "Italie",
		Capitale: "Rome",
	},
	pays{
		Pays:     "Espagne",
		Capitale: "Madrid",
	},
}

func main() {

	tpl, err := template.ParseFiles("fichier.txt")
	if err != nil {
		log.Fatalf("Erreur sur le parsing :%v", err)
	}

	if tpl.Execute(os.Stdout, slice) != nil {
		log.Fatalf("Erreur sur le traitement des données")
	}

}
