package main

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

func main() {
	// création d'un pointeur de gestion de base
	db, err := sql.Open("mysql", "gopher:gopherpswd@/gobase")
	check(err, "à la création du pointeur")

	// diffère la fermeture
	defer db.Close()

	// Vérification de la connexion
	err = db.Ping()
	check(err, "au Ping")

	// Si tout est OK
	fmt.Println("Connexion existante !")
}

func check(e error, msg string) {
	if e != nil {
		log.Fatalf("Erreur %s : %v", msg, e)
	}
}
