package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	fmt.Println("Lancement serveur v1")
	fmt.Println("Test de http://localhost:8080/?q=1234567890")
	http.HandleFunc("/", root)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func root(rep http.ResponseWriter, req *http.Request) {
	// Récupération des valeurs de requêtes
	valeur := req.FormValue("q")
	fmt.Fprintf(rep, "Valeur retournée = %v", valeur)
	fmt.Println(valeur)
}
