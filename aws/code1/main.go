package main

import (
	"fmt"
	"net/http"
)

func main() {
	http.HandleFunc("/", racine)
	http.ListenAndServe(":80", nil)
}

func racine(rep http.ResponseWriter, req *http.Request) {
	fmt.Fprint(rep, "<p>Mon premier serveur sur AWS</p>")
}
