# Autres outils pour le développement

Objectif : proposer des outils supplémentaires utiles pour le développement web.

## 1-HMAC

Permet de réaliser un code de hashage d'authentification : Hash Message Authentication Code. Ce système requiert une donnée sur laquelle va s'appliquer la fonction de hashage et un mot de passe (clé privée).

Go dispose dans sa bibliothèque standard du package ```crypto/hmac``` disposant de deux fonctions :

- ```New(f func() hash.Hash, donnee []byte) hash.Hash``` génère une structure de type ```hash.Hash``` à partir des méthodes de chiffrement disponibles dans ```crypto``` comme ```sha512```) et d'une donnée dont on veut obtenir un hash (comme par exemple une adresse mail).
- ```Equal(hash1, hash2 []byte) bool``` permettra de vérifier si deux hashages sont identiques.

Il existe deux ```hash.Hash``` : un type qui est un alias dans ```crypto``` et une interface !

Le type ```hash.Hash``` est un alias vers ```uint``` : c'est la représentation de la fonction de hashage cryptographique employé.

Quant à l'interface éponyme, elle est implémentée par toutes les fonctions de hashages de la bibliothèque standard. Seront implémentées : ```io.Writer```, ```Size()```, ```Sum()```, ...

Pour notre exemple nous utilisons : le [SHA256](https://golang.org/pkg/crypto/sha256/)

Avec ce sous-package : n'est pas disponible ```HashFunc()``` car dépendant de l'implémentation du type ```SignerOpts```.

Nous utiliserons ```Sum()``` qui prend un slice d'octet en argument et retourne un slice d'octets (qui est la concaténation de l'entrée et du hash). Nous passerons ```nil``` car on ne veut que le hash sous forme de slice.

Comme l'interface ```Hash``` demande l'implémentation de ```io.Writer``` : on va appliquer une clé privée qui est une chaîne au type ```Hash``` via ```io.WriteString()```. Cela va permettre de générer notre hash.

C'est avec ```Sum(nil)``` que l'on va pouvoir afficher avec ```fmt``` le contenu du hash !

Code :

```go
// on crée une nouvelle structure qui contiendra notre donnée à hasher
h := hmac.New(sha256.New, []byte("exemple@fsociety.com"))
// génère le code de hashage à l'aide d'une clé privée
io.WriteString(h, []byte("ceci_est_une_cle_privee"))
// affiche le résultat au format hexadécimal
fmt.Printf("Hash = %x\n", h.Sum(nil))
```

Utilité : côté serveur, on ne conserve que le hash en lieu et place de certaines données très sensibles

Code [Go](code1/main.go)

## 2 Encodage Base64

Le problème du cookie c'est qu'un certain nombre de caractères sont acceptés donc on sera vite gêné par cette restriction. L'encodage en base 64 permet de dépasser les limites précédentes :

Voici un exemple d'utilisation très simple issue de la [documentation de encoding/base64](https://golang.org/pkg/encoding/base64/)

La bibliothèque standard fourni des variables pour encoder et décoder très facilement.

### Encodage

On appellera la variable publique ```StdEncoding``` qui est un sucre syntaxique permettant de générer un pointeur ```Encoding``` : ce pointeur va permettre à la fois de coder et de décoder tout message en base 64 (pointeur vers une structure ```Encoding```)

```go
// Sucre
pointeur := base64.StdEncoding
// Revien à écrire
pointeur = base64.NewEncoding("ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/")
```

Pour encoder Go fournit plusieurs méthodes : nous utiliserons la plus simple ```EncodeToString()``` qui prend en argument un slice d'octets et retourne une chaîne encodée en base 64.

```go
msg := "Ceci est une chaîne avec des caractères non pris en charge par les cookies."
encode := base64.StdEncoding.EncodeToString([]byte(msg))
fmt.Println(encode)
```

### Décodage

On réutilisera le sucre syntaxique ```StdEncoding``` pour décoder. Puis nous appellerons la méthode ```DecodeString()``` : prenant une chaîne en paramètre et retournant un slice d'octets avec une erreur.

```go
decode, err := base64.StdEncoding.DecodeString(encode)
if err != nil {
    log.Fatalf("Erreur dans le décodage : %v", err)
}
```

Code [Go](code2/main.go)

## 3-WebStorage

Cette technologie permet un stockage local (*local storage*) ainsi qu'un stockage de session (*session storage*). On sépare le WebStorage apparu avec le HTML 5 et les cookies apparus bien avant. L'accès aux données du Local et Session storage n'est possible qu'avec JS. La différence entre Session et Local, le premier est temporaire (à la fermeture du navigateur, les données sont effacées) et le second mise sur la persistance.

Présentation [MDN](https://developer.mozilla.org/fr/docs/Web/API/Web_Storage_API)

## 4-Context

**Attention** : le niveau, ici, est pour toute personne ayant un niveau intermédiaire en Go.

Ce package de la bibliothèque standard fournit des outils permettant de propager des deadlines, signaux d'annulation, créer des timeouts, ainsi que de transmettre des valeurs à travers des API mais aussi des processus.

Bien comprendre les contextes nécessite la bonne compréhension de *channels* et des *goroutines*.

Le contextes sont taillés pour la programmation web. Ils sont intégrés avec ```Request```.

### 1-Récupérer le contexte d'une requête

Premier code : on va récupérer le contexte du pointeur de requête et l'afficher (à la console et sur le navigateur)

```go
func root(rep http.ResponseWriter, req *http.Request) {
    // Récupération
    ctx := req.Context()
    // Affichage à la console
    log.Println(ctx)
    // Affichage sur le navigateur
    fmt.Fprintln(rep, ctx)
}
```

Et on obtient ce résultat :

```txt
context.Background.WithValue(type *http.contextKey, val <not Stringer>).WithValue(type *http.contextKey, val 127.0.0.1:8080).WithCancel.WithCancel
```

Code [Go](code3/main.go)

### 2-Intégrer des données

On observera que le contexte conserve des données sur la connexion. Nous avons la base. Maintenant cherchons à passer des valeurs entre différentes pages : non plus avec un cookie et un ersatz de base mais seulement avec les contextes.

Pour cela nous disposons de ```WithValue()``` auquel on passera le contexte et les données. Ces données sont des couples de "clé/valeur".

Nous allons créer deux routes :

- pour "/", nous allons passer au contexte deux valeurs un identifiant utilisateur avec une valeur et le nom de l'utilisateur
- pour "/suite", nous afficherons les données stockées dans le contexte de "/".

**NB** : à chaque requête, un nouveau contexte est généré ! Donc pour transmettre des données entre requête, il nous faudra une variable globale.

Pour récupérer les valeurs on utilise la méthode ```Value()``` à laquelle on passe le nom de la clé associée à la valeur à récupérer. **Attention**, il faudra caster le résultat et le retourner au contexte de départ (on "l'écrase" : en fait on enchaine les contextes à partir d'un contexte de base initialisé, ici par défaut, avec ```Background()``` : pour aller plus loin voir la vidéo de F. Campoy ; cf. infra).

```go
monContexte = monContexte.Value("maCle").(TYPE_DE_LA_VALEUR)
```

Exemple pour "/" :

```go
func root(rep http.ResponseWriter, req *http.Request) {
    // Récupération du contexte
    ctx = req.Context()
    // Ajout des données
    ctx = context.WithValue(ctx, "UserID", 1)
    ctx = context.WithValue(ctx, "nom", "John Doe")

    // Affichage
    fmt.Fprintf(rep, "<p>Bonjour %v, <a href='/suite'>La suite</a></p>", ctx.Value("nom"))

    // Affichage au prompt du shell
    log.Printf("Données enregistrées :\nUserID= %v\nNom=%v\n",
        ctx.Value("UserID"),
        ctx.Value("nom"))
}
```

On évitera de passer des données de type basique mais des structures.

### 3-Gérer les timeouts

Objectif : on reprend le stockage d'un uid associé à un entier et on va gérer un timeout (simulant une longue latence d'accès à un serveur de base de données) et observer ce que l'on peut obtenir avec ```WithTemout```.

Pour gérer les timeouts, le package context fourni une fonction ```WithTimeout``` qui prend deux arguments :

- le contexte précédent
- une durée

Si au bout de X secondes, nanosecondes, ou autres, une fonction d'annulation. Cette fonction va permettre d'arrêter les processus en amont (enfants) et supprime les références en aval (parents).

Exemple de code :

```go
ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
defer cancel()
```

Nous allons créer une simple page "racine" dans laquelle on va simuler un appel pour un accès à une base de données. On va gérer un timeout

générer un timeout très simple : le timeout interviendra au bout d'une seconde. Pour pouvoir utiliser un timeout et passer des valeurs nous utiliserons un *channel* (pour envoyer un résultat) et une *goroutine* (qui génèrera notre latence) :

```go
func accesBDD(ctx context.Context) ??????? {
    ctx, cancel := context.WithTimeout(ctx, 1*time.Second)
    defer cancel()

    ch := make(chan int)

    // on va récupérer la valeur de l'uid avec simulation d'une latence réseau
    go func() {
        // Récupère la donnée après un certain temps : timeout !
        uid := ctx.Value("uid").(int)
        time.Sleep(3 * time.Second)

        // Vérification s'il n'y a pas d'erreur
        if ctx.Err() != nil {
            return
        }
        ch <- uid
    }()
}
```

Pour l'instant nous ne retournons rien. Nous disposons d'un canal ```ch``` mais s'il y a timeout (ce qui sera le cas), on risque de faire face à une erreur (panic, deadlock). Donc il serait intéressant de retourner un résultat et une erreur !

Un context utilise les *channels* pour communiquer : ainsi ```context.Done()``` retourne un canal. Donc on a deux possibilités : soit le canal du ```context``` soit notre canal retourne quelque chose. Pour gérer cela nous avons dans la boîte à outil du langage : ```select``` ! Donc on va pouvoir retourner une valeur et une erreur :

```go
select {
case <-ctx.Done():
    return 0, ctx.Err()
case val := <-ch:
    return val, nil
}
```

A partir de ce code on va pouvoir proposer un comportement particulier pour notre page racine :

```go
func racine(rep http.ResponseWriter, req *http.Request) {
    ctx := req.Context()
    ctx = context.WithValue(ctx, "uid", 42)

    // simulation de l'accès à un serveur distant
    i, err := accesBDD(ctx)
    if err != nil {
        http.Error(rep, err.Error(), 500)
    }
    fmt.Fprintf(rep, "UID = %v", i)
}
```

Avec cet exemple, on comprend l'intérêt du contexte avec le timeout.

### 3-Annulation

Il est possible, grâce aux contextes on va pouvoir avertir les autres processus qu'un processus s'est arrêté (par exemple un canal de renvoyant plus de données) via la fonction ```WithCancellation()```.

Exemple de problème : nous allons créer un générateur de nombre de 0 à l'infini. Nous utiliserons une goroutine qui va envoyer dans un canal chaque valeur incrémentée de 1.

```go
func gen() <-chan int {
    ch := make(chan int)

    // On crée une boucle infinie : elle envoit un entier incrémenté
    go func() {
        var n int
        for {
            ch <- n
            n++
        }
        // Ce message ne sera jamais affiché :
        fmt.Println("Fin de la boucle")
    }()

    return ch
}
```

Mais comment arrêter le processus ?

```go
func main() {

    for v := range gen() {
        fmt.Println(v)
        // on casse la boucle
        if v == 5 {
            break
        }
    }
    // Pendant ce temps notre goroutine tourne !
    time.Sleep(10 * time.Second)
}
```

Le contexte permet de bien gérer les problèmes avec les goroutines pouvant *leaker*.

Nous allons pouvoir arrêter la boucle infinie avec ```WithCancellation```. Pour cela nous allons faire en sorte que notre boucle infinie gère soit un ```context.Done()``` soit l'envoi d'un nombre dans le canal. Ceci est possible avec ```select``` : soit un reçoit une demande d'annulation (```ctx.Done```) et on met fin simplement avec ```return``` à la boucle. Sinon on incrémente notre variable entière.

```go
func gen(ctx context.Context) <-chan int {
    ch := make(chan int)

    go func() {
        var nb int
        for {
            select {
            case <-ctx.Done():  // Annulation
                fmt.Println("Fin de la boucle infinie")
                return
            case ch <- nb:      // Envoi d'un entier dans le canal
                nb++
            }
        }
    }()

    return ch
}
```

Comment utiliser cette annulation ?

Dès que l'on reçoit 5 de notre générateur, on appellera la fonction ```cancel()``` puis on cassera la boucle : en arrière-plan, la goroutine sera elle aussi arrêtée.

```go
func main() {
    ctx, cancel := context.WithCancel(context.Background())
    defer cancel()

    for i := range gen(ctx) {
        fmt.Println(i)
        if i == 5 {
            cancel()
            break
        }
    }

    time.Sleep(3 * time.Second)

}
```

*Remarque* : nous avons utilisé ```context.Background()```. Ceci est un contexte vide : c'est avec lui que tout commence. Il est la "racine" dont dépendra tous les autres contextes.

Code [Go](code7/main.go)

Excellente présentation de ```context``` par F. Campoy sur [Youtube](https://www.youtube.com/watch?v=LSzR0VEraWw) (niveau intermédiaire à avancé)

## 5-HTTPS et TLS

```ListenAndServeTLS(adresse, fichierDeCertification, clé, handler)``` permet de créer un serveur web sécurisé. Les paramètres ```fichierDeCertification``` et ```clé``` sont de type chaîne de caractères.

Exemple : ```err := http.ListenAndServeTLS(":8443", "cert.pem", "key.pem", nil)```

On l'utilisera en lieu et en place du traditionnel ```ListenAndServe()```. Attention au port de l'adresse lors du portage du serveur en mode production : on utilisera le port 443.

Le protocole cryptographique SSL est déprécié au profit du TLS (en version 1.3 depuis 2018).

Il est possible de générer un certificat et une clé dans le cadre du développement : Il existe un fichier ```generate_cert.go```. On va pouvoir l'utiliser pour générer un certificat non signé et une clé privée :

```sh
go run $(go env GOROOT)/src/crypto/tls/generate_cert.go --host=localhost
```

Et pour Windows : ```go run %(go env GOROOT)%/src/crypto/tls/generate_cert.go --host=localhost```

On va pouvoir créer une connexion sécurisée avec le code suivant :

```go
func main() {
    fmt.Println("Premier serveur sécurisé")

    http.HandleFunc("/", root)

    http.Handle("favicon.ico", http.NotFoundHandler())
    log.Fatal(http.ListenAndServeTLS(":10443", "cert.pem", "key.pem", nil))
}
```

Pour les tests, il faudra bien passer [https://localhost:10443](https://localhost:10443)le navigateur nous alertera que le certificat n'est pas sécure : on outrepassera l'avertissement puisque nous ne faisons que des tests en mode local.

Code [Go](code8/main.go)

## 6-JSON

Objet : tout développeur Go a fait face au traitement d'un document ou de données représentée en JSON. Ici on va faire un rappel de la sérialisation/désérialisation en Go d'une donnée depuis/vers JSON. Donc bien connaître JSON est un pré-requis.

### Encoder du JSON

La fonction Marshal a pour objectif de "traduire" du JSON en une structure propre à Go. On peut traduire ceci : Marshal **encode** le JSON.

La fonction prend un argument : une interface. Et elle retourne un slice d'octets et une erreur. Pour récupérer les données d'un JSON, nous devons au préalable définir une structure représentant le schéma du JSON à traiter.

Le package ```encoding/json``` fournit aussi ```Decode()``` et ```Encode()``` qui ont une portée plus "générale". L'intérêt c'est que l'on peut utiliser n'importe quel objet implémentant un ```Reader``` et/ou un ```Writer```. Avec ```Decode``` nous n'allons pas récupérer un slice d'octets puisque l'on retournera les données vers un objet de type ```Writer```.

**Attention**, il nous faudra définir un entête spécifique car nous souhaitons expédier au navigateur un type MIME : "application/json".

Pour nos exemples on va créer un type un peu plus complexe :

```go
type Auteur struct {
    Nom    string
    Prenom string
}
type Livre struct {
    Titre   string
    Auteur  Auteur
    ISBN    string
    NbPages int
}
```

#### 1-Marshal

```go
func marshaler(rep http.ResponseWriter, req *http.Request) {
    rep.Header().Set("Content-Type", "application/json")

    slice, err := json.Marshal(&Livre{
        Titre:   "obabakoak",
        NbPages: 409,
        ISBN:    "9782267019155",
        Auteur: Auteur{
            Prenom: "Bernardo",
            Nom:    "Atxaga",
        },
    })

    if err != nil {
        log.Printf("Erreur dans l'encodage: %v\n", err)
        http.Error(rep, "Erreur dans l'encodage", http.StatusInternalServerError)
    }

    fmt.Fprintln(rep, string(slice))
    // alternative encore plus concise
    // rep.Write(slice)
}
```

Rien de plus simple : on transmet ici une structure "anonyme" à ```Marshal``` qui transmettra un slice et une erreur. Nous disposons de plusieurs méthodes pour retourner notre résultat avec le package ```fmt``` ou encore plus simplement en utilisant la méthode ```Write()``` qui prend en argument un slice d'octets !

#### 2-Encode()

Pour utiliser cette méthode, nous devons créer un pointeur avec ```NewEncoder()```. Cette fonction requiert une type implémentant ```io.Writer``` (ce qui est le cas de ```http.ResponseWriter```). Le pointeur va nous créer un "encodeur JSON". Et le traitement du JSON en sera encore plus simple :

```go
func encoder(rep http.ResponseWriter, req *http.Request) {
    rep.Header().Set("Content-Type", "application/json")

    encoder := json.NewEncoder(rep)
    err := encoder.Encode(&Livre{
        Titre:   "obabakoak",
        NbPages: 409,
        ISBN:    "9782267019155",
        Auteur: Auteur{
            Prenom: "Bernardo",
            Nom:    "Atxaga",
        },
    })

    if err != nil {
        log.Println("Erreur à l'encodage avec Encode()")
        http.Error(rep, "Erreur avec Encode()", http.StatusInternalServerError)
    }
}
```

On peut simplifier le code :

```go
if json.NewEncoder(rep).Encode(...) != nil {
    ...
}
```

Code [Go](code9/main.go)

### Décoder du JSON

De la même manière que pour le décodage on dispose de deux manières de réaliser un décodage d'un JSON vers une structure de données Go :

- ```Unmarshal()```
- ```Decode()```

Ici nous utiliserons aussi les **tags** pour rendre la tâche plus complexe et plus proche de ce que l'on risque de trouver en production.

Les tags vont permettre de faire correspondre des noms de champs différents entre le nom du champ dans la structure Go et le nom du champ du JSON. L'exemple ici : on dispose d'un champ Go ```NbPages``` alors que sa version JSON est ```pages```. De même on va pouvoir préciser que certains champs peuvent être omis (si le champ est vide) grâce au tag ```omitempty``` que l'on rajoute au nom du champ

Notre JSON ressemblera à :

```js
{
    "titre": "obabakoak",
    "auteur": {
        "nom":"Atxaga",
        "prenom":"Bernardo",
    },
    "isbn": "9782267019155",
    "pages": 409
}
```

**Remarque** : tous nos champs JSON ont une graphie légèrement différente de nos structures.

```go

```

#### 1-Unmarshal

La fonction ```Unmarshal``` prend deux paramètres :

- données sous forme d'un slice d'octets
- un type de données : une structure, un type, ... nous utiliserons un pointeur de type Livre.

Et retourne une erreur.

```go
func unmarshaler(rep http.ResponseWriter, req *http.Request) {

    lvr := &Livre{}
    err := json.Unmarshal([]byte(donnees), lvr)
    if err != nil {
        log.Printf("Erreur sur le décodage : %v\n", err)
        http.Error(rep, "Erreur de décodage", http.StatusInternalServerError)
        return
    }
    // on affichera le résultat sur le navigateur avec lvr
}
```

#### 2-Decode()

Cette méthode sera intéressante lorsque l'on voudra lire une donnée de tout type implémentant ```io.Reader```. Par exemple : lire un fichier JSON !

D'abord on récupère le fichier, on l'ouvre en lecture seule puis on passe le pointeur de fichier à la fonction ```NewDecoder()```. Cette fonction retourne un pointeur qui va nous permettre de décoder : et l'opération de décodage s'effectuera avec l'appel de la méthode ```Decode()``` qui ne retourne qu'une erreur. On lui passera un pointeur de type Livre

```go
func decoder(rep http.ResponseWriter, req *http.Request) {
    f, err := os.OpenFile("livre.json", os.O_RDONLY, 0666)
    if err != nil {
        log.Printf("Erreur sur la lecture du fichier : %v\n", err)
        http.Error(rep, "Erreur sur la récupération du JSON", http.StatusNotFound)
    }
    // Ne pas oublier de fermer :
    defer f.Close()

    decoder := json.NewDecoder(f)
    lvr := &Livre{}
    if err = decoder.Decode(lvr); err != nil {
        log.Printf("Erreur dans le décodage : %v\n", err)
        http.Error(rep, "Erreur dans le décodage", http.StatusInternalServerError)
        return
    }

    // Affichage de la récupération des données
    ...
}
```

Le code Go propose en supplément un template pour un affichage plus "élégant" du JSON.

Code [Go](code10/main.go)

Pour une présentation complète de Go avec JSON : direction le [blog officiel](https://blog.golang.org/json). Autre ressource complète : [eager.io](https://eager.io/blog/go-and-json/). Pour extraire un type depuis un JSON : [JOSN-to-Go](https://mholt.github.io/json-to-go/)

## 7-AJAX

Objet : faire une requête AJAX simple mais la réponse sera fournie par le serveur Go. Cette section peut paraître obsolète : il existe beaucoup de bibliothèques et d'API alternatives à AJAX telles queles WebSockets ou l'API fetch !

Pré-requis : connaître JS et savoir faire une requête AJAX.

### 1-Code AJAX

nous créons une simple page HTML "index.html" qui va faire une requête ```GET``` vers le serveur qui retournera un simple texte. Et ce texte sera affiché dans la page statique HTML en retour.

```html
<body>
    <h2>Gestion AJAX et Go</h2>
    <a href="#" onclick="req()">Lancement de l'appel Ajax</a>
    <div id="texte"></div>


    <script>
        let req = () => {
            let xhr = new XMLHttpRequest();
            xhr.open('GET', "/ajax");
            xhr.onreadystatechange = () => {
                if (xhr.readyState === XMLHttpRequest.DONE && xhr.status === 200) {
                    document.querySelector("div#texte").textContent=xhr.responseText;
                }
            };
            xhr.send();
        };
    </script>

</body>
```

### 2-Code Go

Côté serveur : rien de complexe. Pour envoyer "index.html", on va simplement lire le fichier et retourner le résultat (pas besoin de ```html/template```) :

```go
func racine(rep http.ResponseWriter, req *http.Request) {

    f, err := ioutil.ReadFile("index.html")
    if err != nil {
        log.Printf("Erreur dans l'ouverture du fichier : %v", err)
    }

    rep.Header().Set("Content-Type", "text/html; charset=utf-8")
    rep.Write(f)
}
```

Pour traiter la requête AJAX, nous ferons encore plus simple : nous retournerons un simple texte :

```go
func ajax(rep http.ResponseWriter, req *http.Request) {
    log.Println("Appel de la requête /ajax")
    fmt.Fprint(rep, "Hello from Ajax !")
}
```

Documentation AJAX : [MDN](https://developer.mozilla.org/fr/docs/Web/Guide/AJAX)

Code [Go](code11/main.go) / [HTML](code11/index.html)

### 3-Alternative avec fetch

Principe est le même mais beaucoup plus simple ! Le code JS est beaucoup plus simple et lisible :

```js
async function req() {
    let rep = await fetch("/fetch") ;
    let texte = await rep.text();
    document.querySelector("div#texte").textContent = texte;
}
```

(Nous proposons dans le code source deux versions pour traiter le texte envoyé par le serveur : on n'utilise QUE ```fetch``` et ```then```)

Côté Go : aucun changement !

Code [Go](code12/main.go) / [HTML](code12/index.html) (avec les deux codes JS)
