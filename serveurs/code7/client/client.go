package main

import (
	"fmt"
	"log"
	"net"
)

func main() {
	fmt.Println("Client v0.2")

	cnx, err := net.Dial("tcp", "localhost:8080")
	if err != nil {
		log.Fatalf("Erreur connexion : %v", err)
	}

	defer cnx.Close()

	fmt.Println("Envoi d'un message au serveur")
	fmt.Fprintf(cnx, "Appel du client vers le serveur...")
}
