package main

import (
	"fmt"
	"log"
	"net/http"
)

type serve struct{}

func (s serve) ServeHTTP(rep http.ResponseWriter, req *http.Request) {
	fmt.Fprintln(rep, "Réponse en retour.")
}

func main() {
	var s serve
	fmt.Println("Lancement du premier serveur Go.")
	if err := http.ListenAndServe(":8080", s); err != nil {
		log.Fatalf("Erreur ListenAndServe : %v", err)
	}
}
