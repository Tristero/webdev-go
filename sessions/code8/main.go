package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"

	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
)

type utilisateur struct {
	Username   string
	MotDePasse []byte
	Nom        string
	Prenom     string
	ID         string
	New        bool
}

// bases de données live : maps
// nom d'utilisateur ("username") -> utilisateur
var dbUsers = map[string]utilisateur{}

// ID session -> username
var dbIDs = map[string]string{}

// pointeur global de template
var tpl *template.Template

func init() {
	// initialisation du pointeur
	var err error
	tpl, err = template.ParseGlob("*.html")
	if err != nil {
		log.Fatalf("Erreur sur la récupération des templates :%v\n", err)
	}

	mdp, err := bcrypt.GenerateFromPassword([]byte("root"), bcrypt.DefaultCost)
	if err != nil {
		log.Fatalf("Impossible de créer un mot de passe : %v\n", err)
	}
	user := utilisateur{
		MotDePasse: mdp,
		ID:         uuid.New().String(),
		Username:   "jdoe",
		Nom:        "Doe",
		Prenom:     "John",
	}
	// enregistrement d'un utilisateur
	dbUsers[user.Username] = user
}

func main() {
	fmt.Println("Lancement du serveur 8")

	http.HandleFunc("/", racine)
	http.HandleFunc("/inscription", inscription)
	http.HandleFunc("/login", login)
	http.HandleFunc("/compte", compte)
	http.HandleFunc("/logout", logout)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func racine(rep http.ResponseWriter, req *http.Request) {

	// Vérification si le cookie de session existe
	connecte := dejaConnecte(req)

	// connecte ne nous dit pas si le client dispose d'un ancien cookie
	if !connecte {
		c, err := req.Cookie("sid")
		if err == nil {
			c.Value = ""
			c.MaxAge = -1
			http.SetCookie(rep, c)
		}
	}

	if err := tpl.ExecuteTemplate(rep, "racine.html", connecte); err != nil {
		http.Error(rep, err.Error(), 500)
		log.Fatalf("Erreur sur le traitement du template : %v", err)
	}
}
