package main

import (
	"html/template"
	"log"
	"net/http"

	"golang.org/x/crypto/bcrypt"
)

var bErr bool = false

func login(rep http.ResponseWriter, req *http.Request) {

	// Déjà connecté : redirection
	if dejaConnecte(req) {
		http.Redirect(rep, req, "/compte", http.StatusSeeOther)
		log.Println("Déjà connecté : redirection vers le compte")
		return
	}

	// Vérification
	if req.Method == http.MethodPost {

		// l'utilisateur existe ?
		v, ok := dbUsers[req.FormValue("username")]
		if !ok {
			log.Println("Nom d'utilisateur erroné.")
			redirigeLogin(rep, req)
			return
		}

		// Le mot de passe est correct ?
		str := req.FormValue("motdepasse")

		//if string(mdp) != string(v.MotDePasse) {
		if err := bcrypt.CompareHashAndPassword(v.MotDePasse, []byte(str)); err != nil {
			log.Printf("Le mot de passe ne matche pas : %v\n", err)
			redirigeLogin(rep, req)
			return
		}

		// on crée le cookie de session
		c := &http.Cookie{
			Name:  "sid",
			Value: req.FormValue("username"),
		}
		// on stocke l'ID de session et l'ID utilisateur
		dbIDs[c.Value] = v.Username
		http.SetCookie(rep, c)

		log.Println("Aucune erreur : la redirection peut s'effectuer")
		bErr = false
		http.Redirect(rep, req, "/compte", http.StatusSeeOther)
		log.Println(v)
		return
	}

	tpl, err := template.ParseFiles("login.html")
	if err != nil {
		log.Printf("Erreur sur la récupération du template : %v\n", err)
		http.Error(rep, "Erreur de template", http.StatusInternalServerError)
	}

	if err = tpl.Execute(rep, bErr); err != nil {
		log.Printf("Erreur sur le traitement du template :%v\n", err)
		http.Error(rep, "Erreur sur le template", http.StatusInternalServerError)
	}
}
