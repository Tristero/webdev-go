package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Livre struct {
	Titre   string
	Auteur  Auteur
	Editeur string
	Pages   int
}

type Auteur struct {
	Nom    string
	Prenom string
}

func main() {

	// Création du client
	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://localhost:27017"))
	if err != nil {
		log.Fatalf("Erreur à la création du client : %v", err)
	}
	log.Println("Création du client réussie -> Connexion ?")

	// Connexion
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	if err := client.Connect(ctx); err != nil {
		log.Fatalf("Erreur à la connexion : %v", err)
	}
	log.Println("Connexion réussie -> Ping ?")

	if err := client.Ping(ctx, nil); err != nil {
		log.Fatalf("Erreur ping : %v", err)
	}
	log.Println("Ping réussi.")

	// Requête : créer un document
	// Création d'une variable pour le futur document :
	l := Livre{
		Titre: "Shakespeare, la biographie",
		Auteur: Auteur{
			Prenom: "Peter",
			Nom:    "Ackroyd",
		},
		Editeur: "Points",
		Pages:   764,
	}

	coll := client.Database("biblio").Collection("livres")
	// insertion du document
	rslt, err := coll.InsertOne(ctx, l)
	if err != nil {
		log.Printf("Erreur dans l'insertion du document : %v", err)
	} else {
		// on afficle l'ID automatiquement retourné à l'insertion
		fmt.Printf("Document inséré : %v\n", rslt.InsertedID)
	}

	// déconnexion
	if err := client.Disconnect(ctx); err != nil {
		log.Fatalf("Erreur à la déconnexion : %v", err)
	}

	fmt.Println("Déconnexion")
}
