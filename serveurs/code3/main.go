package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
)

var numClient int

func main() {
	fmt.Println("Serveur Go v0.2")
	lis, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatalf("Erreur sur le listener : %v", err)
	}

	for {
		cnx, err := lis.Accept()
		if err != nil {
			log.Fatalf("Erreur sur l'acceptation : %v", err)
		}

		go gereCnx(cnx)
	}
}

// Fonction de gestion des connexions 👍
func gereCnx(cnx net.Conn) {
	// on pourrait protéger l'accès avec Lock()
	numClient++
	// on fait une copie locale
	cnxNB := numClient
	fmt.Println("Une connexion entrante a été demandée - numéro", cnxNB)

	// scanne de la connexion entrante
	scanner := bufio.NewScanner(cnx)
	// boucle de lecture
	for scanner.Scan() {
		fmt.Println(scanner.Text())
	}
	defer cnx.Close()

	// Le code qui suit ne sera pas affiché tant que la connexion ne sera pas achevée
	fmt.Printf("Client n°%d a fermé sa connexion !\n", cnxNB)
}
