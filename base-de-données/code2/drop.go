package main

import (
	"fmt"
	"net/http"
)

func drop(rep http.ResponseWriter, req *http.Request) {
	stmt, err := db.Prepare("DROP TABLE clients")
	check(err, "sur la préparation du DROP")

	defer stmt.Close()

	_, err = stmt.Exec()
	check(err, "sur la récupération du nb de suppression")

	fmt.Fprintln(rep, "La table clients a bien été supprimée !")
}
