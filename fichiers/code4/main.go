package main

import (
	"fmt"
	"net/http"
)

func main() {
	fmt.Println("Serveur de fichiers v4")

	http.ListenAndServe(":8080", http.FileServer(http.Dir("../images")))
}
