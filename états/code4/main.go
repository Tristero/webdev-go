package main

import (
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"net/http"
)

func main() {
	fmt.Println("Lancement serveur v4")
	http.HandleFunc("/", racine)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func racine(rep http.ResponseWriter, req *http.Request) {

	var rslt string

	// Traiter les données reçues
	if req.Method == http.MethodPost {
		// ouvrir
		f, h, err := req.FormFile("fichier")
		if err != nil {
			http.Error(rep, "Impossible d'ouvrir le fichier", 500)
			log.Fatal("Impossible d'ouvrir le fichier")
		}
		defer f.Close()

		// affichage des 3 valeurs retournées par FormFile
		fmt.Printf("Fichier : %v\nEntêtes: % v\nErreur: %v\n", f, h, err)

		// ouverture et lecture du contenu de f
		slc, err := ioutil.ReadAll(f)
		if err != nil {
			http.Error(rep, "Erreur sur la lecture du contenu", 500)
			log.Fatalf("Erreur sur le traitement du fichier : %v", err)
		}
		rslt = string(slc)
	}

	// affiche le formulaire
	tpl, err := template.ParseFiles("template.html")
	if err != nil {
		log.Fatalf("Erreur sur le fichier template à charger : %v", err)
	}

	if tpl.ExecuteTemplate(rep, "template.html", rslt) != nil {
		http.Error(rep, "Erreur parsing template", 500)
		log.Fatalf("Erreur sur le parsing du template : %v", err)
	}
}
