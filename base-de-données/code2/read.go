package main

import (
	"fmt"
	"net/http"
	"strings"
)

func read(rep http.ResponseWriter, req *http.Request) {

	// on aura intérêt à donner le nom de chaque champs : nom et prenom dans la requête
	rows, err := db.Query("select * from clients")
	check(err, "sur les rows")

	defer rows.Close()

	var str strings.Builder
	for rows.Next() {
		var (
			prenom string
			nom    string
		)

		err := rows.Scan(&nom, &prenom)
		check(err, "du scan des de l'enregistrement")
		str.WriteString(prenom + " " + nom + "\n")
	}

	// en cas d'erreur après l'itération
	check(rows.Err(), "")
	fmt.Fprintf(rep, str.String())
}
