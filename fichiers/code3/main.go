package main

import (
	"fmt"
	"net/http"
)

func main() {
	fmt.Println("Serveur v3")
	http.HandleFunc("/", img)
	http.ListenAndServe(":8080", nil)
}

func img(rep http.ResponseWriter, req *http.Request) {
	http.ServeFile(rep, req, "../images/img.png")
}
