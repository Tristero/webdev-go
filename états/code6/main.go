package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
)

func main() {
	fmt.Println("Lancement du serveur 6")
	http.HandleFunc("/", racine)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func racine(rep http.ResponseWriter, req *http.Request) {

	var rslt = make([]byte, req.ContentLength)

	_, err := req.Body.Read(rslt)
	if err != nil {
		http.Error(rep, "Erreur lecture du contenu du fichier envoyé", 500)
		log.Fatalf("Erreur sur le contenu du fichier : %v ", err)
	}

	tpl, err := template.ParseFiles("template.html")
	if err != nil {
		http.Error(rep, "Erreur sur la récupération du template", 500)
		log.Fatalf("Erreur à la récupération du template : %v", err)
	}

	if tpl.ExecuteTemplate(rep, "template.html", string(rslt)) != nil {
		http.Error(rep, "Erreur à l'exécution du template", 500)
		log.Fatalf("Erreur à l'exécution du template : %v", err)
	}
}
