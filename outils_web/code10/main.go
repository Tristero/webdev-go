package main

import (
	"encoding/json"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"os"
)

type Auteur struct {
	Nom    string `json:"nom"`
	Prenom string `json:"prenom"`
}
type Livre struct {
	Titre   string `json:"titre"`
	Auteur  Auteur `json:"auteur,omitempty"`
	ISBN    string `json:"isbn"`
	NbPages int    `json:"pages"`
}

const donnees = `{
	"titre": "obabakoak",
	"auteur": {
		"nom":"Atxaga",
		"prenom":"Bernardo"
	},
	"isbn": "9782267019155",
	"pages": 409
}
`

// Templating
var tpl *template.Template

func init() {
	var err error
	tpl, err = tpl.ParseFiles("rslt.html")
	if err != nil {
		log.Fatalf("Erreur template : %v\n", err)
	}
}

func main() {
	fmt.Println("Lancement du serveur JSON 2")

	http.HandleFunc("/", racine)
	http.HandleFunc("/unmarshal", unmarshaler)
	http.HandleFunc("/decode", decoder)

	http.Handle("favicon.ico", http.NotFoundHandler())
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func racine(rep http.ResponseWriter, req *http.Request) {
	rep.Header().Set("Content-Type", "text/html; charset=utf-8")
	html := `
	<h2>Décodage de JSON</h2>
	<p>
		<a href="/unmarshal">Unmarshal</a>
		<br/>
		<a href="/decode">Decoder</a>
	</p>
	`
	//rep.Write([]byte(html))
	fmt.Fprintln(rep, html)
}

func unmarshaler(rep http.ResponseWriter, req *http.Request) {

	lvr := &Livre{}
	err := json.Unmarshal([]byte(donnees), lvr)
	if err != nil {
		log.Printf("Erreur sur le décodage : %v\n", err)
		http.Error(rep, "Erreur de décodage", http.StatusInternalServerError)
		return
	}

	// Templating
	if err = tpl.Execute(rep, lvr); err != nil {
		log.Printf("Erreur dans l'exécution (template) : %v\n", err)
		http.Error(rep, "Erreur template", http.StatusInternalServerError)
	}

}

func decoder(rep http.ResponseWriter, req *http.Request) {
	f, err := os.OpenFile("livre.json", os.O_RDONLY, 0666)
	if err != nil {
		log.Printf("Erreur sur la lecture du fichier : %v\n", err)
		http.Error(rep, "Erreur sur la récupération du JSON", http.StatusNotFound)
	}
	// Ne pas oublier de fermer :
	defer f.Close()

	decoder := json.NewDecoder(f)
	lvr := &Livre{}
	if err = decoder.Decode(lvr); err != nil {
		log.Printf("Erreur dans le décodage : %v\n", err)
		http.Error(rep, "Erreur dans le décodage", http.StatusInternalServerError)
		return
	}

	// Templating
	if err = tpl.Execute(rep, lvr); err != nil {
		log.Printf("Erreur dans l'exécution (template) : %v\n", err)
		http.Error(rep, "Erreur template", http.StatusInternalServerError)
	}
}
