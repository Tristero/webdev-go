package main

import (
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
)

func main() {
	fmt.Println("Lancement serveur de fichiers v1")
	http.HandleFunc("/", imgLocale2)
	http.HandleFunc("/erreur", imgLocale1)
	log.Fatalln(http.ListenAndServe(":8080", nil))
}

func imgLocale1(rep http.ResponseWriter, req *http.Request) {
	rep.Header().Set("Content-Type", "text/html")
	fmt.Fprintf(
		rep,
		`<img src="../images/img.png">`,
	)
}

func imgLocale2(rep http.ResponseWriter, req *http.Request) {

	f, err := os.Open("../images/img.png")
	if err != nil {
		http.Error(rep, "Fichier introuvable", 404)
		log.Fatalf("Erreur fichier image : %v", err)
	}
	defer f.Close()

	_, err = io.Copy(rep, f)
	if err != nil {
		log.Fatalf("Erreur de copie : %v", err)
	}

}
