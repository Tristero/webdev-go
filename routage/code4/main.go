package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {

	fmt.Println("Lancement du serveur multiplex v4")

	http.HandleFunc("/chien/", func(rep http.ResponseWriter, req *http.Request) {
		fmt.Fprintf(rep, "Page consacrée aux chiens")
	})

	http.HandleFunc("/chat", func(rep http.ResponseWriter, req *http.Request) {
		fmt.Fprintf(rep, "Page consacrée aux chats")
	})

	if err := http.ListenAndServe(":8080", nil); err != nil {
		log.Fatalf("Erreur ListenAndServe : %v", err)
	}
}
