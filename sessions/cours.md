# Sessions

On utilisera des identifiants uniques associés avec des cookies. A chaque utilisateur sera associé un identifiant unique. En production, il faudra penser à utiliser des communications sécurisées en HTTPS.

## 1-UUID

On utilisera les UUID : et on importera un package développé par Google et reposant sur le [RFC 4122](https://tools.ietf.org/html/rfc4122) et le [DCE 1.1](https://pubs.opengroup.org/onlinepubs/9629399/apdxa.htm)

Package : libre et gratuit, disponible [https://github.com/google/uuid](https://github.com/google/uuid)

Son utilisation est très simple :

```go
import (
    "fmt"

    "github.com/google/uuid"
)

func main() {
    rslt := uuid.New()
    fmt.Printf("type %T\nvaleur : %v\n", rslt, rslt.String())
}
```

Associons UUID et cookies :

On va créer une simple page qui va vérifier si le cookie existe avec ```Request.Cookie()```.

- s'il existe on affiche la valeur de l'ID
- sinon on le crée

Rappel : ```Request.Cookie(cle)``` retourne un pointeur de ```Cookie``` et une erreur.

```go
func racine(rep http.ResponseWriter, req *http.Request) {
    c, err := req.Cookie("ID")
    if err != nil {
        // Création du cookie
        v := creationCookie(rep)
        // On affiche le contenu
        fmt.Fprintf(rep, "<p>Un nouveau cookie a été créé avec comme valeur : %s</p>", v)
        return
    }
    // En revanche si le cookie existe :
    fmt.Fprintf(rep, `<p>Le cookie enregistré avec pour valeur valeur = %s</p>`, c.Value)
    fmt.Printf("Cookie existant : %s\n", c.Value)
}

func creationCookie(rep http.ResponseWriter) string {
    rslt := uuid.New()
    fmt.Printf("ID : %v\n", rslt.String())

    c := http.Cookie{
        Name:     "ID",
        Value:    rslt.String(),
        HttpOnly: true,
    }
    http.SetCookie(rep, &c)
    return rslt.String()
}
```

**NB** nous avons rajouté un champ au type ```Cookie``` : ```HttpOnly```. Ce champ ne permet pas à du code JS d'accéder aux données. C'est un moyen supplémentaire de sécuriser.

Code [Go](code1/main.go)

## 2-Sessions uniques

On devra disposer de deux ID : un pour la session et un se référant à l'utilisateur.

Donc nous créerons un type ```Utilsateur``` avec deux champs ```string```. Nous créerons deux maps qui servirons de bases de données :

- une sauvegardant les données utilisateurs
- une sauvegardant les ID (session et utilisateur)

Code :

```go
type utilisateur struct {
    Username string
    Nom      string
    Prenom   string
    ID       string
    New      bool
}

// bases de données live : maps
var dbUsers = map[string]utilisateur{}
var dbIDs = map[string]string{} // ID session / ID utilisateur
```

Au niveau du serveur nous devrons créer plusieurs routes :

- racine qui n'affichera que deux liens : première connexion et se connecter à son compte. On intégrera un formulaire
- affichage des données : est-ce un ancien ou un nouvel utilisateur

Concernant les cookies : nous devons créer un cookie de session unique (lorsqu'il y a connexion à la racine et que le cookie existe déjà : on le supprimera)

On créera deux routes simples :

```go
http.HandleFunc("/", racine)
http.HandleFunc("/compte", connexion)
```

### Gestion du formulaire

On vérifie si un cookie avec le nom ```sid``` existe : si oui on le détruit, sinon on le crée :

```go
func racine(rep http.ResponseWriter, req *http.Request) {
    c, err := req.Cookie("sid")
    if err == nil {
        // Création d'un cookie avec un nouvel ID de session
        fmt.Printf("Cookie existant : suppression du sid = %s\n", c.Value)
        c.MaxAge = -1
    } else {
        fmt.Println("Première connexion : création du cookie !")
    }
    nv := http.Cookie{
        Name:  "sid",
        Value: uuid.New().String(),
    }
    http.SetCookie(rep, &nv)
    fmt.Println("Connexion au site : nouveau cookie généré")

    tpl, err := template.ParseFiles("template.html")
    if err != nil {
        http.Error(rep, err.Error(), 500)
        log.Fatalf("Erreur sur la récupération du template : %v", err)
    }
    if tpl.Execute(rep, nil) != nil {
        http.Error(rep, err.Error(), 500)
        log.Fatalf("Erreur sur le traitement du template : %v", err)
    }
}
```

### Accès aux données

On doit vérifier beaucoup de points : y a-t-il un cookie avec un nom ```sid``` ? non ? alors on redirige. Est-ce qu'on dispose d'un utilisateur avec le nom d'utilisateur passé par le formulaire ? oui on change la donnée pour le champ ```New``` car ce champ est réservé aux nouveaux inscrits et on récupère les données. Sinon on crée l'utilisateur. Dans les deux cas on récupère les données pour les afficher dans le template idoine.

Code :

```go
func connexion(rep http.ResponseWriter, req *http.Request) {

    // Vérifications
    // est-ce que le SID existe ?
    c, err := req.Cookie("sid")
    if err != nil {
        // redirection
        http.Redirect(rep, req, "/", http.StatusSeeOther)
        log.Println("Essai d'intrusion sans SID")
    }

    if req.Method == http.MethodPost {
        var u = utilisateur{}

        // est-ce que l'on a un utilisateur associé ? vérification avec le username

        u, ok := dbUsers[req.FormValue("username")]
        if ok {
            // l'utilisateur n'est plus nouveau :
            u.New = false
            // l'utilisateur existe : on récupère son ID depuis son nom d'utilisateur
            id := dbUsers[u.Username].ID
            dbIDs[c.Value] = id
        } else {
            // si n'existe pas alors on crée un nouvel utilisateur
            // récupération depuis le formulaire des données
            u.Username = req.FormValue("username")
            u.Nom = req.FormValue("nom")
            u.Prenom = req.FormValue("prenom")
            // génère un nouvel ID pour le nouvel utilisateur
            u.ID = uuid.New().String()
            // c'est un nouvel utilisateur
            u.New = true
            // ajout du nouvel utilisateur
            dbUsers[u.Username] = u
            dbIDs[c.Value] = u.ID
        }

        // on exécute notre template :
        tpl, err := template.ParseFiles("compte.html")
        if err != nil {
            http.Error(rep, err.Error(), 500)
        }

        tpl.ExecuteTemplate(rep, "compte.html", u)
    }
}
```

### Problèmes de sécurité

Le code n'est pas exempt de trous de sécurité. Par exemple, il faudra vérifier que le nom d'utilisateur n'existe pas au préalable dans la base de données. Il n'y a pas de mot de passe. Le code est très sale.

Code [Go](code2/main.go) / [HTML : racine](code2/template.html) / [HTML : compte](code2/compte.html)

## 3-sign-up

Nous allons améliorer notre code précédent en gérant le nom d'utilisateur déjà préexistant.

Puis nous vérifierons si l'utilisateur est déjà connecté.

Nous allons modifier notre template racine du site : le formulaire ne comprendra qu'un seul champ : nom d'utilisateur et un bouton de soumission. Et nous rajouterons un lien de connexion pour créer un nouveau compte.

Donc nous devrons aussi créer un nouveau template pour créer un profil.

Si tout est bon, on pourra se connecter à son compte.

### 1-Définition des routes

Nous allons créer plusieurs routes:

- racine
- inscription
- compte

```go
func main() {
    fmt.Println("Lancement du serveur 3")

    http.HandleFunc("/", racine)
    http.HandleFunc("/inscription", inscription)
    http.HandleFunc("/compte", compte)
    log.Fatal(http.ListenAndServe(":8080", nil))
}
```

### 2-Racine du site

Nous devons savoir si un cookie de session existe ? si oui, on le détruit. Sinon on ne crée rien. La création se passera seulement à l'accès au compte ou à l'inscription. Ce sera à notre serveur de rediriger ou non si l'utilisateur existe ou non, etc.

Donc le code pour la racine sera le plus simple :

```go
func racine(rep http.ResponseWriter, req *http.Request) {

    tpl, err := template.ParseFiles("racine.html")
    if err != nil {
        http.Error(rep, err.Error(), 500)
        log.Fatalf("Erreur sur la récupération du template : %v", err)
    }
    if tpl.Execute(rep, nil) != nil {
        http.Error(rep, err.Error(), 500)
        log.Fatalf("Erreur sur le traitement du template : %v", err)
    }
}
```

### 3-Inscription

Nous devons faire deux choses :

- récupérer les données
- vérifier celles-ci avant de rediriger vers le bon service !

Il nous faut gérer plusieurs choses.

Tout d'abord on veut savoir si l'utilisateur est-il déjà connecté ? si oui on redirige. On vérifie non seulement la présence d'un ID de session mais aussi et surtout que cet ID de session correspond à un UUID existant !

On va créer un fichier "outils.go" dans lequel on va gérer nos fonctions de ce type.

```go
func dejaConnecte(req *http.Request) bool {
    c, err := req.Cookie("sid")
    if err != nil {
        return false
    }
    // on va vérifier aussi que le sid est relié à un uuid !
    _, ok := dbIDs[c.Value]
    return ok
}
```

Au niveau du formulaire, comment le traiter ? devons-nous créer une autre route ? En fait le formulaire pointera sur lui-même. On va traiter les infos localement et ensuite rediriger. Contrairement au projet précédent, on va pas envoyer le formulaire à une autre page bien on va rester sur notre page d'inscription et **rediriger** !

Le code sera bien plus léger :

```go
func inscription(rep http.ResponseWriter, req *http.Request) {

    // si l'utilisateur est déjà connecté et référencé
    if dejaConnecte(req) {
        fmt.Println("Utilisateur déjà enregistré : redirection vers le compte")
        http.Redirect(rep, req, "/compte", http.StatusSeeOther)
        return
    }

    // traitement du formulaire
    if req.Method == http.MethodPost {

        u := utilisateur{}

        u.ID = uuid.New().String()
        u.New = true
        u.Nom = req.FormValue("nom")
        u.Prenom = req.FormValue("prenom")
        u.Username = req.FormValue("username")

        // création du cookie de session
        c := &http.Cookie{
            Name:  "sid",
            Value: uuid.New().String(),
        }
        http.SetCookie(rep, c)
        // enregistrement des données
        dbIDs[c.Value] = u.ID
        fmt.Printf("Données IDs : %v\n", dbIDs)
        dbUsers[u.ID] = u
        fmt.Printf("Données users : %v\n", dbUsers)

        // on redirige :
        http.Redirect(rep, req, "/compte", http.StatusSeeOther)
        return
    }

    // traitement du template
    tpl, err := template.ParseFiles("inscription.html")
    if err != nil {
        http.Error(rep, "Erreur récupération du template", http.StatusInternalServerError)
        log.Printf("Erreur sur la récupération du template %v\n", err)
    }

    if tpl.Execute(rep, nil) != nil {
        http.Error(rep, "Erreur traitement du template", http.StatusInternalServerError)
        log.Printf("Erreur sur le traitement du template %v\n", err)
    }
}
```

### Accès au compte

On vérifie que l'on dispose d'un utilisateur avec son cookie de session à l'aide de notre fonction ```dejaConnecte()```

On va se crée une nouvelle fonction ```recupUtilisateur()``` qui retournera un type ```utilisateur``` soit vide soit avec des valeurs.

Code :

```go
func recupUtilisateur(req *http.Request) utilisateur {
    c, err := req.Cookie("sid")
    if err != nil {
        return utilisateur{}
    }
    uid := dbIDs[c.Value]
    return dbUsers[uid]
}
```

On traitera ensuite les données pour les afficher. Mais on aura un problème : nous affichons un message si l'utilisateur est nouveau ou non. Il n'est nouveau qu'avec son incription ! Donc on doit copier la structure dans une nouvelle variable temporaire :

```go
func compte(rep http.ResponseWriter, req *http.Request) {

    // si utilisateur essie de se connecter sans en avoir le droit
    if !dejaConnecte(req) {
        http.Error(rep, "Accès interdit !", http.StatusForbidden)
        log.Println("Accès au compte interdit")
        return
    }

    // récupération des données
    u := recupUtilisateur(req)

    // on va modifier le nouvel utilisateur : dorénavant il ne le sera plus:
    // pour cela on doit faire une copie
    if u.New {
        old := u
        old.New = false
        dbUsers[old.ID] = old
    }

    // affiche le contenu des données de l'utilisateur
    tpl, err := template.ParseFiles("compte.html")
    if err != nil {
        log.Printf("Erreur sur la récup du template :%v\n", err)
        http.Error(rep, "Erreur sur le template", http.StatusInternalServerError)
    }

    if tpl.Execute(rep, u) != nil {
        log.Printf("Erreur sur le traitement du template :%v\n", err)
        http.Error(rep, "Erreur sur le template", http.StatusInternalServerError)
    }
}
```

### Problèmes

Aucun mot de passe n'est utilisé. Et aucun élément de déconnexion permettant la suppression du cookie de session n'a été implémenté : avec n'importe quel nom d'utilisateur on pourra récupérer les données d'un autre utilisateur !

Codes Go : [main](code3/main.go) / [outils](code3/outils.go) / [compte](code3/compte.go) / [inscription](code3/inscription.go)

Codes template : [racine](code3/racine.html) / [inscription](code3/inscription.html) / [compte](code3/compte.html)

## 4-Mot de passe

Pour cela nous utiliserons une bibliothèque fournie par Google : [bcrypt](https://pkg.go.dev/golang.org/x/crypto/bcrypt?tab=doc)

L'import sera "golang.org/x/crypto/bcrypt" : il fera peut-être l'objet d'une intégration future dans la bibliothèque standard.

Nous devons modifier notre structure "utilisateur" pour intégrer un nouveau mot de passe. Le type du nouveau champ est conditionné par ```bcrypt``` : un slice d'octets.

```go
type utilisateur struct {
    Username   string
    MotDePasse []byte
    Nom        string
    Prenom     string
    ID         string
    New        bool
}
```

Le package nous fournit une fonction ```GenerateFromPassword``` qui retourne deux éléments un slice d'octets et une erreur. Elle nécessite deux arguments :

- le mot de passe sous forme d'un slice d'octets
- un entier ```cost``` : il ajoute de la complexité à l'algorithme de hashage. Car ce qui sera conservé ne sera qu'un coda de hashage et non le mot de passe. Plus le "coût" sera élevé, plus le chiffrement/déchiffrement sera élevé. Pour cela nous disposons de 2 constantes : ```MinCost```, ```MaxCost``` et ```DefaultCost```.

Modifions notre template ```inscription.html``` pour intégrer au formulaire une zone pour créer le mot de passe :

```html
<body>
    <form action="/inscription" method="post">

        ...

        <label for="motdepasse">Mot de passe</label>
        <input type="password" name="motdepasse">
        <br>
        <input type="submit" value="Se connecter">
    </form>
</body>
```

Pour générer et stocker notre nouveau mot de passe : dans ```insciption.go```, on intègrera le code suivant (avant d'enregistrer les données dans notre base) :

```go
b, err := bcrypt.GenerateFromPassword([]byte(u.MotDePasse), bcrypt.DefaultCost)
if err != nil {
    http.Error(rep, "Erreur sur le mot de passe", http.StatusInternalServerError)
    log.Printf("Erreur sur la génération du hash : %v\n", err)
    return
    }
u.MotDePasse = b
```

Nous allons modifier le template ```compte.html``` pour y intégrer le champ mot de passe :

```html
<body>

    ...

    <p>
        Mot de passe {{.MotDePasse}}
    </p>
</body>
```

## 5-Login

Objectif : créer une page de login

Dans le précédent code on associait un ID de session avec un ID utilisateur. Il faut modifier cela : on va dorénavant associer un ID de session avec le nom d'utilisateur !

**Donc on changera toutes les références de type ```dbUsers[req.FormValue("username")]``` ou ```dbUsers[XXX]```.**

Pour créer et pouvoir tester notre page de login, il faut avoir au moins un utilisateur dans notre petite base : on va utiliser la fonction ```init()```.

```go
func init() {
    mdp, err := bcrypt.GenerateFromPassword([]byte("root"), bcrypt.DefaultCost)
    if err != nil {
        log.Fatalf("Impossible de créer un mot de passe : %v\n", err)
    }
    user := utilisateur{
        MotDePasse: mdp,
        ID:         uuid.New().String(),
        Username:   "jdoe",
        Nom:        "Doe",
        Prenom:     "John",
    }
    // enregistrement d'un utilisateur
    dbUsers[user.Username] = user
}
```

### 1-Template pour le login

Nous allons supprimer le formulaire présent dans ```racine.html``` et créer une page ```login.html``` :

```html
<body>
    {{if .}}
        <p style="font-weight: bold; color: red;">Le nom d'utilisateur et ou le mot de passe est faux.</p>
    {{end}}
    <form action="/login" method="post">

        <label for="username">Nom d'utilisateur</label>
        <input type="text" name="username">
        <br>
        <label for="motdepasse">Mot de passe</label>
        <input type="password" name="motdepasse">
        <br>
        <input type="submit" value="Se connecter">
    </form>
</body>
</html>
```

### 2-Code Go associé au login

Il va falloir créer une nouvelle route dans ```main.go``` :

```go
http.HandleFunc("/login", login)
```

Que devons-nous gérer ?

- On vérifie si le nom d'utilisateur existe.
- Si le nom d'utilisateur n'existe pas on le redirige à la racine du site.
- Si ce nom existe, on vérifie le mot de passe
- Si le mot de passe est incorrect: on redirige le client vers la page login à nouveau (avec la présence d'un message)
- Si tout OK, on crée un cookie de session et on récupère les données de l'utilisateur.
- Enfin si l'utilisateur est déjà connecté alors on le redirige vers son compte

A chaque erreur nous devons rediriger l'utilisateur avec un code : 302. On doit transformer la requête POST en GET ! Donc nous utiliserons une fonction helper qui évitera de taper du code inutilement :

Dans ```outils.go``` :

```go
func redirigeLogin(rep http.ResponseWriter, req *http.Request) {
    bErr = true
    http.Redirect(rep, req, "/login", 302)
    // Attention ici pas de return : il ne doit s'appliquer que sur le bloc d'instruction concerné
}
```

On remarque qu'il n'y a pas de ```return``` dans la fonction helper : ceci s'explique très simplement. Si on plaçait un return dans cette fonction le code ne s'arrêterait après l'une des vérifications erronées. Le ```return``` ne s'applique qu'au niveau du bloc de code. Donc si on employait ```return``` dans la fonction helper cela ne nous servirait à rien puisque déjà la fonction ne retourne rien !

Comment comparer le mot de passe stocké avec le mot de passe passé par le login ? On ne peut comparer deux slices de bytes. de même si on essaie le code suivant :

```go
// Le mot de passe est correct ?
str := req.FormValue("motdepasse")
mdp, err := bcrypt.GenerateFromPassword([]byte(str), bcrypt.DefaultCost)

if err != nil {
    log.Println("Création du mot de passe de vérification erroné.")
    redirigeLogin(rep, req)
    return
}

if string(mdp) != string(v.MotDePasse) {
    log.Printf("Mot de passe non vérifié\nValeur du formulaire  : %v\nValeur stockée : %v", req.FormValue("motdepasse"), "root")
    log.Printf("Mot de passe non vérifié\nMDP généré du form  : %v\nMDP stocké : %v", string(mdp), string(v.MotDePasse))
    redirigeLogin(rep, req)
    return
}
```

**Il échouera tout le temps.**

Le package ```bcrypt``` dispose d'une fonction utile : ```CompareHashAndPassword()``` qui prend deux arguments et retourne une erreur.

Le premier argument est la valeur hashée de type ```[]byte``` et le second argument est le mot de passe à vérifier de type ```[byte]```. Le retour est ```nil``` si la comparaison matche sinon on a une erreur.

```go
if err := bcrypt.CompareHashAndPassword(v.MotDePasse, []byte(req.FormValue("motdepasse"))); err != nil {
    log.Printf("Le mot de passe ne matche pas : %v\n", err)
    redirigeLogin(rep, req)
    return
}
```

Code [Go login](code6/login.go) / [Template du login](code6/login.login.html)

### 3-Des améliorations

Il est à remarquer deux points :

- un trou de sécurité lié au nom d'utilisateur
- une optimisation sur les templates

Le code n'est pas parfait : on ne chiffre pas le nom d'utilisateur. Il circule en clair et donc il suffira de lancer une attaque par force brute sur le nom d'utilisateur.

Le mieux serait de chiffrer dans le cookie seulement.

Quant à l'optimisation, elle portera sur les templates. Il serait utile de ne pas avoir à créer des pointeurs de templates. On utilisera pour cela ```init()``` et un pointeur global pour pouvoir accéder à nos templates et gagner quelques lignes de code.

Code avec optimisation des templates :

[Go main](code7/main.go) / [Go inscription](code7/inscription.go) / [Go login](code7/login.go)

## 6-Logout

Nous avons vu comment "détruire" un cookie avec ```MaxAge = -1```. Puis nous venons de créer une page de connexion. Il nous reste à implémenter une page de déconnexion. Pour cela nous allons créer une nouvelle route : ```http.HandleFunc("/logout", logout)```
Mais nous n'aurons pas besoin de créer un template pour cela : on va rediriger l'utilisateur à la racine du site.

Il faut non seulement détruire le cookie de session mais il faut aussi supprimer sa référence dans la base dbIDs ! et pour plus de sécurité nous effacerons le contenu de la valeur :

```go
func logout(rep http.ResponseWriter, req *http.Request) {

    if !dejaConnecte(req) {
        http.Redirect(rep, req, "/", http.StatusSeeOther)
        return
    }
    // Pas besoin de vérifier l'erreur puisqu'elle a été gérée par la fonction dejaConnecte
    c, _ := req.Cookie("sid")

    fmt.Printf("Avant suppression, la base Id contient :%v\n", len(dbIDs))
    // suppression du username dans dbIDs
    delete(dbIDs, c.Value)
    fmt.Printf("Après suppression, la base Id contient :%v\n", len(dbIDs))

    // destruction du cookie et de sa valeur
    c.MaxAge = -1
    c.Value = ""
    http.SetCookie(rep, c)

    // redirection
    http.Redirect(rep, req, "/", http.StatusSeeOther)
}
```

Nous allons aussi modifier : les templates racine et compte pour y inclure un lien vers logout.

On ajoute ces lignes au template ```compte.html``` :

```html
<p>
    <a href="/logout">Déconnexion</a>
</p>
```

Pour le template ```racine.html```, nous allons détecté si le cookie existe déjà : notre template étant très simple. Nous allons pouvoir le modifier très simplement.

On va recourir à une simple variable locale booléenne : à chaque connexion sur racine, le code va vérifier si un cookie de session existe et si la valeur est reliée à quelque chose dans la base. Si les deux conditions sont reliées alors on affiche deux liens : vers le logout et vers le compte. Dans tous les cas, on affichera les deux autres liens déjà créés par nos soins (login et inscription). Pour nous simplifier la tâche : on utilisera notre fonction ```dejaConnecte()``` !

Code :

```html
<body>
    <h1>Hello, Gopher</h1>
{{if .}}
<p>
    <a href="/compte">Connexion à mon compte</a>
</p>
<p>
    <a href="/logout">Déconnexion de mon compte</a>
</p>
{{else}}
<p>Déjà inscrit ?<a href="./login">Connexion ici</a></p>
<p>Pas inscrit ? <a href="./inscription">s'inscrire ici</a></p>
{{end}}
</body>
```

Code Go

```go
func racine(rep http.ResponseWriter, req *http.Request) {
    // Vérification si le cookie de session existe
    connecte := dejaConnecte(req)

    // connecte ne nous dit pas si le client dispose d'un ancien cookie
    if !connecte {
        c, err := req.Cookie("sid")
        // s'il existe un cookie de session mais non répertorié (connecte = false)
        // alors on détruit le cookie
        if err == nil {
            c.Value = ""
            c.MaxAge = -1
            http.SetCookie(rep, c)
        }
    }

    if err := tpl.ExecuteTemplate(rep, "racine.html", connecte); err != nil {
        http.Error(rep, err.Error(), 500)
        log.Fatalf("Erreur sur le traitement du template : %v", err)
    }
}
```

Codes Go : [main](code8/main.go) / [logout](code8/logout.go) / [compte](code8/compte.go)
Codes HTML : [racine](code8/racine.html) / [compte](code8/compte.html)

## 7-Permissions

Il peut être intéressant de restreindre l'accès à certaines pages. Pour cela on va rajouter un nouveau champ dans le type ```utilisateur``` : ```Role```. Et l'on pourra pour toutes les pages nécessaires moduler l'accès aux pages en fonction du "niveau d'accréditation" alloué à l'utilisateur.

On pourra utiliser une énumération simple, des chaînes, ou cela sa convenance pour l'associer au champ ```Role```. Puis nous pourrons effectuer des redirections en fonction du niveau d'accréditation.

## 8-Expiration de la session

Il existe plusieurs méthodes pour gérer la fin de session :

- côté client, on fournit une date limite au niveau du cookie : avec ```MaxAge``` et une durée en nanoseconde
- côté serveur, on vérifie la dernière activité du client. Ce qui signifie que notre base de données de session doit conserver la dernière activité du client. Ou bien on crée une autre base de type map. Le plus simple est de modifier la base préexistante

Si on souhaite développer une session limitée côté serveur, il faudra

- soit créer une base pour chaque session soit changer la structure de la map. Si on change notre base ```dbIDs```, on aura une map ```map[string]session{}``` et le type ```session``` ressemblera à :

```go
type session struct{
    sid      string
    activite time.Time
}
```

- A chaque requête on vérifiera la valeur du champ ```activite``` et comparera avec une valeur constante (par exemple, ```dureeMaxSession```). On calculera la différence : en fonction du résultat, le serveur supprimera ou non le cookie de session et il faudra nettoyer la base ```dbIDs``` avec ```delete```.

La solution la plus simple est de modifier le ```MaxAge``` et de déporter le problème sur le client.
