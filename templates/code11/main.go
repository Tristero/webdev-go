package main

import (
	"log"
	"os"
	"text/template"
	"time"
)

var funcs = template.FuncMap{
	"fdt": formatTime,
}

func formatTime(t time.Time) string {
	return t.Format("02-01-2006")
}

func main() {

	t := time.Now()

	tpl := template.New("").Funcs(funcs)

	tpl, err := tpl.ParseFiles("fichier.txt")
	if err != nil {
		log.Fatalf("Erreur sur le parsing : %v", err)
	}

	err = tpl.ExecuteTemplate(os.Stdout, "fichier.txt", t)

	if err != nil {
		log.Fatalf("Erreur sur le traitement du template : %v", err)
	}
}
