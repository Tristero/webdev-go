package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

func main() {
	fmt.Println("Création d'un client avec un contexte.")
	opts := options.Client().ApplyURI("mongodb://localhost:27017")

	// Création d'un contexte à l'intérieur de WithTimeout
	ctx, cancel := context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()

	client, err := mongo.Connect(ctx, opts)
	if err != nil {
		log.Fatalf("Erreur à la connexion : %v", err)
	}

	log.Println("Essai de connexion au serveur")
	// On va créer un autre timeout : les deux sont indépendants
	ctx, cancel = context.WithTimeout(context.Background(), 2*time.Second)
	defer cancel()
	if err = client.Ping(ctx, nil); err != nil {
		log.Fatalf("Erreur au Ping : %v", err)
	}

	fmt.Println("Connexion réussie")

	if err = client.Disconnect(context.TODO()); err != nil {
		log.Fatalf("Erreur à la déconnexion : %v", err)
	}

	fmt.Println("Déconnexion du serveur")
}
