# Templates et Go

Le bibliothèque standard dispose de deux packages pour gérer les templates

- un pour les "textes" : ```text/template```
- un spécifique au HTML : ```html/template```

On pourrait utiliser la concaténation entre des bouts de code HTML et des variables, fonctions, ... puis on enregistre le contenu dans un fichier avec ```ìoutil```mais ce n'est pas efficace ni facilement gérable.

## 1-Templating textuel

### ParseFile et Execute

Nous allons être capable de récupérer le texte, l'analyser, et le manipuler selon nos besoins.

Tout d'abord on récupère un fichier :

```go
t, err := template.ParseFiles("texte1.gohtml")
```

La fonction ```ParseFiles()``` prend un paramètre *variatique* (des noms de fichiers sous forme de chaîne de caractère) et retourne deux valeurs :

- un pointeur de type ```Template``` que l'on manipulera
- une erreur

**NB** : l'import doit pointer vers ```text/template``` et **non** ```html/template```.

Puis on appelle ```Execute()```. Cette fonction retourne une simple erreur. En revanche elle nécessite deux arguments :

- un type ```io.Writer```
- des données (type ```interface{}```)

Donc on pourra "écrire" nos données sur tout élément supportant ```io.Writer``` donc des fichiers. Pour l'instant nous utiliserons, la sortie standard :

```go
if t.Execute(os.Stdout, nil) != nil {
    log.Fatal("Erreur à l'exécution")
}
```

Nous n'avons pas à passer de données pour l'instant.

On peut très bien remplacer la sortie standard par un fichier... voici un exemple :

[Code](code1/1-simple.go):

```go
func main() {
    t, err := template.ParseFiles("texte1.gohtml")
    if err != nil {
        log.Fatalf("Erreur au chargement du fichier : %v", err)
    }

    f, err := os.Create("resultat.html")
    if err != nil {
        log.Fatalf("Erreur à la création du fichier de sortie : %v", err)
    }

    defer f.Close()

    if t.Execute(f, nil) != nil {
        log.Fatal("Erreur à l'exécution")
    }
}
```

### Parsing de plusieurs fichiers

Que se passe-t-il si l'on utilise plusieurs fichiers ? On recourra à la méthode ```ExecuteTemplate()``` qui prend **3 arguments** :

- 1er : un type ```io.Writer```
- 2eme : le nom du fichier
- 3ème : données

```go
tpl, err := template.ParseFiles("fichier1.truc", "fichier2.truc")
...
err = tpl.ExecuteTemplate(os.Stdout, "fichier1.truc", nil)
...
err = tpl.ExecuteTemplate(os.Stdout, "fichier2.truc", nil)
...
```

Au lieu d'utiliser ```ParseFiles```, on pourra utiliser ```ParseGlob()``` qui requiert en argument une chaîne qui est un motif d'expression régulière sous forme de chaîne simple :

Par exemple :

- "*" : utilise tous les fichiers du répertoire courant
- "*.txt" : utilise tous les fichiers portant l'extension "txt
etc.

[Code](code2/main.go) :

```go
func main() {
    tpl, err := template.ParseGlob("*.txt")
    if err != nil {
        log.Fatalf("Erreur au chargement des fichiers : %v", err)
    }

    if tpl.ExecuteTemplate(os.Stdout, "fichier1.txt", nil) != nil {
        log.Fatalf("Erreur sur le traitement du fichier1 : %v", err)
    }

    if tpl.ExecuteTemplate(os.Stdout, "fichier2.txt", nil) != nil {
        log.Fatalf("Erreur sur le traitement du fichier2 : %v", err)
    }

    if tpl.ExecuteTemplate(os.Stdout, "fichier3.txt", nil) != nil {
        log.Fatalf("Erreur sur le traitement du fichier3 : %v", err)
    }
}
```

### Must()

```Must()``` est une fonction d'aide utilisée surtout pour l'initialisation de variable.

Dans notre [exemple](code3/main.go), on va à l'initialisation de notre programme, récupérer tous les fichiers mais aussi les erreurs.

Et comme pour ```ParseGlob()```, on va utiliser ```ExecuteTemplate()``` :

```go
var tpl *template.Template

func init() {
    tpl = template.Must(template.ParseGlob("*.txt"))
}

func main() {
    if tpl.ExecuteTemplate(os.Stdout, "fichier1.txt", nil) != nil {
        log.Fatal("Erreur sur fichier 1.")
    }

    if tpl.ExecuteTemplate(os.Stdout, "fichier2.txt", nil) != nil {
        log.Fatal("Erreur sur fichier 2.")
    }

    if tpl.ExecuteTemplate(os.Stdout, "fichier3.txt", nil) != nil {
        log.Fatal("Erreur sur fichier 3.")
    }
}
```

```Must()``` paniquera lorsqu'une erreur sera rencontrée ici à l'initialisation du programme.

### ParseFiles et ParseGlob

Ces deux noms sont soit des fonctions soit des méthodes. Soit on les appelle comme des fonctions traditionnelles : ```template.ParseFiles()```. Soit on les appelle depuis un pointeur de type ```*template.Template``` comme nous l'avons fait avec la fonction ```Must()```.

## 2-Passer des données

Dans le template (fichier), on utilisera les double-accolades ```{{}}``` dans lesquelles seront passées des expressions particulières.

Exemple simple :

```txt
Hello, {{.}}
```

Le point ici représente la valeur courante de la donnée à un moment donné de l'exécution : si l'on passe une seule donnée, ce sera celle-ci qui sera utilisée. Si l'on emploie un slice, la donnée sera celle qui sera pointée par l'index courant. Nous verrons cela dans les prochains exemples.

Nous passerons une chaîne "world" :

[Code Go](code4/main.go) :

```go
func main() {
    tpl, err := template.ParseFiles("fichier.txt")
    if err != nil {
        log.Fatalf("Erreur sur la récupération du fichier : %v", err)
    }

    if tpl.Execute(os.Stdout, "world") != nil {
        log.Fatalln("Erreur sur le passage de données")
    }
}
```

On peut réécrire ce code  comme suit avec un pointeur de type ```template.Template``` :

```go
var tpl *template.Template

func init() {
    tpl = template.Must(template.ParseFiles("fichier.txt"))
}

func main() {
    if tpl.ExecuteTemplate(os.Stdout, "fichier.txt", "world") != nil {
        log.Fatalln("Erreur sur le passage de données")
    }
}
```

## 3-Passage de valeurs à des variables

Dans le fichier à parser, il est possible de passer une valeur à une variable. La variable est toujours préfixée par ```$```. L'affection s'effectue avec l'opérateur ```:=```.

Exemple :

```go
{{$variable := .}}
```

Et cette variable est accessible n'importe dans le template, très simplement ```{{$wisdom}}```.

Code :  [Go](./code5/main.go)
        [HTML](code5/fichier.html)

## 4-Passer des structures composites

### les slices

Code [Go](code6/main.go) / [Template](code6/fichier.txt)

On se crée un slice de strings et notre objectif est des les afficher ligne à ligne.

Go nous fournit pour le template une syntaxe particulière :

```go
{{range .}} // chaque valeur passée par la
... // code quelconque
    {{.}}
{{end}}
```

Donc ```range``` va itérer sur la donnée qui a été transmise autrement dit le slice. Et chaque valeur est affectée à ```.```

Mais on peut aller encore plus loin en utilisant ```range``` pour récupérer à partir de la donnée courante, la valeur et son index :

```go
{{range $index, $element := .}}
    ... //code
    {{$index}} ... {{$element}}
    ... // code
{{end}}
```

### les maps

Codes : [Go](code7/main.go) / [Template](code7/fichier.txt)

Pour gérer les valeurs d'une map, la bibliothèque standard nous propose : toujours ```range```. Mais si on utilise ```{{range .}}```, la valeur ```.``` représente ... la valeur ! En fait le point retourne deux éléments 👍

- 1 : la clé
- 2 : la valeur

Syntaxe :

```go
{{range $clé, $valeur := .}}
    // code
    {{$clé}}
    // code
    {{$valeur}}
    //code
{{end}}
```

### les structs

Codes : [Go](code8/main.go) / [Template](code8/main.go)

Une structure est composée de champs divers. Donc pour y accéder, on appelle le nom du champ désiré.

Pour un template c'est exactement la même chose en se souvenant que la valeur en cours est représentée sous la forme d'un point :

```go
{{.Champ1}}
// code
{{.Champ2}}
...
```

On peut aller plus loin en utilisant des variables auxquelles on affecte les valeurs des champs :

```go
{{$val1 := .Champs1}}
{{$val2 := .Champs2}}
...
```

### les slices de structs

Codes : [Go](code9/main.go) / [Template](code9/fichier.txt)

On manipule à la fois ```{{range .}}``` et l'accès aux champs.

Voici un exemple 👍

```go
type pays struct {
    Capitale string
    Pays     string
}

var slice = []pays{
    pays{
        Capitale: "Berlin",
        Pays:     "Allemagne",
    },
    ...
}
```

Et côté du template :

```txt
Liste de quelques pays européens et de leur capitales:

{{range .}}
En {{.Pays}}, la capitale est {{.Capitale}}.
{{end}}
```

Bien entendu, on pourra faire des structures de structures... et les manipuler tout aussi simplement.

Le plus important est de bien réfléchir sur la structure de données.

## 5-Fonctions et templates

### Premier exemple et présentation

Il peut-être parfois intéressant d'injecter des fonctions dans un template : attention à ne pas faire n'importe quoi pour des questions de sécurité.

De même le code à mettre en place est précis et demande un peu de réflexion.

Pour injecter une fonction, la bibliothèque standard nous fournit : ```Funcs()```. Cette méthode requiert en argument un type très particulier : ```FuncMap```. Ce type est une map de type ```map[string]interface{}```.

```FuncMap``` permet de stocker les fonctions que l'on souhaite appliquer :

```go
template.FuncMap{
    "nomDansLeTemplate": nomDeLaFonctionGo,
    ...
}
```

Et on peut enregistrer autant de fonctions que l'on souhaite !

Pour notre exemple, on va appeler une fonction de la bibliothèque standard et créer une fonction :

- ```strings.ToUpper```
- ```justeTrois``` qui va retourner 3 lettres.

Et nous allons passer ces fonctions à une variable de type ```template.FuncMap``` :

```go
// Fonction pour le templating
func justeTrois(s string) string {
    s = strings.TrimSpace(s)
    return s[:3]
}

// Enregistrement des fonctions:

var fonctions = template.FuncMap{
    "upc": strings.ToUpper,
    "jt":  justeTrois,
}
```

On crée une structure de données de type ```[]struct{}```

Comment utiliser tout cela ? Là il va falloir faire très attention à la succession des appels de fonctions.

Si on utilise le code suivant :

```go
tpl, err := template.ParseFiles("fichier.txt")
if err != nil {
    log.Fatalf("erreur sur le parsing : %v", err)
}

tpl = tpl.Funcs(fonctions)
```

Ce code va échouer : en indiquant ```"upc" not defined```. Ceci est logique : on ne peut utiliser une variable qui n'a pas été au préalable définie. On doit présenter en premier les fonctions à utiliser PUIS ensuite on va parser. Logique.

Donc il faut recourir à ce code :

```go
tpl := template.New("").Funcs(fonctions)

tpl, err := tpl.ParseFiles("fichier.txt")
if err != nil {
    log.Fatalf("erreur sur le parsing : %v", err)
}
```

Que l'on pourra raccourcir :

```go
tpl, err := template.New("").Funcs(fonctions).ParseFiles("fichier.txt")
if err != nil {
    log.Fatalf("erreur sur le parsing : %v", err)
}
```

Observez la présence de ```New("")``` : il alloue un pointeur de template vide avec un nom passé en paramètre (ici on ne s'intéresse pas à son nom, on ne veut qu'un pointeur).

L'utilisation de ```New()``` nous oblige à utiliser ```ExecuteTemplate```. Si on utilise ```Execute``` Go ne pourra pas trouver le template associé !

```go
err = tpl.ExecuteTemplate(os.Stdout, "fichier.txt", tabPays)
if err != nil {
    log.Fatalf("Ouh erreur : %v", err)
}
```

Côté template :

On appellera le nom de la méthode suivi de la valeur courante. Comme nous utilisons, un tableau de structures on rajoutera le champ dont on veut modifier la valeur :

```txt
Liste :
{{range .}}
En {{.Pays}} ({{jt .Pays}}), la capitale est {{upc .Capitale}}.
{{end}}
```

Code [GO](./code10/main.go) | [Template](code10/fichier.txt)

### Second exemple

Pour cela nous allons utiliser le package ```time```. Nous allons récupérer une heure et nous allons la retraiter :

Nous allons passer de ce format (fourni avec ```time.Now()```) : "2020-02-27 16:33:45.614614052 +0100 CET m=+0.000068473" à "27/02/2020" à un format plus simple : "27-02-2020" (autrement dit un format *jour-mois-année*).

Donc nous allons créer une fonction qui prendra en charge une date de type ```time.Time``` (on récupèrera la date du système) et retournera une chaîne.

Pour traiter les dates, la bibliothèque standard nous fournit le package ```time``` et tous les outils nécessaures :

- Obtenir la date actuelle : ```time.Now()```
- Formater une date : il faut un objet ```time.Time``` et lui appliquer une fonction ```Format(strFormat)```

Notre code de formatage de date ressemblera à :

```go
func formatTime(t time.Time) string {
    return t.Format("02-01-2006")
}
```

On crée une variable globale de type ```FuncMap``` :

```go
var funcs = template.FuncMap{
    "fdt": formatTime,
}
```

Et on reprend le code précédent pour traiter notre template :

```go
func main() {

    t := time.Now()

    tpl := template.New("").Funcs(funcs)

    tpl, err := tpl.ParseFiles("fichier.txt")
    if err != nil {
        log.Fatalf("Erreur sur le parsing : %v", err)
    }

    err = tpl.ExecuteTemplate(os.Stdout, "fichier.txt", t)

    if err != nil {
        log.Fatalf("Erreur sur le traitement du template : %v", err)
    }
}
```

Et notre template :

```txt
Sans formatage : Nous sommes le {{.}}

Avec formatage : Nous sommes le {{fdt .}}
```

Codes : [Go](code11/main.go)/[Template](code11/fichier.txt)

## 6-Pipelines

Comme avec Bash, Zsh, Powershell, F#, etc. on va pouvoir chaîner les appels de fonctions avec l'opérateur template ```|``` (le même opérateur que pour Bash et consorts, ou F#, Haskell)

Nous allons créer 3 fonctions :

- ```db(i int) int``` qui retourne le double de i
- ```rc(i int) float64``` retourne la racine carré de i : attention au type pris en charge par ```math.Sqrt()``` c'est un ```float64```
- ```carre(i int) int``` retourne le carré de i

Pour chaîner des opérations dans un template  :

```txt
Calcul plus complexe {{ db . | carre | rc}}
```

Ici on calcule le double de la valeur courante puis son carré pour revenir à sa racine carré.

Dans le code proposé, on utilise un tableau de valeurs entières.

**NB** : dans nos templates nous avons passé le nom d'une fonction suivi du point. Mais on peut très bien traiter ceci comme un pipeline :

```txt
{{fonction .}}
```

Equivaut à :

```txt
{{. | fonction}}
```

Code [Go](code12/main.go) / [Template](code12/fichier.txt)

## 7-Fonctions globales prédéfinies dans des templates

La bibliothèque standard dispose de fonctions prédéfinies : ```and```, ```call```, ```html```, ```index```, ... ainsi que des opérateurs de comparaison : ```eq```, ```lt```, ...

Explorons quelques-unes de ces fonctions :

### index

On crée un slice de chaines simples que l'on passera très simplement à notre template :

```go
str := []string{"un", "deux", "trois", "quatre", "cinq"}
```

Le code Go est le plus simple possible. Tout le travail va se réaliser pour nous.

La fonction prédéfinie ```index``` prend deux arguments : la valeur courante et un entier représentant un index.

Syntaxe du template :

```txt
{{index . index}}
```

Comme pour les tableaux traditionnels tout index supérieur ou égal à la longeur du table génère une erreur "out of range".

**NB** : on peut commenter son template avec le style de codage Go : ```/* ... */``` mais attention seulement à l'intérieur des doubles accolades :

Syntaxe :

```txt
{{/*
Ceci est un commentaire
*/}}
```

On ne peut pas utiliser ```//``` !!!

**NB** : on peut utiliser des tableaux bien plus complexe et itérer dessus.

### and

La fonction ```and``` prend deux arguments et vérifie s'ils sont "true".

Nous utiliserons une structure de données : un slice de structures. Ces structures seront de la forme :

```go
struct {
    Nom   string
    MDP   string
    Admin bool
}
```

Tout se passe au niveau du template. Nous allons itérer avec ```range``` puis utiliser ```if``` et ```and``` pour vérifier si notre utilisateur a un nom et est administrateur :

```txt
{{range .}}{{if and .Nom .Admin}}
    {{.Nom}} est un admin du système
{{end}}{{end}}
```

Ici nous avons condensé le code pour éviter d'avoir trop de sauts de ligne en sortie.

En résultat seuls deux résultats sont affichés : seuls ceux qui ont un nom d'utilisateur **ET** un mot de passe.

Code [Go](code13/main.go) / [Template](code13/fichier.txt)

### comparaisons

La syntaxe pour la comparaison est identique à celle de Bash par exemple :

```txt
{{eq . valeurATester}}
```

Ici on cherche à savoir si la valeur courante est égale ```eq``` à la valeur à tester.

On dispose des fonctions prédéfinies suivantes :

- ```eq``` ou ```ne```: égalité ou différent
- ```lt``` ou ```gt``` : plus petit/grand que...
- ```le``` ou ```lt``` : plus petit/grand ou égal que ...

Code [Go](code14/main.go) / [Template](code14/fichier.txt)

## 8-Modularisation des templates

Il est très utile de pouvoir découper les templates en sous éléments pour faciliter la maintenance du code.

On crée un premier template qui sera un élément de la structure globale qui sera le template qui appellera le sous-template.

Le sous-template se définit comme suit :

```txt
{{define "NomSousTemplate"}}
Insertion des éléments à définir
{{end}}
```

Le template "racine" appelant le sous-template :

```txt
{{template "NomSousTemplate"}}
```

Le nom d'un sous-template est indépendant de son nom de fichier. Le sous-template sera appelé selon son nom défini avec ```define``` et non via son nom de fichier. Cela signifie que dans un seul fichier on pourra définir autant de templates que l'on souhaite.

Côté code Go :

Comment gérer les sous-templates ? il y a à parser tous les fichiers template mais un seul à exécuter => le template racine :

```go
func main() {
    tpl, err := template.ParseGlob("*.txt")
    if err != nil {
        log.Fatalf("Erreur sur le parsing global : %v", err)
    }

    err = tpl.ExecuteTemplate(os.Stdout, "fichier.txt", nil)
    if err != nil {
        log.Fatalf("Erreur sur le traitement des templates : %v", err)
    }
}
```

Maintenant comment transmettre des données ?

Si pour le template racine, cela ne change rien à ce que nous avons déjà vu. Pour le sous-template on passera la valeur courante comme suit :

- Template racine :

```txt
{{template "NomSousTemplate" .}}
```

- Sous-template :

```txt
{{define "NomSousTemplate"}}
Contenu à insérer
Valeur transmise {{.}}
{{end}}
```

Le code Go **NE CHANGE PAS** !

Code [Go](code15/main.go) / [Template racine](code15/fichier.txt) / [Sous-template](code15/sous-fichier.txt)

## 9-Méthodes et templates

Rappel : Une méthode en Go est une fonction associée à un type particulier.

Exemple :

```go
type personne struct{
    Nom string
    Age int
}

func (p personne) vieillissementAccelere() int {
    // code quelconque : en revanche on utilise un
    // pointeur pour modifier une valeur directement
    return p.Age * 2
}

func main() {
    pers := personne{
        Nom: "John Doe",
        Age: 42,
    }

    fmt.Println(pers)

    fmt.Println(pers.vieillissementAccelere())
}
```

Comment utiliser cette méthode dans un template ?

Très simplement : ```.VieillissementAccelere``` :

```txt
{{.Nom}} a {{.Age}} ans.
Puis on lui fait un vieillissement accéléré :
Il a, maintenant, {{.VieillissementAccelere}} ans.
```

Et on peut utiliser un pipeline de méthodes.

Nous n'avons pas utilisé de **pointeur de type** pour nos fonctions. Si on modifie notre méthode :

```go
func (p *personne) VieillissementAccelere() {
    p.Age = p.Age * 2
}
```

Et que l'on ne change **rien** à notre template. Le résultat sera une erreur :

```txt
Erreur sur le remplissage : template: fichier.txt:3:3: executing "fichier.txt" at <.VieillissementAccelere>: can't evaluate field VieillissementAccelere in type main.personne
```

Autre élément très important : la visibilité de la méthode. Il **faut que la méthode soit exportable** (rappel : la première lettre de la méthode doit être une majuscule).

L'utilisation des méthodes dans un template est restreinte : elles ne peuvent modifier la donnée de départ. Ce traitement ne sera possible que dans le code Go.

Code [Go](code16/main.go) / [Template](code16/fichier.txt)

## 10-package html/template

Ce package est une "surcouche" de text/template. Il comprend nombre de fonctions et méthodes particulières pour le traitement des données HTML notamment certaines permettent d'échapper des caractères pouvant poser des problèmes (e.g. injection de codes malicieux en JS, XSS).

text/template peut être problématique avec le HTML : cf. doc [officielle](https://golang.org/pkg/html/template/#hdr-Security_Model)

Code [Go](code17/main.go) / [Template](code17/fichier.html)

Résultats : [avec injection de code JS](code17/htmlAvecRisque.html) / [sans risque](code17/htmlSansRisque.html)