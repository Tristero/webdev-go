package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
)

func main() {
	fmt.Println("Go server v0.3")
	li, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatalf("Erreur listener : %v", err)
	}

	for {
		cnx, err := li.Accept()
		if err != nil {
			log.Fatalf("Erreur à la connexion : %v", err)
		}

		go gereCnx(cnx)
	}
}

func gereCnx(cnx net.Conn) {
	fmt.Println("Connexion avec un client")
	scanner := bufio.NewScanner(cnx)
	for scanner.Scan() {
		// line est un jeton/token
		line := scanner.Text()
		// affiche à la console le msg du client
		fmt.Println(line)

		// on retourne une réponse
		fmt.Fprintf(cnx, "Vous avez envoyé le message suivant : %v\n\n", line)
	}
	defer cnx.Close()
	// Ce message ne sera pas affiché :
	fmt.Println("Client parti")
}
