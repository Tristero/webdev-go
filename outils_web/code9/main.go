package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
)

type Auteur struct {
	Nom    string
	Prenom string
}
type Livre struct {
	Titre   string
	Auteur  Auteur
	ISBN    string
	NbPages int
}

const donnees = `{
	"titre": "obabakoak",
	"auteur": {
		"nom":"Atxaga",
		"prenom":"Bernardo",
	},
	"isbn": "9782267019155",
	"pages": 409
}
`

func main() {
	fmt.Println("Lancement serveur de JSON")

	http.HandleFunc("/", racine)
	http.HandleFunc("/marshal", marshaler)
	http.HandleFunc("/encode", encoder)

	http.Handle("favicon.ico", http.NotFoundHandler())
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func marshaler(rep http.ResponseWriter, req *http.Request) {
	rep.Header().Set("Content-Type", "application/json")

	slice, err := json.Marshal(&Livre{
		Titre:   "obabakoak",
		NbPages: 409,
		ISBN:    "9782267019155",
		Auteur: Auteur{
			Prenom: "Bernardo",
			Nom:    "Atxaga",
		},
	})

	if err != nil {
		log.Printf("Erreur dans l'encodage: %v\n", err)
		http.Error(rep, "Erreur dans l'encodage", http.StatusInternalServerError)
	}

	// Version longue :
	// fmt.Fprintln(rep, string(slice))
	// alternative plus concise
	rep.Write(slice)
}

func encoder(rep http.ResponseWriter, req *http.Request) {
	rep.Header().Set("Content-Type", "application/json")

	if json.NewEncoder(rep).Encode(&Livre{
		Titre:   "obabakoak",
		NbPages: 409,
		ISBN:    "9782267019155",
		Auteur: Auteur{
			Prenom: "Bernardo",
			Nom:    "Atxaga",
		},
	}) != nil {
		log.Println("Erreur à l'encodage avec Encode()")
		http.Error(rep, "Erreur avec Encode()", http.StatusInternalServerError)
	}
}

func racine(rep http.ResponseWriter, req *http.Request) {
	msg := `<p>
	<a href="/marshal">Marshal()</a><br/>
	<a href="/encode">Encode()</a>
	</p>`

	fmt.Fprintln(rep, msg)
}
