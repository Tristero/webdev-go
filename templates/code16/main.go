package main

import (
	"log"
	"os"
	"text/template"
)

type personne struct {
	Nom string
	Age int
}

func (p personne) VieillissementAccelere() int {
	return p.Age * 2
}

func main() {
	pers := personne{
		Nom: "John Doe",
		Age: 42,
	}

	/*
		fmt.Println(pers)
		pers.VieillissementAccelere()
		fmt.Println(pers)
	*/
	tpl, err := template.ParseFiles("fichier.txt")

	// Alternative : template.ParseFiles(...)
	if err != nil {
		log.Fatalf("Erreur sur le parsing : %v", err)
	}

	err = tpl.Execute(os.Stdout, pers)
	if err != nil {
		log.Fatalf("Erreur sur le remplissage : %v", err)
	}
}
