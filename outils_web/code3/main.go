package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	fmt.Println("Lancement serveur 1")
	http.HandleFunc("/", root)
	http.Handle("favicon.ico", http.NotFoundHandler())
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func root(rep http.ResponseWriter, req *http.Request) {
	ctx := req.Context()
	log.Println(ctx)
	fmt.Fprintln(rep, ctx)
}
