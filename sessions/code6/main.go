package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"

	"github.com/google/uuid"
	"golang.org/x/crypto/bcrypt"
)

type utilisateur struct {
	Username   string
	MotDePasse []byte
	Nom        string
	Prenom     string
	ID         string
	New        bool
}

// bases de données live : maps
// nom d'utilisateur ("username") -> utilisateur
var dbUsers = map[string]utilisateur{}

// ID session -> username
var dbIDs = map[string]string{}

func init() {

	mdp, err := bcrypt.GenerateFromPassword([]byte("root"), bcrypt.DefaultCost)
	if err != nil {
		log.Fatalf("Impossible de créer un mot de passe : %v\n", err)
	}
	user := utilisateur{
		MotDePasse: mdp,
		ID:         uuid.New().String(),
		Username:   "jdoe",
		Nom:        "Doe",
		Prenom:     "John",
	}
	// enregistrement d'un utilisateur
	dbUsers[user.Username] = user
}

func main() {
	fmt.Println("Lancement du serveur 6")

	http.HandleFunc("/", racine)
	http.HandleFunc("/inscription", inscription)
	http.HandleFunc("/login", login)
	http.HandleFunc("/compte", compte)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func racine(rep http.ResponseWriter, req *http.Request) {

	tpl, err := template.ParseFiles("racine.html")
	if err != nil {
		http.Error(rep, err.Error(), 500)
		log.Fatalf("Erreur sur la récupération du template : %v", err)
	}
	if tpl.Execute(rep, nil) != nil {
		http.Error(rep, err.Error(), 500)
		log.Fatalf("Erreur sur le traitement du template : %v", err)
	}
}
