package main

import (
	"context"
	"fmt"
	"log"
	"time"

	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
)

type Livre struct {
	Titre   string
	Auteur  Auteur
	Editeur string
	Pages   int
}

type Auteur struct {
	Nom    string
	Prenom string
}

func main() {

	// Création du client
	client, err := mongo.NewClient(options.Client().ApplyURI("mongodb://localhost:27017"))
	if err != nil {
		log.Fatalf("Erreur à la création du client : %v", err)
	}
	log.Println("Création du client réussie -> Connexion ?")

	// Connexion
	ctx, cancel := context.WithTimeout(context.Background(), 3*time.Second)
	defer cancel()
	if err := client.Connect(ctx); err != nil {
		log.Fatalf("Erreur à la connexion : %v", err)
	}
	log.Println("Connexion réussie -> Ping ?")

	if err := client.Ping(ctx, nil); err != nil {
		log.Fatalf("Erreur ping : %v", err)
	}
	log.Println("Ping réussi.")

	// Requête : insérer plusieurs documents
	// Pour plus de clarté nous avons extrait le code dans une fonction helper

	requete(ctx, client.Database("biblio").Collection("livres"))

	// déconnexion
	if err := client.Disconnect(ctx); err != nil {
		log.Fatalf("Erreur à la déconnexion : %v", err)
	}

	fmt.Println("Déconnexion")
}

func requete(ctx context.Context, coll *mongo.Collection) {
	// Création d'une liste de document :
	lst := []interface{}{
		Livre{
			Titre: "L'art du zazen",
			Auteur: Auteur{
				Prenom: "Pierre",
				Nom:    "Crépon",
			},
			Editeur: "Albin Michel",
			Pages:   166,
		},
		Livre{
			Titre: "Les Argonautiques",
			Auteur: Auteur{
				Prenom: "Apollonios",
				Nom:    "de Rhodes",
			},
			Editeur: "Les Belles Lettres",
			Pages:   359,
		},
	}

	// insertion du document
	rslt, err := coll.InsertMany(ctx, lst)
	if err != nil {
		log.Printf("Erreur dans l'insertion des documents : %v", err)
	} else {
		// on afficle l'ID automatiquement retourné à l'insertion
		fmt.Printf("Documents insérés : %v\n", rslt.InsertedIDs)
	}
}
