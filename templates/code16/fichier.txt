{{.Nom}} a {{.Age}} ans.
Puis on lui fait un vieillissement accéléré :

Il a, maintenant, {{.VieillissementAccelere}} ans. Mais en fait, {{.Nom}} n'a pas changé d'âge : il a toujours {{.Age}} ans.