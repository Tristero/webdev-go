package main

import (
	"fmt"
	"net/http"
	"strings"
)

func insert(rep http.ResponseWriter, req *http.Request) {

	// données
	data := []struct {
		prenom string
		nom    string
	}{
		{"John", "Doe"},
		{"Haskell", "Curry"},
		{"Pike", "Rob"},
		{"Grace", "Hopper"},
	}

	stmt, err := db.Prepare("insert into clients(nom, prenom) values (?, ?)")
	check(err, "sur les déclarations préparées")

	defer stmt.Close()

	var compteur int64
	var msg strings.Builder
	for _, elem := range data {
		r, err := stmt.Exec(elem.nom, elem.prenom)
		msg.WriteString("- " + elem.prenom + " " + elem.prenom + "\n")
		check(err, "sur l'insertion de données")
		i, err := r.RowsAffected()
		check(err, "sur le RowsAffected")
		compteur += i
	}

	fmt.Fprintf(rep, "%sNombre d'enregistrements réalisés : %d", msg.String(), compteur)

}
