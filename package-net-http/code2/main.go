package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
)

type server struct{}

func (s server) ServeHTTP(rep http.ResponseWriter, req *http.Request) {
	if err := req.ParseForm(); err != nil {
		log.Fatalf("Erreur dans le parsing du formulaire : %v", err)
	}

	// Traitement du template

	tpl, err := template.ParseFiles("template.html")
	if err != nil {
		log.Fatalf("Erreur sur le chargement du template : %v", err)
	}

	if err := tpl.Execute(rep, req.Form); err != nil {
		log.Fatalf("Erreur sur le traitement du template : %v", err)
	}

}

func main() {
	s := server{}
	fmt.Println("Lancement du serveur 2.")
	if err := http.ListenAndServe(":8080", s); err != nil {
		log.Fatalf("Erreur ListenAndServe : %v", err)
	}
}
