package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
)

var redirect bool

func main() {
	fmt.Println("Lancement du serveur 7")
	http.HandleFunc("/", racine)
	http.HandleFunc("/redirection", redirection)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func racine(rep http.ResponseWriter, req *http.Request) {
	var methode = req.Method
	fmt.Printf("Méthode HTTP de la requête : %v\n", methode)
	tpl, err := template.ParseFiles("template.html")
	if err != nil {
		http.Error(rep, "Erreur sur le template", http.StatusInternalServerError)
		log.Fatalf("Erreur sur le template : %v", err)
	}

	tpl.ExecuteTemplate(rep, "template.html", redirect)
	redirect = false
}

func redirection(rep http.ResponseWriter, req *http.Request) {
	fmt.Printf("Redirection (méthode du client : %v)\n", req.Method)
	redirect = true
	// 1ère possiblité la plus verbeuse :
	/*
		rep.Header().Set("Location", "/")
		rep.WriteHeader(http.StatusSeeOther)
	*/

	// Seconde possibilité : la plus concise
	http.Redirect(rep, req, "/", http.StatusMovedPermanently)
}
