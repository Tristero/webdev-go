package main

import (
	"fmt"
	"log"
	"net/http"
)

type server struct{}

func (s server) ServeHTTP(rep http.ResponseWriter, req *http.Request) {
	switch req.URL.Path {
	case "/chien":
		fmt.Fprintf(rep, "Page consacrée aux chiens")
	case "/chat":
		fmt.Fprintf(rep, "Page consacrée aux chats")
	default:
		fmt.Fprintf(rep, "Site pour les chiens : /chien et pour les chats : /chat")
	}
}

func main() {
	var s server
	fmt.Println("Serveur multiplex v1")
	if err := http.ListenAndServe(":8080", s); err != nil {
		log.Fatalf("Erreur ListenAndServe : %v", err)
	}
}
