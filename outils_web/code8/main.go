package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	fmt.Println("Premier serveur sécurisé")

	http.HandleFunc("/", root)

	http.Handle("favicon.ico", http.NotFoundHandler())
	log.Fatal(http.ListenAndServeTLS(":10443", "cert.pem", "key.pem", nil))
}

func root(rep http.ResponseWriter, req *http.Request) {
	log.Println("Connexion réussie !")
	fmt.Fprintln(rep, "<h1>Hello from a not so secured server !</h1>")
}
