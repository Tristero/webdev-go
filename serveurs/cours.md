# Créer son propre serveur web avec Go

## 1-Intro

Nous nous focaliserons sur la création de serveur web recevant et répondant à des requêtes HTTP. Nos serveurs devront aussi gérer des routes particulières.

Une requête se compose de 3 parties :

- une ligne de requête : une méthode, une requête-cible et la version HTTP
- des entêtes
- un message optionnel : body

Cette ligne de requête peut ressembler à : ```GET /chemin/vers/repertoire/contenant/fichier.html HTTP/1.O```

Une réponse se compose de 3 parties :

- une ligne de status : la version HTTP, le code status, une phrase
- des entêtes
- un message optionnel : body

La ligne de status peut ressembler à : ```HTTP/1.0 200 OK```

Les entêtes sont le type de contenu (e.g. ```text/html```, ```application/json```)

## 2-Connexion

Pour créer un serveur, la bibliothèque standard propose le package ```net``` et son sous-package ```net/http``` qui permet de créer non seulement des serveurs mais aussi clients HTTP.

Go gère le HTTP/2, les protocoles HTTP et HTTPS.

Notre premier serveur n'acceptera que des connexions.

Etapes à respecter :

- 1 : on écoute un port TCP particulier avec la fonction ```net.Listen()```. Cette fonction prend deux arguments de type ```string``` : le protocole et l'url ; et retourne un ```listener``` et une erreur
- 2 : on gère l'erreur
- 3 : puis on appelle ```defer``` pour fermer la connexion à l'arrêt du programme. Pour cela on appelle sur le listener sa fonction ```Close()```
- 4 : on crée une boucle infinie ```for```
- 5 : dans cette boucle on va gérer les connexions : on appelle la méthode ```Accept``` qui retourne deux valeurs. La première est une connexion de type ```net.Conn``` et une erreur
- 6 : on gère l'erreur
- 7 : on effectue des traitements
- 8 : on clot notre connexion (pas le listener)

Ce qui donne :

```go
package main

import (
    "log"
    "net"
)

func main() {
    li, err := net.Listen("tcp", ":9090")
    if err != nil {
        log.Fatalf("Erreur sur la création du listener : %v", err)
    }

    defer li.Close()

    for {
        cnx, err := li.Accept()
        if err != nil {
            log.Fatalf("Erreur sur la connexion : %v", err)
        }

        // Traitement des requêtes et des réponses

        cnx.Close()
    }
}
```

Nous allons ajouter du code : nous allons retourner un message sur notre connexion via ```io.WriteString``` qui prend un type implémentant ```Writer``` et une chaine de caractère. On a une alternative : ```fmt``` et ses fonctions démarrant par ```Fprint...()``` :

```go
func main() {
    li, err := net.Listen("tcp", ":9090")
    if err != nil {
        log.Fatalf("Erreur sur la création du listener : %v", err)
    }
    fmt.Println("Lancement du serveur")
    defer li.Close()

    for {
        cnx, err := li.Accept()
        if err != nil {
            log.Fatalf("Erreur sur la connexion : %v", err)
        }

        // Traitement des requêtes et des réponses
        io.WriteString(cnx, "Serveur Go 0.1\n")
        fmt.Fprintf(cnx, "lancement le %v\n", time.Now())

        cnx.Close()
    }
}
```

Pour tester le code : on lance le serveur ```go run main.go``` puis dans un autre terminal, on lancera telnet : ```telnet localhost 9090```

Ce qui se passera c'est que le serveur va envoyer deux messages au client telnet puis coupera la connexion.

Pour arrêter notre serveur : ```ctrl+C```

Résumé :

3 étapes à respecter :

1 - l'écoute : ```net.Accept()```
2 - accepter l'écoute : ```monlistener.Accept()```
3 - obtenir la connexion : retour de la méthode ```Accept()```

## 3-Lecture des connexions

Le code traitant les connexions n'est pas du tout pertinent. Nous allons rectifier cela en utilisant les goroutines : le serveur va pouvoir gérer plusieurs connexions. Notre goroutine aura un argument de type ```net.Conn``` qui est la connexion du client au serveur

Syntaxe de base :

```go
package main

import (
    "log"
    "net"
)

func main() {
    lis, err := net.Listen("tcp", "5000")
    if err != nil {
        log.Fatalf("Erreur sur le listener : %v", err)
    }

    for {
        cnx, err := lis.Accept()
        if err != nil {
            log.Fatalf("Erreur sur l'acceptation : %v", err)
        }

        go gereCnx(cnx)
    }
}

// Fonction de gestion des connexions 👍
func gereCnx(cnx net.Conn) {}
```

Que doit-on gérer ?

Nous allons afficher les éléments reçus du client avec le package ```bufio``` et les scanners. On crée un scanner avec ```bufio.NewScanner()``` qui demande en paramètre un type implémentant ```io.Reader```. C'est le cas du type ```net.Conn```.

Puis on boucle tant que le scanner reçoit des données. On affichera le résultat.

Enfin on appelle ```defer``` sur la fermeture de la connexion.

### Aparté sur Scanner

Imaginons une chaine de caractère très longue ```s``` (cette chaine comportera les sauts de ligne ```\n```). Et on souhaite la traiter selon un flux de données. Nous allons utiliser ```bufio``` : nous devons créer un objet ```Scanner``` en lui passant non pas la chaîne directement mais en la transformant à l'aide de ```strings.NewReader(s)```. Cette fonction retourne un type ```Reader``` comme demander par ```NewScanner```.

On va traiter chaque "morceau" reçu par le scanner en une chaîne : via la méthode ```Text()``` et on affiche les "morceaux" :

```go
func main() {
    sc := bufio.NewScanner(strings.NewReader("Ceci est une chaîne très longue\nOu non enfin on ne sait plus trop\nSur ce qui est long ou pas."))
    for sc.Scan() {
        fmt.Println(sc.Text())
    }
}
```

On aura le résultat suivant à la console :

```txt
Ceci est une chaîne très longue
Ou non enfin on ne sait plus trop
Sur ce qui est long ou pas.
```

On peut aller plus loin : on dispose d'outils permettant de découper les valeurs selon ses objectifs. Pour cela le type ```Scanner``` dispose de ```Split()``` et requiert une fonction en argument d'un type très particulier ```bufio.SplitFunc```. Ces fonctions sont notamment données dans le package ```bufio``` : ```ScanWords```, ```ScanRunes```, ... ces fonctions débutent par ```Scan``` Voici un exemple où l'on découpe selon les mots et non les sauts de ligne :

```go
func main() {
    str := "Ceci est une chaîne très longue\nOu non enfin on ne sait plus trop\nSur ce qui est long ou pas."
    sc2 := bufio.NewScanner(strings.NewReader(str))
    sc2.Split(bufio.ScanWords)
    for sc2.Scan() {
        fmt.Println(sc2.Text())
    }
}
```

Bien entendu, on peut créer ses propres fonctions ```SplitFunc```.

### Code

```go
package main

import (
    "bufio"
    "fmt"
    "log"
    "net"
)

func main() {
    fmt.Println("Serveur Go v0.2")
    lis, err := net.Listen("tcp", ":8080")
    if err != nil {
        log.Fatalf("Erreur sur le listener : %v", err)
    }

    for {
        cnx, err := lis.Accept()
        if err != nil {
            log.Fatalf("Erreur sur l'acceptation : %v", err)
        }

        go gereCnx(cnx)
    }
}

// Fonction de gestion des connexions 👍
func gereCnx(cnx net.Conn) {
    fmt.Println("Une connexion entrante a été demandée")
    // scanne de la connexion entrante
    scanner := bufio.NewScanner(cnx)
    // boucle de lecture
    for scanner.Scan() {
        fmt.Println(scanner.Text())
    }
    defer cnx.Close()

    // Le code qui suit ne sera pas affiché tant que la connexion ne sera pas achevée :
    fmt.Println("Ce message ne s'affichera jamais !")
}
```

### Test

On lancera son navigateur Internet (s'il y a le moindre problème : il faut tester avec Firefox).

Qu'observe-t-on ? si on ouvre autant d'onglet, la console affichera autant de message de requête. Ce sont les données du protocole HTTP reçue par le serveur et envoyée par le client.

On pourra même tester des URLs : ```localhost:8080/route/vers/autre/chose```

**NB** : le code fourni [ici](code3/main.go) est proposé avec une petite amélioration : on donne un numéro à chaque appel de connexion (ouverture d'un onglet) et on verra quel client ouvre et ferme sa connexion

Code [Go](code3/main.go) / [Go et Scanner](code2/main.go)

## 4-Ecriture et lecture sur les connexions

On reprend ce que l'on a vu :

- on crée un listener, une connexion (dans une boucle infinie) et on lance une goroutine

```go
func main() {
    li, err := net.Listen("tcp", ":8080")
    if err != nil {
        log.Fatalf("Erreur listener : %v", err)
    }

    for {
        cnx, err := li.Accept()
        if err != nil {
            log.Fatalf("Erreur à la connexion : %v", err)
        }

        go gereCnx(cnx)
    }
}
```

- dans la fonction de gestion de la connexion : on retourne des messages avec ```bufio.Scanner``` qui va lire dans la connexion. On boucle : on récupère le texte transmis et on l'affiche sur la console
- ne pas oublier de fermer la connexion avec ```defer```

```go
func gereCnx(cnx net.Conn) {
    fmt.Println("Connexion avec un client")
    scanner := bufio.NewScanner(cnx)
    for scanner.Scan() {
        // line est jeton/token
        line := scanner.Text()
        // affiche à la console le msg du client
        fmt.Println(line)

        // on retourne une réponse
        fmt.Fprintf(cnx, "Vous avez envoyé le message suivant : %v\n\n", line)
    }
    defer cnx.Close()
    fmt.Println("Client parti")
}
```

[Code](code4/main.go)

Pour tester notre serveur nous utiliseron ```telnet localhost 8080``` et on pourra le texte que l'on souhaite.

On va améliorer notre code en gérant le temps : au bout de 10 secondes de communication, le serveur coupe la connexion. Pour cela la bibliothèque standard nous fournit une méthode ```SetDeadLine()``` prenant un type ```time.Time```.

```go
func gereCnx(cnx net.Conn) {
    fmt.Println("Nouveau client")

    if cnx.SetDeadline(time.Now().Add(10*time.Second)) != nil {
        log.Println("TIMEOUT DE LA CONNEXION")
    }
    ...
}
```

Pour tester : ```telnet localhost 8080```

On notera que celle la connexion avec le client se coupe : pas le serveur.

[Code](code5/main.go)

## 5-Création d'un client

### Lecture

Avec Go, il est possible de créer un client avec la bibliothèque standard.

Ici le client appelle un serveur avec une fonction ```net.Dial()``` qui prend en argument :

- le protocole de communication (string)
- l'adresse et le port (string)

Une forme identique que pour ```net.Listen()``` mais ```Dial()``` retourne un objet de type ```net.Conn``` et une erreur.

Une fois l'erreur gérée, on ```defer``` la fermeture de la connexion :

```go
func main() {
    // on doit bien préciser l'URL et le port
    cnx, err := net.Dial("tcp", "localhost:8080")
    if err != nil {
        log.Fatalf("Erreur sur l'appel : %v", err)
    }
    defer cnx.Close()
}
```

Pour lire ce que le serveur veut nous envoyer, le package ```ioutil``` propose la fonction ```ReadAll()``` qui demande un type implémentant ```io.Reader``` et retourne un slice d'octet et une erreur :

```go
func main() {
    ... // Code inchangé

    tab, err := ioutil.ReadAll(cnx)
    if err != nil {
        log.Fatalf("Erreur sur la lecture des données : %v", err)
    }

    fmt.Println(string(tab))
}
```

Notre serveur sera très simple : pas de goroutine, il gèrera seulement une connexion à la fois et enverra trois messages (de trois façons différentes) avant la fermeture de la connexion.

Codes : [serveur](code6/server.go) / [client](code6/client.go)

### Ecriture

Notre serveur devra lire les connexions entrantes et les afficher au prompt.

Notre client utilisera toujours la fonction ```Dial()``` pour créer notre connexion et nous utiliserons cet objet pour envoyer les données vers le serveur à l'aide ```fmt.Fprintf()``` :

```go
func main() {
    fmt.Println("Client v0.2")

    cnx, err := net.Dial("tcp", "localhost:8080")
    if err != nil {
        log.Fatalf("Erreur connexion : %v", err)
    }

    defer cnx.Close()

    fmt.Println("Envoi d'un message au serveur")
    fmt.Fprintf(cnx, "Appel du client vers le serveur...")
}
```

## 6-Exemple de serveur plus poussé

On a créé un serveur qui lit et retourne des données à un client. Ce serveur implémente le chiffre de César (ou algorithme rot13) [code](code8/serveur.go). On utilisera telnet pour échanger avec le serveur : ```telnet localhost 8080```

## 7-Serveur TCP : requêtes et réponses HTTP

Jusqu'ici nos messages n'étaient pas du HTML mais nous pouvons avec ce que nous avons vu renvoyer du HTML.

Maintenant nous séparons dans deux fonctions différentes : une pour traiter la requête et une pour retourner une réponse.

### traiter la requête

nous allons créer un compteur. Ce compteur va nous servir à mettre en avant la méthode utilisée par le client (c'est le verbe HTTP : GET, POST,...). Comme nous voulons récupérer le nom de la méthode utilisée par le client, celle-ci se trouve dans le premier élément envoyé par le client. Donc on utilisera ```strings.Fields``` pour extraire de la chaîne la méthode (ou verbe HTTP) demandée par le client (ici ce seron GET). Cette fonction ```strings.Fields``` décompose une chaîne à partir des espaces et chaque mot devient un "champ" indépendant :

Exemple : ```"GET / HTTP/1.1"``` devient ```"GET", "/", "HTTP/1.1"```.

Et lorsque la ligne scannée est vide alors la boucle est cassée et on incrémente de 1 notre compteur. D'après les RFC, après les lignes de requête et d'entêtes on trouve une ligne vide donc nous n'avons plus besoin de scanner les entrée.

Code :

```go
func requete(cnx net.Conn) {
    i := 0
    scn := bufio.NewScanner(cnx)
    for scn.Scan() {
        ln := scn.Text()
        fmt.Println(ln)
        // Extraction du nom de la méthode requise par le client
        if i == 0 {
            fmt.Printf("\t**METHODE : %v**\n", strings.Fields(ln)[0])
        }
        // si ligne vide : on casse la boucle
        if ln == "" {
            break
        }
        // incrémente le compteur
        i++
    }
}
```

### Réponse retournée

Pour notre réponse nous allons procéder en deux étapes :

- 1 : envoyer du contenu HTTP
- 2 : retourner du contenu HTML

Pour retourner nos contenus, nous utiliserons une seule méthode  : ```fmt.Fprintf()```.

Pour les entêtes HTTP :

- on commence par spécifier le protocole HTTP avec sa version, le code status de la requête et le message (suivi d'un retour et d'un saut : "\r\n")
- puis on retourne un ```Content-Length``` suivi de la longueur de la chaîne retournée (accompagné de la suite de caractères spéciaux de retour à la ligne "\r\n")
- puis le type du contenu (```Content-Type```) soit ```text/html``` (toujours suivi de "\r\n")
- un passe une ligne vide avec "\r\n"
- et on termine avec le contenu HTML (sans les caractères finaux spéciaux)

Code :

```go
func reponse(cnx net.Conn) {
    // Contenu HTML
    body := `<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <title>Document</title>
</head>
<body>
    <p>Hello, world</p>
</body>
</html>`

    // Entêtes HTTP
    fmt.Fprintf(cnx, "HTTP/1.1 200 OK\r\n")
    fmt.Fprintf(cnx, "Content-Length: %d\r\n", len(body))
    fmt.Fprintf(cnx, "Content-Type: text/html\r\n")
    fmt.Fprintf(cnx, "\r\n")
    fmt.Fprintf(cnx, body)
}
```