package main

import (
	"fmt"
	"io"
	"log"
	"net"
	"time"
)

func main() {
	fmt.Println("Go server v0.1")

	lis, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatalf("Erreur listener : %v", err)
	}

	for {
		cnx, err := lis.Accept()
		if err != nil {
			log.Fatalf("Erreur sur la connexion: %v", err)
		}

		dt := time.Now()
		io.WriteString(cnx, "\nServeur Go TCP\n")
		fmt.Fprintf(cnx, "Nous sommes le %v %v %v\n", dt.Day(), dt.Month(), dt.Year())
		fmt.Fprintln(cnx, "Excellente journée.")

		cnx.Close()
	}
}
