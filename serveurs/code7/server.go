package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
)

func main() {
	lis, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatalf("Erreur listener : %v", err)
	}

	for {
		cnx, err := lis.Accept()
		if err != nil {
			log.Fatalf("Erreur connexion : %v", err)
		}

		go gereCnx(cnx)
	}
}

func gereCnx(cnx net.Conn) {

	//scanner l'entrée
	scanner := bufio.NewScanner(cnx)
	for scanner.Scan() {
		token := scanner.Text()
		fmt.Printf("Message reçu : %s", token)
	}

	defer cnx.Close()

}
