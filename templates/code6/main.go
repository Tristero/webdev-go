package main

import (
	"log"
	"os"
	"text/template"
)

var slice = []string{"Monteverdi", "Mozart", "Beethoven", "Schubert", "Striggio"}

func main() {
	tpl, err := template.ParseFiles("fichier.txt")
	if err != nil {
		log.Fatalf("Erreur sur la récupération du template : %err")
	}

	if tpl.Execute(os.Stdout, slice) != nil {
		log.Fatalf("Erreur sur l'exécution.")
	}
}
