package main

import (
	"log"
	"net/http"
)

func main() {

	log.Println("Lancement de serveur de fichier\nAttention à bien vider le cache")

	http.ListenAndServe(
		":8080",
		http.FileServer(
			http.Dir("."),
		),
	)
}
