package main

import (
	"log"
	"os"
	"strings"
	"text/template"
)

// Fonction pour le templating
func justeTrois(s string) string {
	s = strings.TrimSpace(s)
	return strings.ToUpper(s[:3])
}

// Enregistrement des fonctions:
var fonctions = template.FuncMap{
	"upc": strings.ToUpper,
	"jt":  justeTrois,
}

// Structure de données

var tabPays = []struct {
	Pays     string
	Capitale string
}{
	{
		Pays:     "Espagne",
		Capitale: "Madrid",
	},
	{
		Pays:     "Chine",
		Capitale: "Beijing",
	},
	{
		Pays:     "Italie",
		Capitale: "Rome",
	},
	{
		Pays:     "Danemark",
		Capitale: "Copenhague",
	},
}

func main() {

	//tpl, err := template.New("").Funcs(fonctions).ParseFiles("fichier.txt")
	tpl, err := template.New("").Funcs(fonctions).ParseFiles("fichier.txt")
	if err != nil {
		log.Fatalf("erreur sur le parsing : %v", err)
	}

	err = tpl.ExecuteTemplate(os.Stdout, "fichier.txt", tabPays)
	//err = tpl.Execute(os.Stdout, tabPays)
	if err != nil {
		log.Fatalf("Ouh erreur : %v", err)
	}
}
