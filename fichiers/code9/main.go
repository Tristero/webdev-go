package main

import (
	"fmt"
	"log"
	"net/http"
)

func hello(rep http.ResponseWriter, req *http.Request) {
	fmt.Fprintln(rep, "Hello gophers !")
}

func main() {
	fmt.Println("Lancement serveur 10")
	http.Handle("/ressources", http.NotFoundHandler())
	http.HandleFunc("/ressources/hello/", hello)
	log.Fatal(http.ListenAndServe(":8080", nil))
}
