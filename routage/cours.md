# Routage avec Go

On parle de routage ou de multiplexage lorsque l'on veut gérer les URLs et répondre spécifiquement à celles-ci.

## 1- Premier pas

Le pointeur ```*http.Request``` dispose d'un champ ```URL``` qui est une structure. Et cette structure dispose d'un champ ```Path``` que l'on va utiliser explicitement conjoitnement avec un ```switch``` :

```go
func (s server) ServeHTTP(rep http.ResponseWriter, req *http.Request) {
    switch req.URL.Path {
    case "/chien":
        fmt.Fprintf(rep, "Page consacrée aux chiens")
    case "/chat":
        fmt.Fprintf(rep, "Page consacrée aux chats")
    default:
        fmt.Fprintf(rep, "Site pour les chiens : /chien et pour les chats : /chat")
    }
}
```

Code [Go](code1/main.go)

### Première amélioration

Encore une fois, la bibliothèque standard vient à notre secours pour simplifier le travail.

Au lieu d'utiliser directement ```ListenAndServe```, nous utiliserons ```NewServeMux()``` qui retourne un objet serveur gérant le routage. Sur ce nouvel objet nous passerons une méthode ```Handle``` qui prend deux arguments :

- une URL sous forme de chaîne : c'est un pattern
- un type ```http.Handler```

Concernant le pattern ou motif : le comportement "/chien" et "/chien/" ne sera **pas** le même. Si on utilise "/chien/", le code Go fonctionnera si on passe au navigateur une URL plus longue : "/chien/et/autre/chose". En revanche, avec le motif "/chien", on aura une erreur 404 ! Donc attention au motif !

Le type ```http.Handler``` sera notre variable initialisée de type ```chien``` ou ```chat```.

Puis on appellera ```ListenAndServe``` en passant en second paramètre le serveur multiplex.

Le problème c'est que nous devons créer deux types pour les deux appels de ```Handle```. On a encore un peut trop de boilerplate code.

```go
func main() {
    var chien chien
    var chat chat
    mux := http.NewServeMux()
    fmt.Println("Lancement serveur multiplex v2")
    mux.Handle("/chien", chien)
    mux.Handle("/chat", chat)

    if err := http.ListenAndServe(":8080", mux); err != nil {
        log.Fatalf("Erreur ListenAndServe : %v", err)
    }
}
```

Nous sommes obligés de créer deux types ```struct``` pour chien et chat, nous devons aussi implémenter deux fois ```ServeHTTP```. Donc si nous avons beaucoup de routes à gérer, ce code n'est pas très élégant :

```go
type chien struct{}
type chat struct{}

func (c chien) ServeHTTP(rep http.ResponseWriter, req *http.Request) {
    fmt.Fprintf(rep, "Appel de la page consacré aux chiens")
}

func (c chat) ServeHTTP(rep http.ResponseWriter, req *http.Request) {
    fmt.Fprintf(rep, "Appel de la page consacré aux chats")

}
```

Tests : pour les URLs on pourra tester : "/chien", "/chien/autre/", "chat", "chat/test", ...

Code [Go](code2/main.go)

## 2-Seconde amélioration

La bibliothèque standard nous une écriture plus rapide sans avoir à créer un nouvel objet ```NewServeMux```. Pour cela la bibliothèque standard fournit une *fonction* ```Handle``` se comportant exactement comme la *méthode* ```Handle``` de ```NewServerMux``` : la différence c'est que la **fonction** ```Handle``` crée pour nous un serveur par défaut. Mais quid de ```ListenAndServe``` ? au lieu de passer en second paramètre un objet, on passe simplement ```nil``` :

```go
func main() {
    var chien Chien
    var chat Chat
    fmt.Println("Lancement serveur multiplex v3")

    http.Handle("/chien", chien)
    http.Handle("/chat", chat)

    if err := http.ListenAndServe(":8080", nil); err != nil {
        log.Fatalf("Erreur ListenAndServe :%v"; err)
    }
}
```

Code [Go](code3/main.go)

## 3-Troisième amélioration

L'écriture peut encore être condensée avec ```HandleFunc()``` : cette fonction prend motif en paramètre et une fonction de type ```func(ResponseWriter, *Request)``` !

Avec cette nouvelle façon de procéder on se libère de la création d'un type avec sa valeur zéro, on n'a plus besoin de créer des implémentations ```ServeHTTP``` :

```go
func main() {

    fmt.Println("Lancement du serveur multiplex v4")

    http.HandleFunc("/chien/", func(rep http.ResponseWriter, req *http.Request) {
        fmt.Fprintf(rep, "Page consacrée aux chiens")
    })

    http.HandleFunc("/chat", func(rep http.ResponseWriter, req *http.Request) {
        fmt.Fprintf(rep, "Page consacrée aux chats")
    })

    if err := http.ListenAndServe(":8080", nil); err != nil {
        log.Fatalf("Erreur ListenAndServe : %v", err)
    }
}
```

Code [Go](code4/main.go)

## 4-Bibliothèque tiers : httprouter

C'est une des nombreuses bibliothèques disponibles pour gérer différemment les routes : [URL](https://pkg.go.dev/github.com/julienschmidt/httprouter?tab=doc). C'est aussi l'une des plus populaires.

Cette bibliothèque permet d'affiner les motifs pour le routage, d'améliorer les performances du serveur. Il faudra importer la bibliothèque ```github.com/julienschmidt/httprouter```

Dans un premier temps on crée un objet avec ```New()``` : ```httprouter.New()```. A cet objet est associé des méthodes portant les mêmes noms que les méthodes HTTP : ```GET```, ```POST```...

Ces méthodes prennent deux paramètres :

- un motif
- une fonction de type ```func(ResponseWriter, *Request, httprouter.Params)```. Le dernier argument est facultatif et permet de gérer les paramètres transmis.

```go
func index(rep http.ResponseWriter, req *http.Request, _ httprouter.Params) {
    fmt.Fprintln(rep, "Hello world")
}

func main() {
    fmt.Println("Lancement du serveur multiplex v5")
    mux := httprouter.New()

    mux.GET("/", index)
    log.Fatal(http.ListenAndServe(":8080", mux))
}
```

Code [Go](code5/maing.go)

Il est possible de gérer les paramètres passés via les URLs et ```httprouter``` gère deux types de pattern :

- ```:nomDeParametre``` : récupère le paramètre nommé
- ```*nomDeParametre``` : récupère tous les paramètres

### Exemples pour ```:nomDeParametre```

Pattern : ```/blog/:categorie```

Si on tape ```blog/go```, on aura ```categorie="go"```.

Si on tape ```blog/go/```, on aura une redirection mais le matching échouera.

Si on tape ```blog/html```, on aura ```categorie="html"```.

Si on tape ```blog/html/truc```, aucun matching n'aura lieu.

Exemple :

```go
func user(rep http.ResponseWriter, req *http.Request, params httprouter.Params) {
    prenom := params.ByName("prenom")
    nom := params.ByName("nom")
    fmt.Fprintf(rep, "Hello, %s %s", prenom, nom)
}

func main() {
    fmt.Println("Serveur httprouter v2")

    mux := httprouter.New()
    mux.GET("/:prenom/:nom", user)

    log.Fatalln(http.ListenAndServe(":8080", mux))
}
```

On testera dans un navigateur : ```/john/doe```, ```/john``` et on observera s'il y a des erreurs.

Code [Go](code6/main.go)

### Exemples pour ```*nomDeParametre```

Pattern : ```/blog/*fichiers```

On pourra récupérer le fichier :

- racine avec ```/blog/```
- un fichier nommé par ex. "Licence" : ```/blog/Licence```
- un fichier dans un sous-répertoire : ```/blog/sous/repertoire/fichier.html```

On aura une redirection si on tape ```/blog```

Cette bibliothèque est bien moins permissive sur la gestion des URLs que la bibliothèque standard !
