package main

import (
	"fmt"
	"log"
	"net/http"
	"os"
)

func main() {
	fmt.Println("Serveur v2")
	http.HandleFunc("/", img)
	http.ListenAndServe(":8080", nil)
}

func img(rep http.ResponseWriter, req *http.Request) {
	fichier, err := os.Open("../images/img.png")
	if err != nil {
		http.Error(rep, "Erreur sur le fichier", 404)
		log.Fatalf("Fichier inconnu : %v", err)
	}

	defer fichier.Close()

	fs, err := fichier.Stat()
	if err != nil {
		http.Error(rep, "Erreur sur la récupération des infos du fichier", 404)
		log.Fatalf("Erreur sur la récup d'info du fichier %v", err)
	}

	http.ServeContent(
		rep,
		req,
		fs.Name(),
		fs.ModTime(),
		fichier,
	)

	fmt.Printf("Fichier demandé avec le pointeur os.File: %v\nFichier demandé avec fs.Name() : %v\nDate de modif : %v\n", fichier.Name(), fs.Name(), fs.ModTime())
}
