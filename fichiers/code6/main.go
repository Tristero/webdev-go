package main

import (
	"fmt"
	"net/http"
)

func main() {
	fmt.Println("Serveur de fichier v6")

	http.HandleFunc("/", root)
	http.Handle("/ressources/",
		http.StripPrefix("/ressources/",
			http.FileServer(http.Dir("../images/"))),
	)

	http.ListenAndServe(":8080", nil)
}

func root(rep http.ResponseWriter, req *http.Request) {
	rep.Header().Set("Content-type", "text/html; charset=utf-8")
	fmt.Fprintln(
		rep,
		`<img src="/ressources/img2.png">`,
	)
}
