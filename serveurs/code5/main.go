package main

import (
	"bufio"
	"fmt"
	"log"
	"net"
	"time"
)

func main() {
	fmt.Println("Go server v0.4")
	li, err := net.Listen("tcp", ":8080")
	if err != nil {
		log.Fatalf("Erreur listener : %v", err)
	}

	for {
		cnx, err := li.Accept()
		if err != nil {
			log.Fatalf("Erreur connexion : % v", err)
		}

		go gereCnx(cnx)
	}
}

func gereCnx(cnx net.Conn) {
	fmt.Println("Nouveau client")

	if cnx.SetDeadline(time.Now().Add(10*time.Second)) != nil {
		log.Println("TIMEOUT DE LA CONNEXION")
	}

	scanner := bufio.NewScanner(cnx)

	for scanner.Scan() {
		token := scanner.Text()
		fmt.Println(token)
		// réponse
		fmt.Fprintf(cnx, "Message reçu : %v\n", token)
	}
	defer cnx.Close()

	fmt.Println("Fin de connexion")
}
