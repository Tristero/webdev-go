package main

import (
	"bufio"
	"fmt"
	"strings"
)

func main() {

	str := "Ceci est une chaîne très longue\nOu non enfin on ne sait plus trop\nSur ce qui est long ou pas."
	sc := bufio.NewScanner(strings.NewReader(str))
	for sc.Scan() {
		ln := sc.Text()
		fmt.Println(ln)
	}

	sc2 := bufio.NewScanner(strings.NewReader(str))
	sc2.Split(bufio.ScanWords)
	for sc2.Scan() {
		fmt.Println(sc2.Text())
	}
}
