# Le package net/http

## 1-Introduction

Jusqu'ici nous avons employé le package ```net```, mais nous dispons d'outils standard évitant tous les codes répétitifs ou *boilerplate code* dans le sous-package ```net/http```. Ce package nous fournit des implémentations client et serveur du protocole HTTP mais aussi HTTPS !

Le "point d'entrée" ou le point central du package ```net/http``` est l'interface ```Handler``` :

```go
type Handler interface {
    ServeHTTP(ResponseWriter, *Request)
}
```

On attachera cette implémentation sur n'importe quel : un entier, une structure, ... :

```go
type truc int

func (t truc) ServeHTTP(w http.ResponseWriter, r *http.Request){
    // code
}
```

Cette interface demande que soit implémenter une fonction prenant un argument pour une réponse et un argument pour la requête. C'est la fonction de base pour créer un serveur et son objectif est de répondre à une requête HTTP uniquement. Nous verrons dans la suite les types de ces arguments.

Pour gérer les connexions nous abandonnerons ```Listen``` car on dispose d'un outil permettant de raccourcir le développement avec ```ListenAndServe()```. Cette fonction prend deux arguments : une adresse sous la forme d'une chaîne de caractère et un handler de type ... ```Handler```. Et elle retourne une erreur.

```Request``` est une structure avec un grand nombre de champs dont : une methode (```string```), une URL (aussi une structure ```url.URL```), des entêtes de type ```Header``` (un type map avec, en clé un type ```string``` et un valeur un slice de ```string```s), un Body de type ```io.ReadCloser```,... Rien de bien compliqué : il s'agit d'une composition de types. Ce type sera exploitable dans des objets implémentant ```Handler```.

```ResponseWriter``` est une interface demandant l'implémentation de 3 méthodes dont les signatures sont :

- ```Header() Header```
- ```Write([byte]) (int, error)``` : c'est le ```type Writer```
- ```WriteHeader(int)```

## 2-ListenAndServe

L'implémentation de ```ServeHTTP()``` va être utilisable par la fonction ```ListenAndServe()``` :

- notre implémentation ```ServeHTTP``` ne traitera pas la requête reçue mais enverra une réponse dans le ```ResponseWriter```.
- dans la fonction ```main```, on initialisera une variable avec le type implémentant ```ServeHTTP``` et on passera cette variable à ```ListenAndServe()``` :

```go
type serve struct{}

func (s serve) ServeHTTP(rep http.ResponseWriter, req *http.Request) {
    fmt.Fprintln(rep, "Réponse en retour.")
}

func main() {
    var s serve
    http.ListenAndServe(":8080", s)
}
```

Que fais ```ListenAndServe``` ? elle gère les connexions entrantes. Les connexions sont acceptées et gérées pour nous (selon le principe vu lors de la création des serveurs par nos soins).

Dans notre code nous avons utilisé un type ```struct``` mais cela peut être n'importe quoi : l'avantage d'une structure vide c'est qu'elle ne prend aucun espace en mémoire.

Test : on lance notre code et on se connecte avec notre navigateur à l'adresse [http://localhost:8080](http://localhost:8080)

Si tout s'est bien déroulé on peut lire notre message passé en réponse.

ATTENTION : ```ListenAndServe()``` retourne une erreur qu'il faudra gérer.

Code [Go](code1/main.go)

Le type ```Request``` est une structure servie par ```ListenAndServe()``` et à cette structure sont associées tout un ensemble de méthodes permettant de traiter les requêtes reçues du client.

Le type ```ResponseWriter``` procède de la même manière mais pour traiter comment la réponse doit être rendue.

## 3-Récupération des données depuis le client

Objet : traitement des données via le type ```*net.Response```

Ainsi nous pouvons traiter des requêtes issus de formulaires comme celui-ci :

```html
<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Formulaire et Go</title>
</head>
<body>
    <!--S'il y a des données le code ci-dessous est activé-->
    {{if .}}
    <main>
        <p><strong>Noms de variable</strong> (identificateurs) et <em>valeurs : </em></p>
        {{range $cle, $val := .}}
            <p><strong>{{$cle}}</strong></p>
            <ul>
                {{range $val}}
                    <li>
                        <em>{{.}}</em>
                    </li>
                {{end}}
            </ul>
        {{end}}
    </main>
    {{end}}

    <form action="/" method="POST">
        <input type="text" name="varNom" placeholder="prenom" autofocus autocomplete="off">
        <input type="submit" name="btn-submit" value="texte du bouton">
    </form>
</body>
</html>
```

Le code HTML est un template la partie ```<main>``` ne sera exécutée que si elle reçoit des données dans le cas contraire, elle n'est pas traitée par notre moteur de gestion des templates.

Notre serveur va récupérer les données avec une méthode spéciale de ```Request``` : ```ParseForm()``` qui ne prend aucun argument mais retourne une erreur. L'objet ```Request``` contiendra alors l'ensemble des valeurs transmises dans un type ```Values``` (que l'on appellera comme suit : ```req.Form``` ou  ```req.**PostForm*``` si l'on souhaite que récupérer les éléments d'un formulaire POST) qui n'est qu'une map : avec des clés de type ```string``` et des valeurs stockées sous forme d'un slice de chaînes.

Pour traiter notre template on utilisera l'une des deux techniques de traitement des templates que nous avons vu.

Nous utiliserons la version la plus simple où l'on traitera directement notre template dans notre méthode implémentant ```ServeHTTP```

```go
func (s server) ServeHTTP(rep http.ResponseWriter, req *http.Request) {
    if err := req.ParseForm(); err != nil {
        log.Fatalf("Erreur dans le parsing du formulaire : %v", err)
    }

    // Traitement du template

    tpl, err := template.ParseFiles("template.html")
    if err != nil {
        log.Fatalf("Erreur sur le chargement du template : %v", err)
    }

    if err := tpl.Execute(rep, req.Form); err != nil {
        log.Fatalf("Erreur sur le traitement du template : %v", err)
    }
}
```

Résultat : on démarrage on aura seulement notre formulaire car aucune donnée n'a été transmise à notre template. Ce n'est qu'à la soumission de notre formulaire que les données vont pouvoir être à nouveau traiter par notre moteur de traitement des templates. Avec le résultat suivant :

```txt
btn-submit

   . texte du bouton

fnom

   . john

```

Si on rajoute un élément de type : ```<form action="/?fnom=bidule">```, le traitement nous retournera deux valeurs pour le champ ```fnom``` : "bidule" et la chaîne que l'on aura passé au formulaire. Bien entendu on peut utiliser n'importe quel nom pour les paramètres de requête : ```/?var=autre``` et dans ce cas, le résultat sera encore différents. Nous aurons trois champs au lieu de deux.

Le type ```Request``` dispose des champs ```Form``` et ```PostForm```. La différence est que le premier permet de récupérer les requêtes passées par l'URL (paramètres de requêtes) ; alors que le second ne va récupérer **uniquement** que les formulaires POST.

Code [Go](code2/main.go) / [Template](code2/template.html)

```Request``` contient des champs très intéressants :

- ```Body``` : qui stocke des contenus (type ```io.ReadCloser```)
- ```RemoteAddr``` : l'adresse du client
- ```Host``` : les informations de l'hôte
- ```Method``` : la méthode HTTP demandée par le client

Dans cet autre exemple on va récupérer la méthode requise par le client et les données du formulaire. Le tout sera stocké dans une structure :

```go
donnees := struct {
    Methode    string
    URL        *url.URL
    Formulaire url.Values
    Adresse    string
    Headers    http.Header
}{
    Methode:    req.Method,
    URL:        req.URL,
    Formulaire: req.PostForm,
    Adresse:    req.RemoteAddr,
    Headers:    req.Header,
}
```

On pourra tester une URL différente et observer le résultat et la méthode demandée... On pourra modifier le code pour étudier les headers qui sont de type ```Headers``` qui une map qu'il faudra décomposer.

Le code Go diffère dans son implémentation : nous avons utilisé l'implémentation avec ```init()``` et un pointeur global de type ```template.Template```.

Le template sera nettement plus complexe : il faudra donc bien faire attention à bien gérer les boucles et autre appel de fonction.

Code [Go](code3/main.go) / [Template](code3/template.html)

Pour parser le contenu d'un formulaire, la bibliothèque standard Go propose une fonction-hepler : ```FormValue(clé)``` la clé est une chaîne et retourne une chaîne.

## 4-Traiter les réponses

Objet : exploration de ```http.ResponseWriter```.

Le type ```ResponseWriter``` est une interface demandant 3 méthodes :

- ```Header() Header```
- ```Write([]byte) (int, error)```
- ```WriteHeader(int)```

Manipulons la première méthode en injectant des éléments et pour cela nous devons utiliser la méthode ```Set```, comme suit, car ```Header()``` retourne un objet :

```go
rep.Header().Set(cle, valeur)
```

où la ```cle``` et la ```valeur``` sont des chaînes.

Code :

```go
func (s server) ServeHTTP(rep http.ResponseWriter, req *http.Request) {
    rep.Header().Set("Message", "Ceci est une création inutile")
    rep.Header().Set("Content-Type", "text/html; charset=utf-8")
    fmt.Fprintln(rep, "<h1>Code HTML</h1>")
}
```

On testera notre code : ```curl localhost:8080 -v```

On vérifiera que notre message inutile sera bien inscrit dans les entêtes.

Code [Go](code4/main.go)
