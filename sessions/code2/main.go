package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"

	"github.com/google/uuid"
)

type utilisateur struct {
	Username string
	Nom      string
	Prenom   string
	ID       string
	New      bool
}

// bases de données live : maps
var dbUsers = map[string]utilisateur{}
var dbIDs = map[string]string{} // ID session / ID utilisateur

func main() {
	fmt.Println("Lancement du serveur 2")

	http.HandleFunc("/", racine)
	http.HandleFunc("/compte", connexion)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func racine(rep http.ResponseWriter, req *http.Request) {
	c, err := req.Cookie("sid")
	if err == nil {
		// Création d'un cookie avec un nouvel ID de session
		fmt.Printf("Cookie existant : suppression du sid = %s\n", c.Value)
		c.MaxAge = -1
	} else {
		fmt.Println("Première connexion : création du cookie !")
	}
	nv := http.Cookie{
		Name:  "sid",
		Value: uuid.New().String(),
	}
	http.SetCookie(rep, &nv)
	fmt.Println("Connexion au site : nouveau cookie généré")

	tpl, err := template.ParseFiles("template.html")
	if err != nil {
		http.Error(rep, err.Error(), 500)
		log.Fatalf("Erreur sur la récupération du template : %v", err)
	}
	if tpl.Execute(rep, nil) != nil {
		http.Error(rep, err.Error(), 500)
		log.Fatalf("Erreur sur le traitement du template : %v", err)
	}
}

func connexion(rep http.ResponseWriter, req *http.Request) {

	// Vérifications
	// est-ce que le SID existe ?
	c, err := req.Cookie("sid")
	if err != nil {
		// redirection
		http.Redirect(rep, req, "/", http.StatusSeeOther)
		log.Println("Essai d'intrusion sans SID")
	}

	if req.Method == http.MethodPost {
		var u = utilisateur{}

		// est-ce que l'on a un utilisateur associé ? vérification avec le username

		u, ok := dbUsers[req.FormValue("username")]
		if ok {
			// l'utilisateur n'est plus nouveau :
			u.New = false
			// l'utilisateur existe : on récupère son ID depuis son nom d'utilisateur
			fmt.Printf("Utilisateur existant : %v\n", dbUsers[u.Username])
			id := dbUsers[u.Username].ID
			dbIDs[c.Value] = id
		} else {
			// si n'existe pas alors on crée un nouvel utilisateur
			fmt.Println("Création d'un nouvel utilisateur")
			// récupération depuis le formulaire des données
			u.Username = req.FormValue("username")
			u.Nom = req.FormValue("nom")
			u.Prenom = req.FormValue("prenom")
			// génère un nouvel ID pour le nouvel utilisateur
			u.ID = uuid.New().String()
			// c'est un nouvel utilisateur
			u.New = true
			// ajout du nouvel utilisateur
			dbUsers[u.Username] = u
			dbIDs[c.Value] = u.ID
		}

		fmt.Println("Utilisateur ", u)
		// on exécute notre template :
		tpl, err := template.ParseFiles("compte.html")
		if err != nil {
			http.Error(rep, err.Error(), 500)
		}

		tpl.ExecuteTemplate(rep, "compte.html", u)
		return
	}

	http.Redirect(rep, req, "/", http.StatusSeeOther)
}
