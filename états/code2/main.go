package main

import (
	"fmt"
	"log"
	"net/http"
)

func main() {
	fmt.Println("Lancement serveur v2")
	http.HandleFunc("/", racine)
	log.Fatal(http.ListenAndServe(":8080", nil))
}

func racine(rep http.ResponseWriter, req *http.Request) {
	v := req.FormValue("q")
	rep.Header().Set("Content-Type", "text/html; charset=utf-8")
	fmt.Fprintf(rep, `
<form method="post">
<input type="text" name="q">
<input type="submit">
</form>
<br/>
`+v)
}
