# Etats

Les états sont les éléments persistents tels que : qui est connecté. Nous verrons :

- comment passer des données via les URLs
- comment rediriger
- comment gérer les cookies
- comment gérer les sessions

## 1-Transmettre des données

on peut transmettre des données du client vers le serveur via une URL via le corps (```body```) de la requête.

Lorsque l'on transmet des infos via des URLs on utilise la méthode GET. Lorsqu'on utilise un formulaire ```<form>``` on utilise la méthode POST.

Moyen mnémotechnique :

```txt
GET == URL == 3 lettres

POST == FORM == 4 lettres
```

### via les URLs

Pour les URLs, on transmet les informations après le ```path``` suivi d'un ```?``` suivi d'une paire de clé (identifiant) / valeur. Et si l'on a un ensemble de paires clé-valeur : on les sépare avec ```&```. Voici un exemple :

```txt
https://www.mon-domaine.org/mon_path?user=john&vid=12345678#00h02m30s
```

Donc :

- le ```path``` est ```mon_path```
- il y a une requête puisqu'il y a présence de ```?```
- il y a un ensemble de paires de clé-valeur (total = 2)
- la première paire : ```user=john``` où ```user``` est la clé et ```john``` la valeur
- la seconde paire : ```id="12345678``` (```vid``` est la clé et ```12345678``` est la valeur associée)
- à la valeur ```12345678``` est associé un fragment (une valeur suivi d'un ```#``` puis d'une autre valeur)
- les deux paires sont séparées par ```&```

Parler d'état ici, c'est parler de transmettre l'état des données entre différentes pages web.

Pour récupérer des valeurs, nous disposons de la méthode ```Request.FormValues(clé string) string```.

Exemple :

```go
func main() {
    fmt.Println("Lancement serveur v1")
    fmt.Println("Test de http://localhost:8080/?q=1234567890")
    http.HandleFunc("/", root)
    log.Fatal(http.ListenAndServe(":8080", nil))
}

func root(rep http.ResponseWriter, req *http.Request) {
    // Récupération des valeurs de requêtes
    valeur := req.FormValue("q")
    fmt.Fprintf(rep, "Valeur retournée = %v", valeur)
    fmt.Println(valeur)
}
```

Code [Go](code1/main.go)

### via un formulaire

Avec un formulaire on dispose de deux façons de transmettre les infos :

- via l'URL
- via le corps/*body* de la requête

Nous allons coder un formulaire très basique directement dans une fonction implémentant le type ```Handler```. Nous réutilison exactement le même type de code que précédemment avec ```Request.FormValue()```

Code :

```go
func racine(rep http.ResponseWriter, req *http.Request) {
    v := req.FormValue("q")
    rep.Header().Set("Content-Type", "text/html; charset=utf-8")
    fmt.Fprintf(rep, `
<form method="post">
<input type="text" name="q">
<input type="submit">
</form>
<br/>
`+v)
}
```

Et comme nous n'utilisons pas de méthode GET, les clés et les valeurs associées ne sont pas transmises par l'URL mais dans le corps de la requête.

Code [Go](code2/main.go)

Dans cet autre exemple nous allons pousser la complexité en intégrant un template simple. Nous récupèrerons les données avec ```FormValue``` puis les stockeront dans une structure.

On crée un type structure très simple :

```go
type Personne struct {
    Nom    string
    Prenom string
}
```

Notre template affichera un formulaire et les résultats (à la condition que les deux champs ont été au préalable rempli) :

```html
<body>
    <form  method="post">
        <label for="prenom">Prénom</label>
        <input type="text" name="prenom" id="prenom">
        <br>
        <label for="nom">Nom</label>
        <input type="text" name="nom" id="nom">
        <br>
        <input type="submit">
    </form>

    <br>
{{if and (ne .Prenom "") (ne .Nom "")}}
    <p>Prénom : {{.Prenom}}</p>
    <p>Nom : {{.Nom}}</p>
{{else}}
    <p>Veuillez renseigner les deux champs.</p>
{{end}}
</body>
```

Pour le traitement côté Go :

```go
func racine(rep http.ResponseWriter, req *http.Request) {
    p := Personne{}
    p.Prenom = req.FormValue("prenom")
    p.Nom = req.FormValue("nom")

    tpl, err := template.ParseFiles("template.html")
    if err != nil {
        http.Error(rep, "Template : problème", http.StatusNotFound)
        log.Fatal("Erreur sur le template : %v", err)
    }

    err = tpl.ExecuteTemplate(rep, "template.html", p)
    if err != nil {
        http.Error(rep, "Erreur exécution templating", http.StatusNotFound)
        log.Fatal("Erreur exécution du templating : %v", err)
    }
}
```

Code [Go](code3/main.go) / [HTML](code3/template.html)

## 2-Téléversement, lecture et écriture d'un fichier côté serveur

Nous allons dans un premier temps réaliser un système qui va afficher un formulaire. Ce formulaire va récupérer un fichier. Celui-ci sera lu et affiché en retour.

Dans un premier temps, nous allons devoir vérifier que la méthode utilisée est bien POST : la bibliothèque standard dispose de la méthode ```Method()``` et de la constante ```MethodPost``` que nous utiliserons dans une comparaison d'égalité.

Si La méthode HTTP est bien POST, alors on on ouvre le fichier en le récupérant via la méthode ```Request.FormFile(clé)``` . Cette méthode retourne trois éléments :

- un fichier de type ```multipart.File```
- un entête (un pointeur ```multipart.FileHeader```)
- une erreur

Nous afficherons le contenu des deux premiers arguments à la console puis on essayera d'afficher le contenu avec ```ioutil.ReadAll()```. Pourquoi ```ReadAll``` plutôt que ```ReadFile``` ? parceque le premier est plus "générique" : il accepte tout type implémentant l'interface ```io.Reader```. Alors que la seconde fonction nécessite un fichier local.

Puis seulement après ces traitements nous afficherons le formulaire et les résultats (nous utiliserons un simple template)

Code :

```go
func racine(rep http.ResponseWriter, req *http.Request) {

    var rslt string

    // Traiter les données reçues
    if req.Method == http.MethodPost {
        // ouvrir
        f, h, err := req.FormFile("fichier")
        if err != nil {
            http.Error(rep, "Impossible d'ouvrir le fichier", 500)
            log.Fatal("Impossible d'ouvrir le fichier")
        }
        defer f.Close()

        // affichage des 3 valeurs retournées par FormFile
        fmt.Printf("Fichier : %v\nEntêtes: % v\nErreur: %v\n", f, h, err)

        // ouverture et lecture du contenu de f
        slc, err := ioutil.ReadAll(f)
        if err != nil {
            http.Error(rep, "Erreur sur la lecture du contenu", 500)
            log.Fatalf("Erreur sur le traitement du fichier : %v", err)
        }
        rslt = string(slc)
    }

    // affiche le formulaire
    tpl, err := template.ParseFiles("template.html")
    if err != nil {
        log.Fatalf("Erreur sur le fichier template à charger : %v", err)
    }

    if tpl.ExecuteTemplate(rep, "template.html", rslt) != nil {
        http.Error(rep, "Erreur parsing template", 500)
        log.Fatalf("Erreur sur le parsing du template : %v", err)
    }
}
```

Code [Go](code4/main.go) / [HTML](code4/template.html)

Dans le second exemple : nous allons rajouter l'écriture sur le fichier dans un répetoire "user" :

Pour récupérer le nom du fichier nous utiliserons le type ```multipart.FileHeader```. Puis nous créerons un pointeur de type ```os.File``` sur lequel nous écrirons à l'aide de ```Write```. On récupèrera le nom du fichier téléchargé et on le réemploiera comme nom dans le répertoire ```user```. De même nous ne coderons pas en dur le chemin d'accès : nous utiliserons  ```filepath.Join()```.

```go
    // Traitement du fichier téléchargé
    if req.Method == http.MethodPost {
        // ouvrir le fichier reçu
        f, h, err := req.FormFile("fichier")
        if err != nil {
            e := "Erreur sur le fichier téléversé"
            http.Error(rep, e, 500)
            log.Fatalln(e, " : ", err)
        }
        defer f.Close()

        slice, err := ioutil.ReadAll(f)
        if err != nil {
            e := "Erreur sur la lecture du contenu du fichier"
            http.Error(rep, e, 500)
            log.Fatalln(e, " : ", err)
        }

        rslt = string(slice)
        // Affichage du nom du fichier récupéré
        fmt.Printf("Nom du fichier récupéré : %v\n", h.Filename)

        // Ecriture du contenu du fichier en local dans le dossier ./user

        dst, err := os.Create(filepath.Join("./user/", h.Filename))
        if err != nil {
            e := "Erreur à la création du pointeur de fichier"
            http.Error(rep, e, 500)
            log.Fatalln(e, " : ", err)
        }
        defer dst.Close()

        // écriture
        _, err = dst.Write(slice)
        if err != nil {
            e := "Erreur à l'écriture"
            http.Error(rep, e, 500)
            log.Fatalln(e, " : ", err)
        }
    }
```

Attention à bien fermer les pointeurs de fichier ouverts.

Code [Go](code5/main.go) / [HTML](code5/template.html)

## 3-enctype

Lors de la création de nos formulaires, nous avons utilisé ```enctype="multipart/form-data```. Cet attribut est **obligatoire** à chaque fois que l'on veut uploader des fichiers vers un serveur.

Il existe d'autres type tel que ```application/x-www-form-urlencoded``` qui permet d'utiliser les paires de clés-valeurs dans l'URL (c'est le type par défaut). Ou encore ```text/plain``` qui est à éviter absolument dans le cadre de la production : c'est uniquement pour le développement pour des bibliothèques de parsing, pour générer des données brutes.

Comment traiter le type ```text/plain``` ?

Le plus simplement du monde :

- on crée un slice d'octets de longueur égale à ```req.ContentLength``` sinon nous n'aurons aucun retour : on définit l'espace par défaut de notre slice
- puis on lit le contenu du champ ```Body``` qui dispose d'une méthode ```Read()```
- on passe à ```Read()``` le slice en argument
- convertit le slice en string
- et pour terminer, on passe la chaîne au template

Code :

```go
func racine(rep http.ResponseWriter, req *http.Request) {

    var rslt = make([]byte, req.ContentLength)

    _, err := req.Body.Read(rslt)
    if err != nil {
        http.Error(rep, "Erreur lecture du contenu du fichier envoyé", 500)
        log.Fatalf("Erreur sur le contenu du fichier : %v ", err)
    }

    tpl, err := template.ParseFiles("template.html")
    if err != nil {
        http.Error(rep, "Erreur sur la récupération du template", 500)
        log.Fatalf("Erreur à la récupération du template : %v", err)
    }

    if tpl.ExecuteTemplate(rep, "template.html", string(rslt)) != nil {
        http.Error(rep, "Erreur à l'exécution du template", 500)
        log.Fatalf("Erreur à l'exécution du template : %v", err)
    }
}
```

Code [Go](code6/main.go) / [HTML](code6/template.html)

## 4-Redirection

Définition : le client effectue une demande de page à un serveur et ce dernier redirige la requête vers une autre page.

Go dispose de code HTTP sous forme de constantes [doc](https://golang.org/pkg/net/http/#pkg-constants) concernant les redirections (la série des 300 à 307). Et nous nous intéresserons au 301, 303 et 307.

- 301 : page déplacée de façon permanente.
- 303 : voir ailleurs : la réponse a été déplacée ailleurs
- 307 : redirection temporaire

Concernant le code 301, la plupart des navigateurs se redirigeront automatiquement après un premier passage si l'utilisateur retape la même URL.

Avec le code 303, la méthode HTTP est changé : on se retrouve à utiliser GET. La plupart utilise le code 302 pour commencer. Pourquoi choisir 303 et pas 302 ? réponse de [Wikipédia](https://fr.wikipedia.org/wiki/HTTP_302) : "Beaucoup de navigateurs webs implémentent ce code d'une manière qui viole les standards, en changeant le type de la nouvelle requête vers GET, sans tenir compte du type de la requête originale (c'est-à-dire POST). C'est pour cette raison que deux nouveaux statuts ont été créés : HTTP 303 et HTTP 307 pour distinguer les deux comportements, avec 303 changeant le type de la requête vers GET et 307 conservant le type de la requête originelle."

C'est pour cela que nous utiliserons les codes 303 et 307.

Pour plus de renseignement la RFC 7231 donne les éléments nécessaires pour comprendre les redirections : [section consacrée aux redirections (en)](https://tools.ietf.org/html/rfc7231#section-6.4)

### Mise en pratique avec le status 302

Pour cette mise en application, nous allons rediriger en écrivant directement dans les entêtes avec ```Header().Set()```. Rappel : ```Set()``` prend une clé et une valeur sous forme de chaîne de caractères. Nous redirigerons d'un chemin vers la racine tout simplement. Le code status que nous utiliserons est ```http.StatusSeeOther```. Et nous l'écrirons dans l'entête avec la méthode ```*Request.WriteHeader()```.

Code de base pour rediriger :

```go
func redirection(rep http.ResponseWriter, req *http.Request) {
    rep.Header().Set("Location", "/")
    rep.WriteHeader(http.StatusSeeOther)
}
```

Nous pouvons utiliser une autre fonction : ```http.Redirect()``` qui prend comme arguments :

- un type ```io.Writer``` (ce sera notre réponse de type ```http.ResponseWriter```)
- la requête
- le chemin vers lequel on se redirige, sous forme de chaîne
- et le code status

Avec cette fonction on n'a plus besoin d'écrire explicitement dans les entêtes : on gagne du temps.

Réécriture du code précédent :

```go
func redirection(rep http.ResponseWriter, req *http.Request) {
    http.Redirect(rep, req, "/", http.StatusSeeOther)
}
```

Code [Go](code7/main.go) / [Template](code7/template.html)

### Mise en pratique du code 307

Le code 307 indique une redirection temporaire.

Avec un code ```http.StatusTemporaryRedirect```, la redirection ne changera pas la méthode employée : ```POST``` sera conservé.

Code :

```go
func redirection(rep http.ResponseWriter, req *http.Request) {
    http.Redirect(rep, req, "/", http.StatusTemporaryRedirect)
}
```

### Mise en pratique du code 301

Rappel : le code 301 indique que la page a été déplacée définitivement.

Soit on utilise l'entier 301 soit la constante ```http.StatusMovedPermanently``` :

```go
func redirection(rep http.ResponseWriter, req *http.Request) {
    http.Redirect(rep, req, "/", http.StatusMovedPermanently)
}
```

Ce qui se passera avec les navigateurs "récents" est le "souvenir" d'une redirection permanente sans passer par le serveur, bien évidemment, seulement après un premier passage obligatoire vers "/redirection".

Pour bien tester : ne pas hésiter à nettoyer le cache.

## 5-Cookies

Rappel : le cookie est un fichier texte stocké sur le client, créé par le serveur et écrit par le serveur à la condition que le client l'autorise. Ces fichiers sont spécifiques à un nom de domaine.

### Ecriture et lecture de cookies

Pour créer un cookie, c'est le package ```http``` qui propose ```SetCookie()``` pour créer. Pour cela on passe à cette fonction deux paramètres :

- un type ```http.ResponseWriter```
- un pointeur de type ```Cookie```

Qu'est ce que le type ```Cookie``` ? Ce type qui dispose d'un certin nombre de champs : Name, Value, Path, Domain, Expires, MaxAge, ... C'est dans ```Value``` que l'on stockera des données. A ces données on associera un nom (champ ```Name```). ```MaxAge``` est intéressant car on donne une durée : égale à zéro, alors pas de durée de vie maximale. Inférieur à 0 signifie que le cookie doit être détruit. Supérieur à zéro, la durée de vie est uen durée en seconde. Associé au type ```Cookie``` on ne dispose que d'une fonction : ```String()``` permettant d'afficher le contenu d'un cookie.

Côté client (c'est-à-dire la requête envoyée par le client), on dispose avec le type ```Request``` de plusieurs méthodes :

- pour récupérer plusieurs cookies : ```Cookies()``` qui retourne un slice de ```Cookie```s
- pour ajouter un cookie : ```AddCookie(c *Cookie)```
- pour récupérer un cookie en particulier : ```Cookie(name string)```. Cette méthode deux valeurs : un pointeur de ```Cookie``` et une erreur

### Création

On utilise ```http.SetCookie()``` auquel on passe : le ```ResponseWriter``` et un pointeur de ```Cookie```. On affecte aux champs ```Name``` et ```Value```, une chaîne de caractères pour chacun :

```go
func setCookie(rep http.ResponseWriter, req *http.Request) {
    // Création d'un type Cookie
    c := http.Cookie{
        Name:  "ID",
        Value: "1234567890AB",
    }

    // Création du cookie
    http.SetCookie(rep, &c)
    fmt.Fprintf(rep, "Nous avons écrit des données dans un cookie.")

}
```

**ATTENTION** à bien vérifier ceci : on crée **d'abord** le cookie **puis** on crée l'interface HTML.

### Lecture

On récupère le cookie en lisant la valeur associée à  ```Name```. Et on affiche le résultat :

```go
func readCookie(rep http.ResponseWriter, req *http.Request) {
    // Récupération du cookie
    c, err := req.Cookie("ID")
    if err != nil {
        http.Error(rep, "Aucun cookie trouvé : "+err.Error(), http.StatusNoContent)
        log.Fatalf("Erreur sur la récupération du cookie : %v", err)
    }

    fmt.Fprintf(rep, "Contenu du cookie : %v", c)
}
```

Code [Go](code8/main.go) / [HTML](code8/template.html)

### Test

On ouvrir les outils de développement du navigateur pour regarder le cookie créé.

### Créer plusieurs cookies

Il faudra créer autant d'objets ```Cookie``` que l'on a besoin. Mais à chaque fois il faudra appeler la fonction ```SetCookie()```.

Un cookie = un appel de ```SetCookie()```.

### Suppresion du/des cookies

On utilisera le champ ```MaxAge``` qui enregistre la durée de vie en seconde d'un cookie. Pour le supprime on utilise un entier négatif. Puis on appelle à nouveau ```SetCookie```.

Il existe un champ dans le type ```Cookie``` : ```Expires``` qui prend un type ```time.Time``` qui est optionnel

Code :

```go
func supprime(rep http.ResponseWriter, req *http.Request) {
    c, err := req.Cookie("ID")
    if err != nil {
        http.Redirect(rep, req, "./", http.StatusSeeOther)
        log.Println("Erreur impossible de récupérer le cookie : ", err)
        return
    }
    c.MaxAge = -1
    http.SetCookie(rep, c)
    http.Redirect(rep, req, "/", http.StatusSeeOther)
}
```

Code [Go](code9/main.go)
