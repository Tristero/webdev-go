package main

import (
	"log"
	"os"
	"text/template"
)

var tpl *template.Template

func init() {
	tpl = template.Must(template.ParseGlob("*.txt"))
}

func main() {
	if tpl.ExecuteTemplate(os.Stdout, "fichier1.txt", nil) != nil {
		log.Fatal("Erreur sur fichier 1.")
	}

	if tpl.ExecuteTemplate(os.Stdout, "fichier2.txt", nil) != nil {
		log.Fatal("Erreur sur fichier 2.")
	}

	if tpl.ExecuteTemplate(os.Stdout, "fichier3.txt", nil) != nil {
		log.Fatal("Erreur sur fichier 3.")
	}
}
