package main

import (
	"fmt"
	"html/template"
	"log"
	"net/http"
	"net/url"
)

type server struct{}

func (s server) ServeHTTP(rep http.ResponseWriter, req *http.Request) {

	// parsing du formulaire
	if err := req.ParseForm(); err != nil {
		log.Fatalf("Erreur sur le parsing : %v", err)
	}

	// stockage des données
	donnees := struct {
		Methode    string
		URL        *url.URL
		Formulaire url.Values
		Adresse    string
		Headers    http.Header
	}{
		Methode:    req.Method,
		URL:        req.URL,
		Formulaire: req.PostForm,
		Adresse:    req.RemoteAddr,
		Headers:    req.Header,
	}

	if err := tpl.ExecuteTemplate(rep, "template.html", donnees); err != nil {
		log.Fatalf("erreur sur le traitement final du template : %v", err)
	}
}

var tpl *template.Template

func init() {
	var err error
	tpl, err = template.New("").ParseFiles("template.html")
	if err != nil {
		log.Fatalf("Erreur parsing du template : %v", err)
	}

}

func main() {
	var s server
	fmt.Println("Lancement du serveur 3")
	if err := http.ListenAndServe(":8080", s); err != nil {
		log.Fatalf("Erreur sur ListenAndServe : %v", err)
	}
}
